from ORM import *
from flask import *
import io
import json
import ORM
import pymongo
import ipdb
import gzip
import json
import base64
import re
import time
import psycopg2
import psycopg2.extras
from bson import Binary, Code
from bson.json_util import dumps
from bson.json_util import loads
app = Flask(__name__)

"""Connexion a la base de donnees"""
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)


@app.route("/")
def hello():
	return ("Hello Loic !!!!")


@app.route("/list/Taxon", methods=['POST', 'GET'])
#affiche tous les taxons de la base
def taxon_all():
	T1 = time.time()
	T2 = time.clock()
	F1 = time.time()
	try:
		cursor.execute("SELECT taxon_id, taxon_name from Taxon ;")
	except psycopg2.Error as e:
		print (e.pgerror)
	FF1 = time.time()
	print( "temps de la requete 1 %f" %(FF1 - F1))
	results = cursor.fetchall()
	cursor.execute('commit')
	resultsf = dict()
	taxon_name = list()
	for i in results:
		identifiant, name = i
		taxon_name.append(name)
		resultsf[name] =  identifiant
	taxon_encode = dumps(resultsf)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction list/Taxon : %f" %(FT1 - T1))
	print("Temps CPU de la fonction list/Taxon : %f" %(FT2 - T2))
	return render_template("taxon_selection.html", taxon_name = taxon_name, taxon_dict = taxon_encode)

@app.route("/tree",methods=['GET'])
def test():
	tables = ['taxon', 'sequence']
	oid = request.args.get('id', None)
	result_taxons = dict()
	taxon_name = list()
	element_name = list()
	dico_test = list()
	print("debut")
	if oid is None:
		print("eheo")
		try:
			cursor.execute("SELECT taxon_id, taxon_name from Taxon ;")
		except psycopg2.Error as e:
			print (e.pgerror)
		results = cursor.fetchall()
		cursor.execute('commit')
		for i in results:
			identifiant, name = i
			taxon_name.append(name)
			result_taxons[name] =  identifiant
		for i,j in zip(result_taxons.keys(),result_taxons.values()):
			dico_test.append({'id':'taxon_'+str(j),'text':i, 'state':'closed'})
	else:
		print("suite")
		table,tid = oid.split("_")
		if table in tables:
			print("suite suite")
			k = tables.index(table)
			print(str(k))
			if k != len(tables):
				childrentable = tables[k+1]
				print(childrentable)
				try:
					cursor.execute("SELECT %s_id,%s_name from  ;",(childrentable,childrentable,childrentable,))
				except psycopg2.Error as e:
					print (e.pgerror)
				results = cursor.fetchall()
				print("coucou")
				print(results)
				cursor.execute('commit')
				for i in results:
					identifiant, name = i
					element_name.append(name)
					result_taxons[name] =  identifiant
				if k+1 != len(tables):
					for i,j in zip(result_taxons.keys(),result_taxons.values()):
						dico_test.append({'id':childrentable+str(j),'text':i, 'state':'closed'})
				else:
					for i,j in zip(result_taxons.keys(),result_taxons.values()):
						dico_test.append({'id':childrentable+str(j),'text':i, 'state':'open'})
	#taxon_encode = dumps(resultsf)
	#~ dico_test = [
		#~ {'id': 'taxon_1', 'text':'node 1', 'state': 'open'},
		#~ {'id': 'taxon_2', 'text': 'node 2', 'state': 'closed'}
	#~ ]
	#~ return json.dumps(dico_test)
	return json.dumps(dico_test)
	
def hidden_encode(data):
	return base64.b64encode(json.dumps(data).encode('utf-8')).decode('ascii')
	
def hidden_decode(text):
	return json.loads(base64.b64decode(text).decode('utf-8'))


@app.route("/taxons/chromosomes", methods=['POST', 'GET'])
#affiche tous les taxons de la base
#def taxon_all():
def taxon_chromosomes():
	T1 = time.time()
	T2 = time.clock()
	#Noms des taxons
	list_taxon_name = request.form.getlist('Taxons')
	print("taille liste",str(len(list_taxon_name)))	
	print(list_taxon_name)
	#Liste des identifiants des taxons
	taxon_list = request.form.getlist('taxon_dict')[0]
	chromosomes_id_list = request.form.getlist('Chromosomes')
	#Dictionnaire des noms des taxons et de leurs identifiants
	taxon_decode = loads(taxon_list)
	print("taille dico",str(len(taxon_decode)))
	print(taxon_decode)
	if not list_taxon_name:
		try:
			cursor.execute("SELECT taxon_name from Taxon;")
		except psycopg2.Error as e:
			print (e.pgerror)
		info_taxons = cursor.fetchall()
		cursor.execute('commit')
		for i in info_taxons:
			name = i[0]
			list_taxon_name.append(name)
			print("taille new liste",str(len(list_taxon_name)))	
			print(list_taxon_name)
	chromosomes_list = dict()
	family_name = list()
	family = dict()
	chromosome_name_id = dict()
	taxon_dict2 = dict()  
	#Identifiant du taxon selectionne
	taxon_id = [taxon_decode[name] for name in list_taxon_name]
	#print(taxon_id)
	#Requetes liste des chromosomes avec l'identifiant du taxon selectionne
	#chromosomes = list()
	#chromosomes_id = dict()
	all_chromosomes_id = list()
	dict2 = dict()
	chromosome_list = dict()
	#Dictionnaire des noms des taxons et de leurs identifiants
	dict2 = {str(taxon_decode[d]) : d for d in taxon_decode}
	F1 = time.time()
	try:
		cursor.execute("SELECT taxon_id, taxon_name from Taxon ;")
	except psycopg2.Error as e:
		print (e.pgerror)
	FF1 = time.time()
	print( "temps de la requete 1 %f" %(FF1 - F1))
	results = cursor.fetchall()
	cursor.execute('commit')
	resultsf = dict()
	taxon_name = list()
	for i in results:
		identifiant, name = i
		taxon_name.append(name)
		resultsf[name] =  identifiant
	taxon_encode = dumps(resultsf)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction list/Taxon : %f" %(FT1 - T1))
	print("Temps CPU de la fonction list/Taxon : %f" %(FT2 - T2))
	#return render_template("taxons_chromosomes_test.html", taxon_name = taxon_name, taxon_dict = taxon_encode)

	T1 = time.time()
	T2 = time.clock()
	F1 = time.time()
	try:
		cursor.execute("SELECT sequence_id, taxon_id, sequence_name, chromosome_name FROM Sequence WHERE taxon_id IN %s ;", (tuple(taxon_id),))
	except psycopg2.Error as e:
		print (e.pgerror)
	FF1 = time.time()
	print(" temps de la requete 1 %f" %(FF1 - F1))
	A1 = time.time()
	sequenceId_list = list()
	for i in cursor:
		(sequence_id,taxon_id,sequence_name,chromosome_name) = i
		#sequenceId_list.append(sequence_id)
		#chromosomes_id[i[1]] = i[0]
		all_chromosomes_id.append(sequence_id)
		#chromosome_list[taxon_id][chromosome_name]={}
		#if dict2[str(taxon_id)] not in chromosome_list:
			#chromosome_list[dict2[str(taxon_id)]] ={}
		#Taxon_name recupéré avec le dict2 a partir du taxon_id
		chromosome_list["%s : %s : %s" %(dict2[str(taxon_id)],chromosome_name,sequence_name)] = sequence_id
		#if "%s : %s" %(dict2[str(taxon_id)],chromosome_name) not in chromosome_list:
			#chromosome_list["%s : %s" %(dict2[str(taxon_id)],chromosome_name)] = [sequence_id]
		#else:
			#chromosome_list["%s : %s" %(dict2[str(taxon_id)],chromosome_name)].append(sequence_id)
	#print(chromosome_list)
	cursor.execute('commit')
	print(chromosome_list)
	print(type(chromosome_list))
	A2 = time.time()
	#print(chromosome_list)
	print(" temps du remplissage %f" %(A2 - A1))
	family_id = dict()
	t1 = time.time()
	if not chromosomes_id_list:
		try:
			cursor.execute("SELECT family_id,family_name FROM Family WHERE sequence_id IN %s;",(tuple(all_chromosomes_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
	else:
		try:
			cursor.execute("SELECT family_id,family_name FROM Family WHERE sequence_id IN %s;",(tuple(chromosomes_id_list),))
		except psycopg2.Error as e:
			print (e.pgerror)
	for i in cursor:
		family_id[i[1]] = i[0]
	t3 = time.time()
	print(t3-t1)
	#~ if not family_id:
		#~ family_name = [d for d in family_id]
		#~ family_name = list({v['_id']:v for v in family_name}.values())
		#~ family_name_id = {str(d['_id']) : d['name'] for d in family_id}
		#~ for g in family_name : 
			#~ family[g['name']] = g['_id']

	blockName_dict = dict()
	if not chromosomes_id_list:
		try:
			cursor.execute("SELECT block_id,block_name FROM Block WHERE sequence_id IN %s;",(tuple(all_chromosomes_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
	else:
		try:
			cursor.execute("SELECT block_id,block_name FROM Block WHERE sequence_id IN %s;",(tuple(chromosomes_id_list),))
		except psycopg2.Error as e:
			print (e.pgerror)		
	for i in cursor:
		blockName_dict[i[1]] = i[0]
	cursor.execute('commit')
	print(blockName_dict)
	print(type(blockName_dict))
	#print(chromosomes)
	#print(chromosome_list)
	#Dictionnaire des noms et identifiants des chromosomes
	#chromosomes_id = {d[1] : d[0] for d in chromosomes}
	#Liste des identifiants des chromosomes
	#all_chromosomes_id = [d[0] for d in chromosomes]
	#Dictionnaire des identifiants des taxons : noms des chromosomes
	#Modifications:
	#Liste de tri croissant des caracteres ASCII des clés du dictionnaire
	#Dictionnaire des noms des chromosomes (nomEspece+nomChro) et des idenfiants des chromosomes
	#chromosome_list = {"%s : %s" %(dict2[str(d[2])], d[1]) : d[0]for d in chromosomes}
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction /taxons/chromosomes : %f" %(FT1 - T1))
	print("Temps CPU de la fonction /taxons/chromosomes : %f" %(FT2 - T2))
	return render_template("taxons_chromosomes_test_2.html", taxon_name = taxon_name, taxon_dict = taxon_encode, chromosome = chromosome_list,list_taxon = taxon_id, chro_dict = dumps(all_chromosomes_id), block_dict = blockName_dict, fmaily_id = family_id )

@app.route("/taxons/monomers_output", methods=['POST', 'GET'])
def monomers_output():
	monomerlist = request.form.getlist('Monomers')
	ipdb.set_trace()
	resultsf = list()
	for name in monomerlist :
		ligne = {}
		try:
			cursor.execute("SELECT monomer_name, monomer_begin, monomer_end, monomer_strand FROM Monomere where monomer_name = %s;",(name,))
		except psycopg2.Error as e:
			print (e.pgerror)
		monomer_info = cursor.fetchone()
		cursor.execute('commit')
		ligne['name'],ligne['begin'],ligne['end'],ligne['strand'] = monomer_info
		ligne['chromosome_name'] = monomerlist[i][j]['name'].split("_")[0]
		resultsf.append(ligne)
		conn.commit()
	return render_template("taxons_monomers_output.html", monomer = resultsf)
	
def gen_chromosome_id_list(chromosome_list):
	T1 = time.time()
	T2 = time.clock()
	chromosome_id = list()
	for name in chromosome_list:
		(taxon_id, sequence_name, chromosome_name) = name.split(" : ")
		#cursor.execute("SELECT taxon_id FROM Taxon WHERE taxon_name = %s ;", (taxon_name,))
		#taxon_identifiant = cursor.fetchall()
		taxon_id = taxon_identifiant[0]
		try:
			cursor.execute("SELECT sequence_id FROM Sequence WHERE chromosome_name = %s AND taxon_id = %s ;", (chromosome_name, taxon_id,))
		except psycopg2.Error as e:
			print (e.pgerror)
		chr_id = cursor.fetchall()
		cursor.execute('commit')
		chromosome_id.append(chr_id)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction gen_chromosome_id_list() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction gen_chromosome_id_list() : %f" %(FT2 - T2))
	return chromosome_id
   
def find_blocks(chromosome_list):
	T1 = time.time()
	T2 = time.clock()
	F1 = time.time()
	print("taille liste")
	print(str(len(chromosome_list)))
	try:
		cursor.execute("SELECT block_id FROM Block where sequence_id IN %s ;", (tuple(chromosome_list),))
	except psycopg2.Error as e:
		print (e.pgerror)
	FF1 = time.time()
	print("Temps de la requete blocks %f" %(FF1 - F1))
	block_identifiant = cursor.fetchall()
	cursor.execute('commit')
	blocks = [d[0] for d in block_identifiant]
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction find_blocks() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction find_blocks() : %f" %(FT2 - T2))
	return blocks

def find_monomers(chromosome_list):
	## Voir pour passer directement par chromosome ##
	T1 = time.time()
	T2 = time.clock()
	try:
		cursor.execute("SELECT block_id FROM Block WHERE chromosome_id IN %s ;", (chromosome_list,))
	except psycopg2.Error as e:
		print (e.pgerror)
	block_identifiant = cursor.fetchall()
	cursor.execute('commit')
	blocks = [d for d in block_identifiant]
	try:
		cursor.execute("SELECT monomer_id FROM Monomer WHERE chromosome_id IN %s ;", (chromosome_list,))
	except psycopg2.Error as e:
		print (e.pgerror)
	monomer_identifiant = cursor.fetchall()
	cursor.execute('commit')
	monomers = [d[0] for d in monomer_identifiant]
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction find_monomers() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction find_monomers() : %f" %(FT2 - T2))
	return monomers

@app.route("/taxons/filters", methods=['POST', 'GET'])
def filters():
	T1 = time.time()
	T2 = time.clock()
	#Liste de tous les chromosomes selectionnes de la requete choix des chromosomes 
	chromosome_list = request.form.getlist('Chromosomes')
	#print(chromosome_list)
	family_list = request.form.getlist('Family')
	phase = request.form.getlist('phase')[0]
	base_begin = request.form.getlist('base_pair_begin')[0]
	base_end = request.form.getlist('base_pair_end')[0]
	distance_centro = request.form.getlist('distance_from_centromer')[0]
	block_id = request.form.getlist('block_name')
	taxon_list = request.form.getlist('list_taxon')
	block_length = request.form.getlist('block_length')[0]
	monomer_length = request.form.getlist('monomer_length')[0]
	#Liste de tous les identifiants de chromosomes de l'espece
	all_chromosomes_id = loads(request.form.getlist('list_chromosomes')[0])
	#print(all_chromosomes_id)
	#aa = [str(i) for i in all_chromosomes_id]
	#print(aa[1:10])
	#liste_chrs = open("list_chrs", "w")
	#liste_chrs.write(",".join(aa))
	#liste_chrs.close()
	chromosome_id = list()
	monomers = list()
	blocks = list()
	family_name_show = list()
	
	##Conditions si les differents champs sont remplis##
	print("type phase",type(phase))
	#print(str(len(phase)))
	#if not isinstance(phase,str):
	if phase.isdigit():
		phase = int(phase)
		print("pas un str initial")
	elif not phase.isdigit() or not phase:
		phase = 1
	#print(str(phase))
	#if not isinstance(base_begin,str):
	if base_begin.isdigit():
		b_begin = int(base_begin)
		print("pas un str initial")
	elif not base_begin.isdigit() or not base_begin:
	   b_begin = 0
	   print("case vide")
	else:
	   print("toto")
	#if not isinstance(base_end,str):
	if base_end.isdigit():	
		b_end = int(base_end)
		print("pas un str initial")
	elif not base_begin.isdigit() or not base_end:
		b_end = 4000000000
		print("case vide")
	else:
	   print("toto")
	#if not isinstance(distance_centro,str):
	if distance_centro.isdigit():
		distance_centro = int(distance_centro)

	#Selection par noms de chromosomes
	#if isinstance(chromosome_list,list):
	if chromosome_list:
		chromosome_list = [d for d in chromosome_list]
		#print("liste chro")
	else:
		#print(" pas une liste chros")
		chromosome_list = all_chromosomes_id
		chromosome_list = [d for d in chromosome_list]
		#chromosome_name = list()
		#chromosome_list = [ObjectId(d) for d in chromosome_list]
		#print(chromosome_list)
		#Tri de la liste de chromosomes selectionnes
		chromosome_list2 = sorted(chromosome_list)
	print("la liste de chro",str(len(chromosome_list)))
	#cursor.execute("SELECT block_id FROM Block where chromosome_id IN %s ;", (tuple(all_chromosomes_id),))
	#all_blocks_id = list()
	#for i in cursor:
	#	all_blocks_id.append(i[0])
	F1 = time.time()
	try:
		cursor.execute("SELECT count(monomer_id) FROM Monomer where sequence_id IN %s ;", (tuple(all_chromosomes_id),))
	except psycopg2.Error as e:
		print (e.pgerror)
	FF1 = time.time()
	print("Temps de la requete 1 %f" %(FF1 - F1))
	monomerstot = cursor.fetchone()
	cursor.execute('commit')
	print(str(monomerstot))
	#print("comptage monomer fait")
	F2 = time.time()
	cursor.execute("SELECT count(block_id) FROM Block where sequence_id IN %s ;", (tuple(all_chromosomes_id),))
	FF2 = time.time()
	print("Temps de la requete 2 %f" %(FF2 - F2))
	blockstot = cursor.fetchone()
	cursor.execute('commit')
	#print("comptage block fait")
	F3 = time.time()
	if not base_begin or not base_end:
		#print("pas de begin end")
		try:
			cursor.execute("SELECT monomer_id FROM Monomer where sequence_id IN %s ;", (tuple(chromosome_list),))
		except psycopg2.Error as e:
			print (e.pgerror)
	else:
		try:	
			cursor.execute("SELECT monomer_id FROM Monomer where sequence_id IN %s AND monomer_begin >= %s AND monomer_end <= %s ;", (tuple(chromosome_list), b_begin, b_end,))
		except psycopg2.Error as e:
			print (e.pgerror)
	FF3 = time.time()
	print("temps de la requete 3 %f" %(FF3 -F3))
	monomer_identifiant = cursor.fetchall()
	cursor.execute('commit')
	monomers = [d[0] for d in monomer_identifiant]
	"""	
	if distance_centro.strip() != '':
		print("centro pas vide")
		##
		cursor.execute("SELECT * FROM Chromosome WHERE chromosome_id IN %s;",(chromosome_list,))
		chromosomes_array = cursor.fetchall()
		#chromosomes_array = list(c.find({'_id' : {'$in': chromosome_list}}))[0]
		if 'centromer' not in chromosomes_array:
			monomers = find_monomers(chromosome_id)
			print("centro pas ds le chro")
		else : 
			print("centro ds le chro")
			info_centromer = chromosomes_array['centromer']
			centromer_begin_list = [int(d['begin']) for d in info_centromer]
			centromer_begin = min(centromer_begin_list)
			centromer_end_list = [int(d['end']) for d in info_centromer]
			centromer_end = max(centromer_end_list)
			##
			cursor.execute("SELECT monomer_id FROM Monomer WHERE chromosome_id IN %s AND monomer_begin >= (%s - %s) AND monomer_end <= (%s - %s) ;", (chromosome_list, centromer_begin, distance_centro, centromer_end, distance_centro,))
			monomers_identifiant = cursor.fetchall()
			#v = list(m.find( {'Chromosome_id' : {'$in': chromosome_list}, 'begin' : {"$gte" : (centromer_begin - distance_centro)} , 'end' : {"$lte" : (centromer_end + distance_centro)} }))
			monomers = [d[0] for d in v]
			blocks = find_blocks(chromosome_id)
	else:
		print("centro vide")

	#?#Apparement quand champs vide on a bien une liste de familles
	if family_list:
		#ObjectId() est utlise pr transforme en id pr mongoDB
		family_list = [ObjectId(d) for d in family_list]
		##
		v = list(m.find({'families' : {"$elemMatch" :{'family_id' :  {"$in" : family_list}}}}))
		monomers_list_family =  [d['_id'] for d in v]
		monomers = list(set(monomers)& set(monomers_list_family))
		##
		cursor.execute("SELECT family_name FROM Family WHERE family_id IN  %s ;"), (family_list))
		family_list_to_name = cursor.fetchall()
		#family_list_to_name = list(f.find({'_id' : {"$in" : family_list} }))
		family_name_show = [d[0] for d in family_list_to_name]
		print("liste famille")
	else:
		print("vide")

	"""
	blocks = find_blocks(chromosome_list)
	#Selection par noms de blocks
	#print("trzergtefg")
	if block_id:
		monomers2 = []
		#block_list = block_name.split("\r\n")
		#blocks_id = block_name.split("\r\n")
		#cursor.execute("SELECT block_id FROM Block WHERE block_name IN %s ;", (tuple(block_list),))
		#blocks_id = cursor.fetchall()
		try:
			cursor.execute("SELECT monomer_id FROM Monomer WHERE block_id IN %s ;", (tuple(block_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
		monomers2 = cursor.fetchall()
		cursor.execute('commit')
		monomers2 = [d[0] for d in monomers2]
		monomers = [element for element in monomers if element in monomers2]
		try:
			cursor.execute("SELECT DISTINCT block_name FROM Block WHERE block_id IN %s ;", (tuple(block_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
		block_name = list()
		for i in cursor:
			block_name.append(i[0])
		cursor.execute('commit')
		#block_name = block_id
		#modification
	else:
		block_name = []
#		print("une chaine block")
	monomers_number = len(monomers)
	blocks_number = len(blocks)
	F4 = time.time()
	#Attention si l'objet est une liste de liste, la fonction dump() la transforme en dictionnaire
	monomers_list_encode = dumps([d for d in monomers])
	blocks_list_encode = dumps([d for d in blocks])
	FF4 = time.time()
	print("Temps de la requete 4 %f" %(FF4 - F4)) 
	#Liste des identifiants des chromosomes selectionnes
	F5 = time.time()	
	try:
		cursor.execute("SELECT DISTINCT chromosome_name FROM Sequence WHERE sequence_id IN %s ;", (tuple(chromosome_list),))
	except psycopg2.Error as e:
		print (e.pgerror)
	chromosome_list_to_name = cursor.fetchall()
	cursor.execute('commit')
	FF5 = time.time()
	print("Temps de la requete 5 %f" %(FF5 - F5))
	#Liste des noms des chromosomes selectionnes
	chromosome_name_show = [d[0] for d in chromosome_list_to_name]
	#Modifications:
	#Tri des caracteres ASCII des noms des chromosomes par ordre croissant
	chromosome_name_show = sorted(chromosome_name_show)
	try:
		cursor.execute("SELECT DISTINCT sequence_name FROM Sequence WHERE sequence_id IN %s ;", (tuple(chromosome_list),))
	except psycopg2.Error as e:
		print (e.pgerror)
	sequence_name_list = cursor.fetchall()
	cursor.execute('commit')
	#Liste des noms des chromosomes selectionnes
	sequence_name_show = [d[0] for d in sequence_name_list]
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction /taxons/filters : %f" %(FT1 - T1))
	print("Temps CPU de la fonction /taxons/filters : %f" %(FT2 - T2))
	block_length_id = list()
	monomer_length_id = list()
	monomers_id = list()
	if block_length:
		list_length = block_length.rstrip("\n").split(":")
		print(list_length[0])
		print(list_length[1])
		if block_length.isdigit():
			try:
				cursor.execute("SELECT block_id FROM Block WHERE block_length = %s;",(block_length,))
			except psycopg2.Error as e:
				print (e.pgerror)
				for i in cursor:
					block_length_id.append(i[0])
			cursor.execute('commit')
		elif len(list_length) == 2:
			if list_length[0] == "" and list_length[1].isdigit():
				try:
					cursor.execute("SELECT block_id FROM Block WHERE block_length =< %s;",(list_length[1],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					block_length_id.append(i[0])
				cursor.execute('commit')
			elif list_length[1] == "" and list_length[0].isdigit():
				try:
					cursor.execute("SELECT block_id FROM Block WHERE block_length >= %s;",(list_length[0],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					block_length_id.append(i[0])
				cursor.execute('commit')
			elif list_length[0].isdigit() and list_length[1].isdigit():
				try:
					cursor.execute("SELECT block_id FROM Block WHERE block_length >= %s and block_length <= %s ;",(list_length[0],list_length[1],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					block_length_id.append(i[0])
				cursor.execute('commit')
		try:
			cursor.execute("SELECT monomer_id FROM Monomer WHERE block_id IN %s ;",(tuple(block_length_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			monomers_id.append(i[0])
	if monomer_length:
		list_length2 = monomer_length.rstrip("\n").split(":")
		print(list_length2[0])
		print(list_length2[1])
		if monomer_length.isdigit():
			try:
				cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_length = %s;",(monomer_length,))
			except psycopg2.Error as e:
				print (e.pgerror)
				for i in cursor:
					monomer_length_id.append(i[0])
			cursor.execute('commit')
		elif len(list_length) == 2:
			if list_length2[0] == "" and list_length2[1].isdigit():
				try:
					cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_length <= %s;",(list_length2[1],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					monomer_length_id.append(i[0])
				cursor.execute('commit')
			elif list_length2[1] == "" and list_length2[0].isdigit():
				try:
					cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_length >= %s;",(list_length2[0],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					monomer_length_id.append(i[0])
				cursor.execute('commit')
			elif list_length2[0].isdigit() and list_length2[1].isdigit():
				try:
					cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_length >= %s and monomer_length <= %s ;",(list_length2[0],list_length2[1],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					monomer_length_id.append(i[0])
				cursor.execute('commit')
		try:
			cursor.execute("SELECT monomer_id FROM Monomer WHERE block_id IN %s ;",(tuple(monomer_length_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			monomers_id.append(i[0])				
		
	####DEBUT DU TESTS
	#TEST = time.time()
	#TEST2 = time.clock()
	#cursor.execute("SELECT monomer_id FROM Monomer where chromosome_id IN %s ;", (tuple(all_chromosomes_id),))
	#cursor.execute("prepare myplan as " "select monomer_id FROM Monomer WHERE chromosome_id = $1 ;")
	#for i in all_chromosomes_id:
		#cursor.execute("execute myplan (%s);", (str(i),))
		#cursor.execute("select monomer_id FROM Monomer WHERE chromosome_id = %s;"(i,))
		#cursor.execute('commit')
	#FTEST = time.time()
	#FTEST2 = time.clock()
	#print("Temps total requete test : %f" %(FTEST - TEST))
	#print("Temps CPU requete test : %f" %(FTEST2 - TEST2))
	####FIN DU TEST
	#a = cursor.fetchall()
	#print(a)
	#print(a[0:10])
	#print("taille de a",len(a))
	return render_template("show_filters.html", phase = phase, chromosomes = chromosome_list, family = family_list, blocks = blocks_number, blockstot = blockstot[0], blocks_list_encode = blocks_list_encode,
	base_begin = base_begin, base_end = base_end, monomers = monomers_number, monomerstot = monomerstot[0], monomers_list_encode = monomers_list_encode, distance_centro = distance_centro,
	block_name = block_name, family_name_show = family_name_show, chromosome_name_show = chromosome_name_show, sequence_name_show = sequence_name_show )


@app.route("/results/monomers",  methods=['POST', 'GET'])
def results():
	monomers_list = request.form.getlist("monomers_list_encode")[0]
	blocks_list = request.form.getlist("blocks_list_encode")[0]
	retrieve_monomers = request.form.getlist("retrieve_monomers")
	retrieve_blocks = request.form.getlist("retrieve_blocks")
	phase = request.form.getlist("monomer_phase")[0]
	#print(retrieve_monomers)
	#print(retrieve_blocks)
	if retrieve_monomers == ['retrieve_monomers']:
		list_encode = monomers_list
		retrieve_type = "monomers"
	else:
		list_encode = blocks_list
		retrieve_type = "blocks"
	return render_template("results_monomers.html", list_encode = list_encode, retrieve_type = retrieve_type, phase = phase)

@app.route("/results/monomers&blocks", methods=['POST', 'GET'])
def results_monomers_blocks():
	list_encode = loads(request.form.getlist("list_encode")[0])
	retrieve_type = request.form.getlist("retrieve_type")[0]
	format_output = request.form.getlist("format")[0]
	fasta_output = request.form.getlist("output")[0]
	csv_output = request.form.getlist("csv")[0]
	phase = int(request.form.getlist("monomer_phase")[0])
	name_format = request.form.getlist("name_format")
	separator_format = request.form.getlist("separator_format")[0]
	results = io.StringIO()
	family_string = str()
	coll = {"monomers" : Monomer, "blocks" : Block}
	mono_counter = 0
	block_counter = 0
	liste_name = list()
	if coll[retrieve_type] == Monomer:
		if format_output == 'format_fasta':
			for identifiant in list_encode:
				liste_name= []
				if "block_name" in name_format:
					try:
						cursor.execute("SELECT block_id FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)		
					block_id = cursor.fetchone()
					cursor.execute('commit')					
					try:
						cursor.execute("SELECT block_name FROM Block WHERE block_id = %s ;",(block_id[0],) )
					except psycopg2.Error as e:
						print (e.pgerror)
					block_name = cursor.fetchone()
					cursor.execute('commit')
					liste_name.append(block_name['block_name'])
				if "monomer_id" in name_format:
					liste_name.append(identifiant)
				if "begin" in name_format:
					try:
						cursor.execute("SELECT monomer_begin FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_begin = cursor.fetchone()
					cursor.execute('commit')
					liste_name.append(str(monomer_begin['monomer_begin']))
				if "end" in name_format:
					try:
						cursor.execute("SELECT monomer_end FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_end = cursor.fetchone()
					cursor.execute('commit')
					liste_name.append(str(monomer_end['monomer_end']))
				if "strand" in name_format:
					try:
						cursor.execute("SELECT monomer_strand FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_strand = cursor.fetchone()
					cursor.execute('commit')
					liste_name.append(str(monomer_strand['monomer_strand']))
				if "monomer_length" in name_format:
					try:
						cursor.execute("SELECT monomer_length FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_length = cursor.fetchone()
					cursor.execute('commit')
					liste_name.append(str(monomer_length['monomer_length']))
				if "database_name" in name_format:
					try:
						cursor.execute("SELECT monomer_name FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_database = cursor.fetchone()
					cursor.execute('commit')
					liste_name.append(monomer_database['monomer_name'])
				name = separator_format.join(liste_name)
				print(name)
				try:
					cursor.execute("SELECT monomer_index,monomer_nucleotide FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				index = info['monomer_index']
				nucleotide = info['monomer_nucleotide']
				"""if name_format == "database_name":
					try:
						cursor.execute("SELECT monomer_name,monomer_nucleotide,monomer_index FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(name,nucleotide,index) = info
					print(str(len(nucleotide)))
					print(str(len(index)))
				elif name_format == "chr_begin_end_strand":
					try:
						cursor.execute("SELECT monomer_name,monomer_nucleotide,monomer_index,sequence_id,monomer_strand FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(mono_name,nucleotide,index,sequence_id,strand) = info
					mono_list = mono_name.rstrip("\n").split("_")
					begin = mono_list[1]
					print(begin)
					end = mono_list[2]
					print(end)
					try:
						cursor.execute("SELECT sequence_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					sequence_name = cursor.fetchone()
					cursor.execute('commit')
					print(sequence_name)
					name = sequence_name[0]+"_"+str(begin)+"_"+str(end)+"_"+str(strand)
				elif name_format == "simple_numbering":
					try:
						cursor.execute("SELECT monomer_nucleotide,monomer_index FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(nucleotide,index) = info
					mono_counter += 1
					name = str(mono_counter)"""
				results.write(">%s\n" %name)		
				phase_counter = False
				duo = False
				end_monomer = True
				position_index = 0
				previous = None
				begin = None
				if phase == 1:
					try:
						cursor.execute("SELECT monomer_sequence FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_seq = cursor.fetchone()
					cursor.execute('commit')
					monomer_unique = monomer_seq['monomer_sequence']
					sequence2 = '\n'.join((monomer_unique)[pos:pos+60] for pos in range(0,len(monomer_unique), 60))
					results.write( "%s\n" %sequence2 )
				else:
					for base in index:
						if base >= phase and not phase_counter:
							for i in range(position_index,position_previous,-1):
								if index[i] <= 0:
									begin = i
							if begin is None:
								begin = position_index
							phase_counter = True
						if base < previous and base > 0:
							duo = True
						if base >= phase and duo and end_monomer:
							sequence2 = '\n'.join((nucleotide)[pos:pos+60] for pos in range(begin, position_index + 1, 60))
							results.write( "%s\n" %sequence2 )
							end_monomer = False
						if base > 0:
							previous = base
							position_previous = position_index
						position_index += 1
			sequence_output = results.getvalue()
			if fasta_output == 'fasta.gzip':
				response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				filename = "%s.fasta.gz" %retrieve_type
			else:
				filename = "%s.fasta" %retrieve_type
				response=Response(sequence_output)
		else:
			for identifiant in list_encode:
				separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
				if name_format == "database_name":
					try:
						cursor.execute("SELECT monomer_name,monomer_nucleotide,monomer_begin,monomer_end,monomer_strand,sequence_id FROM Monomer WHERE monomer_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(name,nucleotide,begin,end,strand,sequence_id) = info
				elif name_format == "chr_begin_end_strand":
					try:
						cursor.execute("SELECT monomer_nucleotide,monomer_begin,monomer_end,monomer_strand,sequence_id FROM Monomer WHERE monomer_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(nucleotide,begin,end,strand,sequence_id) = info
					try:
						cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					chromosome_name = cursor.fetchone()
					cursor.execute('commit')
					name = chromosome_name[0]+"_"+str(begin)+"_"+str(end)+"_"+str(strand)
				elif name_format == "simple_numbering":
					try:
						cursor.execute("SELECT monomer_nucleotide,monomer_begin,monomer_end,monomer_strand,sequence_id FROM Monomer WHERE monomer_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(nucleotide,begin,end,strand,sequence_id) = info
					mono_counter += 1
					name = str(mono_counter)
				try:
					cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()
				cursor.execute('commit')
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name[0], separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				"""if 'families' in monomer_info:
					family_results = set()
					for families_name in monomer_info['families']:
						family_results.add(families_name['name'])
					if len(family_results) == 1:
						family_string = str(family_results)
					else:
						family_string = '-'.join(family_results)
					results.write("%s%s" % (family_string, separators[csv_output]))"""
				results.write("%s\n" % nucleotide)
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.txt" %retrieve_type
	elif coll[retrieve_type] == Block:
		if format_output == 'format_fasta':
			for identifiant in list_encode:
				try:
					cursor.execute("SELECT block_name,block_nucleotide FROM Block WHERE block_id = %s ;",(identifiant,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				(name,nucleotide) = info
				results.write(">%s\n" %name)
				#phase_counter = True
				#position_index = 0
				#for base in index:
				#if (position_index + 1) == phase and phase_counter:
				sequence2 = '\n'.join((nucleotide)[pos:pos+60] for pos in range(0, len(nucleotide), 60))
				#phase_counter = False
				#position_index += 1	 
				results.write( "%s\n" %sequence2 )
			sequence_output = results.getvalue()
			if fasta_output == 'fasta.gzip':
				response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				filename = "%s.fasta.gz" %retrieve_type
			else:
				filename = "%s.fasta" %retrieve_type
				response=Response(sequence_output)
		else:
			for identifiant in list_encode:
				separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
				if name_format == "database_name":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(name,nucleotide,begin,end,strand,sequence_id) = info
				#chromosome_name = info[0].split('_',1)[0]
				elif name_format == "chr_begin_end_strand":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(block_name,nucleotide,begin,end,strand,sequence_id) = info
					try:
						cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					chromosome_name = cursor.fetchone()
					cursor.execute('commit')
					name = chromosome_name[0]+"_"+str(begin)+"_"+str(end)+"_"+str(strand)			
				elif name_format == "simple_numbering":
					try:
						cursor.execute("SELECT block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(nucleotide,begin,end,strand,sequence_id) = info
					block_counter += 1
					name = str(block_counter)
				try:
					cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()
				cursor.execute('commit')
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name, separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				results.write("%s\n" % nucleotide)
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.txt" %retrieve_type				
	response.headers.add('Content-type', 'application/octet-stream')
	response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	return response

@app.route("/alphasat/accueil" , methods=['POST', 'GET'])
def accueil():
	return render_template("alphasat_accueil.html")

@app.route("/alphasat/contact", methods=['POST', 'GET'])
def contact():
	return render_template("contact.html")
	
@app.route("/alphasat/help_and_documentation", methods=['POST', 'GET'])
def documentation():
	return render_template("help_and_documentation.html")
	
if __name__ == "__main__":
	app.run(port=PORT, host="0.0.0.0", debug=True)

