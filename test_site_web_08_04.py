from flask import *
import io
import json
import ipdb
import gzip
import json
import base64
import re
import time
import psycopg2
import psycopg2.extras
import pandas
from psycopg2.extensions import AsIs
from bson import Binary, Code
from bson.json_util import dumps
from bson.json_util import loads
from sort_functions import (atoi,natural_keys)
import svgwrite
import mimetypes
app = Flask(__name__)
PORT = 8000
from operator import itemgetter
"""Connexion a la base de donnees"""
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
taxon_id_list = []
seq_id_list = []
block_id_list = []
family_id_list = []
monomer_id_list = []
monomers_list = []
#def getVariables(args):
	
@app.route("/")
def hello():
	return ("Hello Loïc !!!!")


@app.route("/list/Taxon", methods=['POST', 'GET'])
#affiche tous les taxons de la base
def taxon_all():
	T1 = time.time()
	T2 = time.clock()
	F1 = time.time()
	try:
		cursor.execute("SELECT taxon_id, taxon_name from Taxon ;")
	except psycopg2.Error as e:
		print (e.pgerror)
	FF1 = time.time()
	print( "temps de la requete 1 %f" %(FF1 - F1))
	results = cursor.fetchall()
	cursor.execute('commit')
	resultsf = dict()
	taxon_name = list()
	for i in results:
		identifiant, name = i
		taxon_name.append(name)
		resultsf[name] =  identifiant
	taxon_encode = dumps(resultsf)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction list/Taxon : %f" %(FT1 - T1))
	print("Temps CPU de la fonction list/Taxon : %f" %(FT2 - T2))
	return render_template("filter_page.html", taxon_name = taxon_name, taxon_dict = taxon_encode)
			
@app.route("/test",methods=['GET'])
def test():
	#~ aa = [{'id':'aa','text':'aa','state':'closed'},{'id':'bb','text':'bb','state':'opened'}]
	#~ df = pandas.read_sql("SELECT Sequence.sequence_id,Sequence.chromosome_id,Block.block_id,Monomer.monomer_id FROM Sequence NATURAL JOIN Block NATURAL JOIN Monomer;",conn)
	#~ #print(df)
	#~ stats = df.groupby(['chromosome_id','sequence_id']).reset_index()
	#~ print(stats)
	block_count_query = ' '.join([
		'SELECT Taxon.taxon_id,Taxon.taxon_name AS taxon_name,Sequence.chromosome_id,Sequence.sequence_id,Sequence.sequence_name AS sequence_name,count(Block.block_id) AS block_count',
		'FROM Taxon',
		'LEFT JOIN Sequence ON Taxon.taxon_id=Sequence.taxon_id',
		'LEFT JOIN Block ON Sequence.sequence_id=Block.sequence_id',
		'GROUP BY Taxon.taxon_id,Sequence.sequence_id'
	])

	monomer_count_query = ' '.join([
		'SELECT Sequence.sequence_id,Block.block_id,count(Monomer.monomer_id) AS monomer_count',
		'FROM Sequence',
		'LEFT JOIN Block ON Block.sequence_id=Sequence.sequence_id',
		'LEFT JOIN Monomer ON Monomer.block_id=Block.block_id',
		'GROUP BY Sequence.sequence_id, Block.block_id'
	])

	#~ co =sqlite3.connect('/home/julien/alphasat')
	#~ cu = co.cursor()
	cursor.execute(block_count_query)
	#~ print('Block count cursor')
	for r in cursor:
		print(r)
	cursor.execute(monomer_count_query)
	#~ print('Monomer count cursor')
	for r in cursor:
		print(r)	
		
	block_count_df = pandas.read_sql(block_count_query,conn)
	block_count_df.reindex(index=block_count_df['sequence_id'].values)
	print('Block count dataframe')
	print(block_count_df)
	monomer_count_df = pandas.read_sql(monomer_count_query,conn)
	monomer_count_df.reindex(index=block_count_df['sequence_id'].values)
	print('Monomer count dataframe')
	print(monomer_count_df)
	monomer_sum_s = monomer_count_df.groupby('sequence_id')['monomer_count'].sum()
	monomer_sum_s.reset_index()
	print('Monomer sum serie')
	print(monomer_sum_s)
	block_count_df['monomer_sum'] = monomer_sum_s
	global_count_df = block_count_df.merge(monomer_count_df,on='sequence_id',how='left')
	global_count_df.reindex(index=global_count_df['sequence_id'].values)
	print("essai")
	print(global_count_df.loc[3,'sequence_id'])
	print(monomer_sum_s.iloc[3])
	print('global count dataframe')
	print(global_count_df)
	return 'Ok'

@app.route("/tree",methods=['GET'])
def tree():
	tables = ['taxon', 'chromosome', 'sequence']
	#taxonSelection, , , = getVariables(request.args)
	oid = request.args.get('id', None)
	result_taxons = dict()
	taxon_name = list()
	element_name = list()
	result_element = dict()
	dico_test = list()
	results2 = list()
	#Tree vide
	if oid is None:
		sequences_number = 0
		blocks_number = 0
		monomers_number = 0
		try:
			cursor.execute("SELECT taxon_id, taxon_name, number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id IS NULL;")
			#cursor.execute("SELECT supertaxon_id from Taxon where supertaxon_id IS NOT NULL;")
		except psycopg2.Error as e:
			print (e.pgerror)
		results = cursor.fetchall()
		cursor.execute('commit') 		
		#Pas de supertaxon racine
		if not results:
			try:
				cursor.execute("SELECT taxon_id, taxon_name, number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id IS NOT NULL;")
			except psycopg2.Error as e:
				print (e.pgerror)
			results = cursor.fetchall()
			cursor.execute('commit')
		for result in results:
			try:
				cursor.execute("SELECT number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id = %s;",(result['taxon_id'],))
			except psycopg2.Error as e:
				print (e.pgerror)
			for i in cursor:
				result['number_sequences'] = int(result['number_sequences']) + int(i['number_sequences'])
				result['number_blocks'] = int(result['number_blocks']) + int(i['number_blocks'])
				result['number_monomers'] = int(result['number_monomers']) + int(i['number_monomers'])
			result_taxons[result['taxon_id']] =  [result['taxon_name'],result['number_sequences'],result['number_blocks'],result['number_monomers']]
			
		#Envoie des résultats
		for i in result_taxons:
			dico_test.append({'id':'taxon_'+str(i),'text':result_taxons[i][0]+" ( sequences : "+str(result_taxons[i][1])+" , blocks : "+str(result_taxons[i][2])+" , monomers : "+str(result_taxons[i][3])+" )","iconCls":"icon-blank", 'state':'closed'})
	#Tree non vide
	else:
		sequences_number = 0
		blocks_number = 0
		monomers_number = 0
		info = oid.split("_")
		table = info[0]
		table_id = info[1]
		if table in tables:
			k = tables.index(table)
			if k < (len(tables) - 1):
				childrentable = tables[k+1]
				#~ try:
					#~ cursor.execute("SELECT %(id)s_id, %(name)s_name from %(table)s ;",{"table" : AsIs(childrentable), "id" : AsIs(childrentable), "name" : AsIs(childrentable)})
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
			#Condition taxon
			if table == "taxon":
				#Condition si taxon est un supertaxon
				try:
					cursor.execute("SELECT taxon_id, taxon_name, number_sequences, number_blocks, number_monomers from Taxon WHERE supertaxon_id = %s;",(table_id,))
				except psycopg2.Error as e:
					print (e.pgerror)
				results = cursor.fetchall()
				cursor.execute('commit')
				childrentable = "taxon"
				k = 0
				#Condition pas un supertaxon
				if not results:
					try:
						cursor.execute("SELECT chromosome_id from Sequence WHERE taxon_id = %s;",(table_id,))
					except psycopg2.Error as e:
						print (e.pgerror)
					chromosomes_id = cursor.fetchall()
					for chromosome in chromosomes_id:
						try:
							cursor.execute("SELECT chromosome_name,number_sequences,number_blocks,number_monomers from Chromosome WHERE chromosome_id = %s;",(chromosome['chromosome_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						results = cursor.fetchall()
						cursor.execute('commit')
						childrentable = "chromosome"
						k = 1			
						for i in results:
						#~ try:
							#~ cursor.execute("SELECT count(DISTINCT block_id) from Block WHERE sequence_id = %s;",(i["sequence_id"],))
						#~ except psycopg2.Error as e:
							#~ print (e.pgerror)
						#~ number_blocks = cursor.fetchone()
						#~ cursor.execute('commit')
						#~ try:
							#~ cursor.execute("SELECT count(DISTINCT monomer_id) from Monomer WHERE sequence_id = %s;",(i["sequence_id"],))
						#~ except psycopg2.Error as e:
							#~ print (e.pgerror)
						#~ number_monomers = cursor.fetchone()
						#~ cursor.execute('commit')											
							result_element[chromosome['chromosome_id']] =  [i['chromosome_name'],i['number_sequences'], i['number_blocks'], i['number_monomers']]
					for i in result_element:
						dico_test.append({'id':childrentable+"_"+str(i),'text':result_element[i][0]+" ( sequences : "+str(result_element[i][1])+" , blocks : "+str(result_element[i][2])+" , monomers : "+str(result_element[i][3])+" )", "iconCls":"icon-blank",'state':'opened'}) #+" ( blocks: "+str(result_element[i][1])+" ,monomers: "+str(result_element[i][2])+")"
				else:
					for result in results:
						try:
							cursor.execute("SELECT number_sequences, number_blocks, number_monomers from Taxon where supertaxon_id = %s;",(result['taxon_id'],))
						except psycopg2.Error as e:
							print (e.pgerror)
						for i in cursor:
							result['number_sequences'] = int(result['number_sequences']) + int(i['number_sequences'])
							result['number_blocks'] = int(result['number_blocks']) + int(i['number_blocks'])
							result['number_monomers'] = int(result['number_monomers']) + int(i['number_monomers'])
						result_element[result['taxon_id']] =  [result['taxon_name'],result['number_sequences'],result['number_blocks'],result['number_monomers']]
					for i in result_element:
						dico_test.append({'id':childrentable+"_"+str(i),'text':result_element[i][0]+" ( sequences : "+str(result_element[i][1])+" , blocks : "+str(result_element[i][2])+" , monomers : "+str(result_element[i][3])+" )", "iconCls":"icon-blank",'state':'closed'})
			#~ elif table == "chromosome":
				#~ print("chromosome")
				#~ name = info[2]
				#~ print(name)
				#~ print(table_id)
				#~ try:
					#~ cursor.execute("SELECT sequence_id, sequence_name from Sequence WHERE taxon_id = %s AND chromosome_name = %s;",(table_id,name,))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ results = cursor.fetchall()
				#~ print(results)
				#~ cursor.execute('commit')
				#~ childrentable = "sequence"
				#~ for i in results:
					#~ identifiant, name = i
					#~ element_name.append(name)
					#~ result_element[name] =  identifiant
				#~ for i,j in zip(result_element.keys(),result_element.values()):
					#~ dico_test.append({'id':childrentable+"_"+str(j),'text':i, "iconCls":"icon-blank",'state':'open'})
	#dico_sort = sorted(dico_test,key=lambda k:k['text'])
	dico_test.sort(key=lambda k: natural_keys(k['text']))
	return json.dumps(dico_test)
	
	
@app.route("/family_tree", methods=['POST', 'GET'])
def family_tree():
	selection = request.args.getlist('id')
	print("selection")
	print(selection)
	seq_id = []
	families_id = []
	selection = list(map(int,selection))
	print(selection)
	try:
		cursor.execute("SELECT monomer_id FROM Monomer WHERE block_id = ANY(%s);",(selection,))
	except psycopg2.Error as e:
		print (e.pgerror)
	monomers_id = cursor.fetchall()
	cursor.execute('commit')
	print("monomers_id")
	print(monomers_id)
	for monomer in monomers_id:
		try:
			cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s;",(int(monomer['monomer_id']),))
		except psycopg2.Error as e:
			print (e.pgerror)
		for family in cursor:
			print(family)
			families_id.append(family['family_id'])
		cursor.execute('commit')
	#~ print("families_id")
	#~ print(families_id)
	families_id = list(map(int,families_id))
	try:
		cur.execute("SELECT family_id,family_name,sequence_id FROM Family WHERE family_id = ANY(%s);",(families_id,))
	except psycopg2.Error as e:
		print (e.pgerror)
	identifiant = cur.fetchall()
	for i in identifiant:
		seq_id.append(i)
	cursor.execute('commit')
	return json.dumps(seq_id)
	
@app.route("/test_form",methods=['GET','POST'])
def data_form():
	#~ test = request.args.getlist('ff')
	#~ test2 = request.form.getlist('ff')
	#~ test3 = request.values.get('ff')
	#test4 = request.form['ff']
	#test5 = request.form[test]
	#~ test6 = request.form.getlist('test')
	data = dict()
	#~ print(request.form)
	
	for key in request.form:
		data[key] = request.form[key]
		print(type(request.form[key]))
	#~ print(data)
	#~ print(type(data))
	#~ print(data.keys())
	#~ print(data['block_length'])
	#block_length = request.args.get('block_length', None)
	#~ print(block_length)
	#phase = request.args.get('phase', None)	
	#monomer_length = request.args.get('monomer_length', None)
	#monomer_position = request.args.get('monomer_begin_end', None)
	#dico_form = { 'block_length' : block_length,'phase':phase,'monomer_length':monomer_length,'monomer_position':monomer_position }
	#~ print(dico_form)
	#~ print("coucou data_form")
	#return "ok"
	#return json.dumps(dico_form)
	#~ form_data = request.args.getlist('selection[]')
	#~ block_length = None
	#~ monomer_length = None
	#~ monomer_positions = None
	#~ phase = None
	return render_template("filter_page.html", block_length = data['block_length'], monomer_length = data['monomer_length'], monomer_position = data['monomer_begin_end'], phase = data['phase'])		

@app.route("/search_form", methods=['POST', 'GET'])
def search():
	#Renvoie le formulaire
	return render_template("search.html")

@app.route("/results",methods=['GET','POST'])
def results_form():
	phase = request.args.get("_phase")
	list_encode = request.args.get("monomer_list")
	retrieve_type = "toto"
	return render_template("results.html", list_encode = list_encode, retrieve_type = retrieve_type, phase = phase)

def get_max_length(data):
	mx=0
	for chr in data:
		if data[chr]['length'] > mx:
			mx=data[chr]['length']
	return(mx)
	
@app.route("/results/files",methods=['GET','POST'])
def files_results():
	#parametres
	mimetypes.add_type('image/svg+xml', '.svg', True)
	mimetypes.add_type("image/svg+xml", ".svgz", True)
	global taxon_id_list
	print("tata")
	print(taxon_id_list)
	global monomer_id_list
	list_encode = monomer_id_list
	phase = request.form.get('monomer_phase',None)
	#~ print(str(phase))
	if isinstance(phase,int):
		phase = int(phase)
	else:
		phase = 1
	#~ print(phase)
	#~ print(type(phase))
	#retrieve_type = request.form.getlist("retrieve_type")[0]
	#variable
	format_output = request.form.getlist("format")[0]
	fasta_output = request.form.getlist("output")[0]
	csv_output = request.form.getlist("csv")[0]
	name_option = request.form.getlist("name_option")
	name_format = request.form.getlist("name_format")
	separator_format = request.form.getlist("separator_format")[0]
	results = io.StringIO()
	family_string = str()
	coll = {"monomers" : 'Monomer', "blocks" : 'Block'}
	mono_counter = 0
	block_counter = 0
	liste_name = list()
	##temp
	retrieve_type = "monomers"
	if coll[retrieve_type] == 'Monomer':
		if format_output == 'format_fasta':
			for identifiant in list_encode:
				name = monomer_name(name_format,identifiant,separator_format)
				try:
					cursor.execute("SELECT monomer_index,monomer_nucleotide,monomer_length FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				index = info['monomer_index']
				nucleotide = info['monomer_nucleotide']
				length = info['monomer_length']
				results.write(">%s\n" %name)		
				if phase == 1:
					try:
						cursor.execute("SELECT monomer_sequence FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_seq = cursor.fetchone()
					cursor.execute('commit')
					monomer_unique = monomer_seq['monomer_sequence']
					sequence2 = '\n'.join((monomer_unique)[pos:pos+60] for pos in range(0,len(monomer_unique), 60))
					results.write( "%s\n" %sequence2 )
				else:
					begin = None
					end = None
					ii = 0
					while ii < (len(index)-1) and index[ii] < phase:
						begin = ii + 1
						ii += 1
					jj = begin - 1
					while jj >= 0 and index[jj] <= 0:
						begin = jj
						jj -= 1
					yy = length
					while yy < (len(index)-1) and index[yy] < phase:
						end = yy
						yy += 1
					zz = end
					while zz >= 0  and index[zz] <= 0:
						end = zz
						zz -= 1
					monomer = nucleotide[begin:end+1]
					sequence2 = '\n'.join(monomer[pos:pos+60] for pos in range(0, len(monomer),60))
					results.write( "%s\n" %sequence2 )					
			sequence_output = results.getvalue()
			if fasta_output == 'fasta.gzip':
				response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				filename = "%s.fasta.gz" %retrieve_type
			else:
				filename = "%s.fasta" %retrieve_type
				response=Response(sequence_output)
		elif format_output == 'format_csv':
			#Fichier CSV
			for identifiant in list_encode:
				name = monomer_name(name_format,identifiant,separator_format)
				separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
				try:
					cursor.execute("SELECT monomer_nucleotide,monomer_begin,monomer_end,monomer_strand,sequence_id FROM Monomer WHERE monomer_id = %s ;", (identifiant,))
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				(nucleotide,begin,end,strand,sequence_id) = info				
				try:
					cursor.execute("SELECT chromosome_id FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_id = cursor.fetchone()
				try:
					cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s ;",(chromosome_id[0],) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()			
				cursor.execute('commit')
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name[0], separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				"""if 'families' in monomer_info:
					family_results = set()
					for families_name in monomer_info['families']:
						family_results.add(families_name['name'])
					if len(family_results) == 1:
						family_string = str(family_results)
					else:
						family_string = '-'.join(family_results)
					results.write("%s%s" % (family_string, separators[csv_output]))"""
				results.write("%s\n" % nucleotide)
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.txt" %retrieve_type
			response.headers.add('Content-type', 'application/octet-stream')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
		elif format_output == 'format_svg':
			#Fichier SVG
			for tx in taxon_id_list:
				data={}
				monomer_data = {}
				try:
					cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				seq_id = cursor.fetchall()
				for i in seq_id:
					monomer_data[i['sequence_name']] = { 'monomers': [] }
					parse = re.match(r"(chr)([A-Z]|[a-z]|[0-9])+$", i['sequence_name'])
					if parse:
						print(i['sequence_name'])
						try:
							cursor.execute("SELECT sequence_cytoband FROM Sequence WHERE sequence_id = %s ;",(i['sequence_id'],) )
						except psycopg2.Error as e:
							print (e.pgerror)
						cytoband_list = cursor.fetchone()
						try:
							cursor.execute("SELECT block_id FROM Block WHERE sequence_id = %s ;",(i['sequence_id'],) )
						except psycopg2.Error as e:
							print (e.pgerror)
						blocks_id = cursor.fetchall()
						try:
							cursor.execute("SELECT monomer_id,monomer_begin_sequence,monomer_end_sequence,monomer_length FROM Monomer WHERE block_id = ANY(%s) ;",(blocks_id,) )
						except psycopg2.Error as e:
							print (e.pgerror)												
						monomers = cursor.fetchall()
						for j in monomers:
							monomers_infos = {'monomer_id' : j['monomer_id'],'monomer_begin_sequence' : j['monomer_begin_sequence'],'monomer_end_sequence' : j['monomer_end_sequence'],'monomer_length' : j['monomer_length']}
							monomer_data[i['sequence_name']]['monomers'].append(monomers_infos)
						#~ #for i in cursor:
							#~ #print(i)
							#~ #print(type(i))
							#~ #if i:
								#~ #cytoband_list = list(i[0])
							#~ #print("cytoband")
							#~ #print(cytoband_list)
							#~ #print(type(cytoband_list))
						if cytoband_list[0] is not None:
							for row in cytoband_list['sequence_cytoband']:
								#~ #print(row)
								if row[0] not in data:
									data[row[0]]={'length': 0, 'bands': []}
									#~ #print(row[0])
								if row[0] in data:
									if int(row[2]) > int(data[row[0]]['length']):
										data[row[0]]['length']=int(row[2])
										bd={'start': int(row[1]), 'end': int(row[2]), 'name': row[3], 'type': row[4]}
										data[row[0]]['bands'].append(bd)
										#~ #print(data[row[0]]['length'])
			width=18000
			heigh=6000
			heigh_plus=1000
			nb_chr=len(data)
			max_length=get_max_length(data)
			#~ #print(max_length)
			filename = "%s.svg" %retrieve_type
			svg_document = svgwrite.Drawing(filename = filename,size = (width, heigh+heigh_plus),profile='full')
			#~ #Fraction de la largeur pour chaque chromosome
			x=width/nb_chr
			#~ #Ratio nb de pixel par pb
			y=heigh/max_length
			nb=0
			chromosomes = list(data.keys())
			chromosomes.sort(key=lambda k: natural_keys(k))
			for chro in chromosomes:
				#~ #print(chr)
				ins=(nb*x,  heigh - data[chro]['length']*y)
				siz=(x/2, data[chro]['length']*y)
				#~ #Ajouter un element
				svg_document.add(svg_document.rect(insert = ins,
												#~ #largeur/hauteur
												size = siz,
												#~ #largeur de trait
												stroke_width = "1",
												#~ #couleur
												stroke = "black",
												#~ #remplissage en blanc
												fill = "rgb(255,255,255)"))
				g = svg_document.g(style="font-size:200;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:1;fill:none")
				g.add(svg_document.text(chro[3:],
												insert = (nb*x + x/8, heigh + 400),
												fill = 'black'
												))
				svg_document.add(g)
				#~ #svg_document.add(svg_document.text.TextArea(chr[3:],x = nb*x, y =  heigh + 800, width = x, height = (nb*x, heigh + 400) - (heigh - data[chr]['length']*y)))
				st=heigh - data[chro]['length']*y
				for bd in data[chro]['bands']:
					ins=(nb*x, st+bd['start']*y+0.5*heigh_plus*y)
					siz=(x/2, (bd['end']-bd['start'])*y)
					#~ #print(siz)
					#~ #print(ins)
					#~ #print("bd")
					#~ #print(bd)
					#~ #noir
					col="rgb(255,255,255)"
					if bd['type'] == 'gneg':
						#~ #blanc
						col="rgb(255,255,255)"
						#~ #Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gpos100':
						#~ #noir
						col='rgb(0, 0, 0)'
						#~ #Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gpos75':
						#~ #gris foncé
						col='rgb(169, 169, 169)'
						#~ #Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gpos50':
						#~ #gris clair
						col='rgb(192, 192, 192)'
						#~ #Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gpos25':
						#gris très clair
						col='rgb(211, 211, 211)'
						#~ #Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'acen':
						#bleu
						col='rgb( 0, 0, 255)'
						#~ #Ajouter un rectangle pour les bandes
						ins=((nb*x)+((x/2)*(1/4)), st+bd['start']*y+0.5*heigh_plus*y)
						siz = (x/4, (bd['end']-bd['start'])*y)
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gvar':
						#blanc
						col='rgb( 255, 255, 255)'
						#~ #Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'stalk':
						#~ #rouge
						col='rgb( 200, 0, 0)'
						#~ #Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
				#~ #print(data[chr])
				for seq in monomer_data:
					if seq == chro:
						for mono in monomer_data[seq]['monomers']:
							color='rgb( 0, 255, 10)'
							#~ triangle = svg_document.polygon([((nb*x-100),(int(mono['monomer_end_sequence'])*y)) , ((nb*x),(int(mono['monomer_end_sequence'])*y)) , ((nb*x),(int(mono['monomer_end_sequence'])*y+100))], id='triangle', stroke='red', fill='red')
							#~ g = svgwrite.container.Group(transform='rotate(45)')
							#~ svg_document.add(g)
							#~ g.add(triangle)
							ins=((nb*x), (st+int(mono['monomer_begin_sequence'])*y+0.5*heigh_plus*y))
							siz=((x/2), ((int(mono['monomer_end_sequence'])-int(mono['monomer_begin_sequence']))*y)*1000)
							svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "green",
															fill = color))
				nb=nb+1
			#svg_document.save()
			#sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			###test
			#~ ins= (1000,2000)
			#~ siz = (1000,500)
			#~ width=18000
			#~ heigh=6000
			#~ heigh_plus=1000
			#~ filename = "%s.svg" %retrieve_type
			#~ svg_document = svgwrite.Drawing(filename = filename,size = (width, heigh+heigh_plus),profile='full')
			#~ svg_document.add(svg_document.rect(insert = ins,
												#~ #largeur/hauteur
												#~ size = siz,
												#~ #largeur de trait
												#~ stroke_width = "1",
												#~ #couleur
												#~ stroke = "red",
												#~ #remplissage en blanc
												#~ fill = "rgb(0,0,255)"))
			###fin
			svg_file = svg_document.tostring()
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response = Response(svg_file)		
			response.headers.add('Content-type', 'image/svg+xml')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	elif coll[retrieve_type] == Block:
		if format_output == 'format_fasta':
			for identifiant in list_encode:
				try:
					cursor.execute("SELECT block_name,block_nucleotide FROM Block WHERE block_id = %s ;",(identifiant,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				(name,nucleotide) = info
				results.write(">%s\n" %name)
				sequence2 = '\n'.join((nucleotide)[pos:pos+60] for pos in range(0, len(nucleotide), 60))
				results.write( "%s\n" %sequence2 )
			sequence_output = results.getvalue()
			if fasta_output == 'fasta.gzip':
				response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				filename = "%s.fasta.gz" %retrieve_type
			else:
				filename = "%s.fasta" %retrieve_type
				response=Response(sequence_output)
		else:
			for identifiant in list_encode:
				separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
				if name_format == "database_name":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(name,nucleotide,begin,end,strand,sequence_id) = info
				#chromosome_name = info[0].split('_',1)[0]
				elif name_format == "chr_begin_end_strand":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(block_name,nucleotide,begin,end,strand,sequence_id) = info
					try:
						cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					chromosome_name = cursor.fetchone()
					cursor.execute('commit')
					name = chromosome_name[0]+"_"+str(begin)+"_"+str(end)+"_"+str(strand)			
				elif name_format == "simple_numbering":
					try:
						cursor.execute("SELECT block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(nucleotide,begin,end,strand,sequence_id) = info
					block_counter += 1
					name = str(block_counter)
				try:
					cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()
				cursor.execute('commit')
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name, separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				results.write("%s\n" % nucleotide)
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.txt" %retrieve_type
			response.headers.add('Content-type', 'application/octet-stream')
			response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	return response

@app.route("/caryotype", methods=['POST', 'GET'])
def caryotype():
	mimetypes.add_type('image/svg+xml', '.svg', True)
	mimetypes.add_type("image/svg+xml", ".svgz", True)
	mimetypes.add_type('image/svg+xml', '.svg')
	global monomer_id_list
	list_encode = monomer_id_list
	global taxon_id_list
	#~ print("tata")
	#~ print(taxon_id_list)
	format_output = request.form.getlist("format")[0]
	retrieve_type = "monomers"
	if format_output == 'format_svg':
			#Fichier SVG
			#~ print("toto")
			#~ print(taxon_id_list)
			#~ print(type(taxon_id_list))
			for tx in taxon_id_list:
				data={}
				monomer_data = {}
				try:
					cursor.execute("SELECT sequence_name,sequence_id FROM Sequence WHERE taxon_id = %s ;",(tx,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				seq_id = cursor.fetchall()
				for i in seq_id:
					parse = re.match(r"(chr)([A-Z]|[a-z]|[0-9])+$", i['sequence_name'])
					if parse:
						print(i['sequence_name'])
						try:
							cursor.execute("SELECT sequence_cytoband FROM Sequence WHERE sequence_id = %s ;",(i['sequence_id'],) )
						except psycopg2.Error as e:
							print (e.pgerror)
						cytoband_list = cursor.fetchone()
						try:
							cursor.execute("SELECT block_id FROM Block WHERE sequence_id = %s ;",(i['sequence_id'],) )
						except psycopg2.Error as e:
							print (e.pgerror)
						blocks_id = cursor.fetchall()
						try:
							cursor.execute("SELECT monomer_id,monomer_begin_sequence,monomer_end_sequence,monomer_length FROM Monomer WHERE block_id = ANY(%s) ;",(blocks_id,) )
						except psycopg2.Error as e:
							print (e.pgerror)												
						monomers = cursor.fetchall()
						for j in monomers:
							monomer_data[i['sequence_name']] = { 'monomers': [] }
							monomers_infos = {j['monomer_id'],j['monomer_begin_sequence'],j['monomer_end_sequence'],j['monomer_length']}
							monomer_data[i['sequence_name']]['monomers'].append(monomers_infos)
						#~ print("bonjour")
						#~ print(cytoband_list[0])
						#~ print(cytoband_list['sequence_cytoband'])
						#~ print(type(cytoband_list))
						#~ print(type(cytoband_list[0]))
						#~ for i in cursor:
							#~ print(i)
							#~ print(type(i))
							#~ if i:
								#~ cytoband_list = list(i[0])
							#~ print("cytoband")
							#~ print(cytoband_list)
							#~ print(type(cytoband_list))
						if cytoband_list[0] is not None:
							for row in cytoband_list['sequence_cytoband']:
								#~ print("coucou")
								#~ print(row)
								#~ print(type(row))
								#print(row)
								if row[0] not in data:
									data[row[0]]={'length': 0, 'bands': []}
									#print(row[0])
								if row[0] in data:
									if int(row[2]) > int(data[row[0]]['length']):
										data[row[0]]['length']=int(row[2])
										bd={'start': int(row[1]), 'end': int(row[2]), 'name': row[3], 'type': row[4]}
										data[row[0]]['bands'].append(bd)
										#print(data[row[0]]['length'])
			width=1800
			heigh=600
			heigh_plus=100
			nb_chr=len(data)
			max_length=get_max_length(data)
			#print(max_length)
			filename = "%s.svg" %retrieve_type
			svg_document = svgwrite.Drawing(filename = filename,size = (width, heigh+heigh_plus),profile='full')
			#Fraction de la largeur pour chaque chromosome
			x=width/nb_chr
			#Ratio nb de pixel par pb
			y=heigh/max_length
			nb=0
			chromosomes = list(data.keys())
			chromosomes.sort(key=lambda k: natural_keys(k))
			for chro in chromosomes:
				#~ print(chr)
				ins=(nb*x,  heigh - data[chro]['length']*y)
				siz=(x/2, data[chro]['length']*y)
				#Ajouter un element
				svg_document.add(svg_document.rect(insert = ins,
												#largeur/hauteur
												size = siz,
												#largeur de trait
												stroke_width = "1",
												#couleur
												stroke = "black",
												#remplissage en blanc
												fill = "rgb(255,255,255)"))
				g = svg_document.g(style="font-size:200;font-family:Comic Sans MS, Arial;font-weight:none;font-style:none;stroke:black;stroke-width:1;fill:none")
				g.add(svg_document.text(chro[3:],
												insert = (nb*x, heigh + 400),
												fill = 'black'
												))
				svg_document.add(g)
				#svg_document.add(svg_document.text.TextArea(chr[3:],x = nb*x, y =  heigh + 800, width = x, height = (nb*x, heigh + 400) - (heigh - data[chr]['length']*y)))
				st=heigh - data[chro]['length']*y
				for bd in data[chro]['bands']:
					ins=(nb*x, st+bd['start']*y+0.5*heigh_plus*y)
					siz=(x/2, (bd['end']-bd['start'])*y)
					#~ print(siz)
					#~ print(ins)
					#~ print("bd")
					#~ print(bd)
					#noir
					col="rgb(255,255,255)"
					if bd['type'] == 'gneg':
						#blanc
						col="rgb(255,255,255)"
						#Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gpos100':
						#noir
						col='rgb(0, 0, 0)'
						#Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gpos75':
						#gris foncé
						col='rgb(169, 169, 169)'
						#Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gpos50':
						#gris clair
						col='rgb(192, 192, 192)'
						#Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gpos25':
						#gris très clair
						col='rgb(211, 211, 211)'
						#Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'acen':
						#bleu
						col='rgb( 0, 0, 255)'
						#Ajouter un rectangle pour les bandes
						ins=((nb*x)+((x/2)*(1/4)), st+bd['start']*y+0.5*heigh_plus*y)
						siz = (x/4, (bd['end']-bd['start'])*y)
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'gvar':
						#blanc
						col='rgb( 255, 255, 255)'
						#Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
					elif bd['type'] == 'stalk':
						#rouge
						col='rgb( 200, 0, 0)'
						#Ajouter un rectangle pour les bandes
						svg_document.add(svg_document.rect(insert = ins,
															size = siz,
															stroke_width = "1",
															stroke = "black",
															fill = col))
				#print(data[chr])
				for seq in monomers:
					print("qsepflsfpgdpflgdpfllgdpflgdohkdglhkg,,hkdghsdfs")
					if seq == chro:
						for mono in monomers[seq]['monomers']:
							triangle = svg_document.polygon([((nb*x)-100*1000,int(mono['monomer_end_sequence'])*y*1000), (nb*x*1000,int(mono['monomer_end_sequence'])*y*1000), (nb*x*1000,(int(mono['monomer_end_sequence'])*y*1000))-100], id='triangle', stroke='red', fill='red')
							g = svgwrite.container.Group(transform='rotate(45)')
							svg_document.add(g)
							g.add(triangle)
				nb=nb+1
			#svg_document.save()
			#sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			svg_file = svg_document.tostring()
			response=Response(svg_file)		
	response.headers.add('Content-type', 'image/svg+xml')
	response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	#return response
	#return render_template("caryotype.html", svg_file = svg_file)
	return render_template("caryotype.html", svg = svg_file)
@app.route("/datagrid_sequences", methods=['POST', 'GET'])
def sequences_data():
	global taxon_id_list
	global seq_id_list
	seq_id_list = []
	#selection = request.form('selection')
	selection = request.args.getlist('selection[]')
	search = request.args.get('search')
	#new_selection = selection[1:len(selection)-1].replace('"','')
	liste_chr = list()
	liste_taxon = list()
	seq_id = []
	taxon_list = []
	#~ print("sequences")
	#input_data = new_selection.split(",")
	for i in selection:
		info = i.split("_")
		if info[0] == "chromosome":
			liste_chr.append(info[1])
		elif info[0] == "taxon":
			liste_taxon.append(info[1])
	liste_chr = list(map(int,liste_chr))
	liste_taxon = list(map(int,liste_taxon))
	taxon_id_list = liste_taxon
	print("titi")
	#~ print(liste_taxon)
	print(type(liste_taxon))
	print(taxon_id_list)
	for j in liste_chr:		
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(j,))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			try:
				cur.execute("SELECT sequence_name,sequence_id,taxon_id,chromosome_id,sequence_length,number_blocks,number_monomers FROM Sequence WHERE chromosome_id = %s;",(j,))
			except psycopg2.Error as e:
				print (e.pgerror)
			identifiant = cur.fetchall()
			for i in identifiant:
				i['chromosome_name'] = chr_name[0]
				try:
					cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(i['taxon_id']),))
				except psycopg2.Error as e:
					print (e.pgerror)
				taxon_name = cursor.fetchall()
				i['taxon_name'] = taxon_name[0]
				seq_id.append(i)
				seq_id_list.append(i['sequence_id'])
				if i['taxon_id'] not in taxon_list:
					taxon_list.append(i['taxon_id'])
			cursor.execute('commit')		
	for k in liste_taxon:
		supertaxon = False
		if k not in taxon_list:
			try:
				cursor.execute("SELECT taxon_id FROM Taxon WHERE supertaxon_id = %s;",(k,))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_id = cursor.fetchall()
			for tx in taxon_id:
				if tx['taxon_id'] in liste_taxon:
					supertaxon = True
			if not supertaxon:
				sequences_list = []
				subtaxon_id = []
				subtaxon_id.append({'taxon_id':info[1]})
				#~ try:
					#~ cursor.execute("SELECT taxon_id FROM Taxon WHERE supertaxon_id = %s;",(int(info[1]),))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ taxon_id = cursor.fetchall()
				#~ print("first taxon_id")
				#~ print(taxon_id)
				subtaxon_id.extend(taxon_id)
				while taxon_id:
					for i in taxon_id:
						try:
							cursor.execute("SELECT taxon_id FROM Taxon WHERE supertaxon_id = %s;",(int(i['taxon_id']),))
						except psycopg2.Error as e:
							print (e.pgerror)
						taxon_id = cursor.fetchall()
						subtaxon_id.extend(taxon_id)
				if subtaxon_id:
					for i in subtaxon_id:
						try:
							cur.execute("SELECT sequence_name,sequence_id,chromosome_id,sequence_length,number_blocks,number_monomers FROM Sequence WHERE taxon_id = %s;",(int(i['taxon_id']),))
						except psycopg2.Error as e:
							print (e.pgerror)
						identifiant = cur.fetchall()
						sequences_list.extend(identifiant)
				else:
					try:
						cur.execute("SELECT sequence_name,sequence_id,chromosome_id,sequence_length,number_blocks,number_monomers FROM Sequence WHERE taxon_id = %s;",(k,))
					except psycopg2.Error as e:
						print (e.pgerror)
					identifiant = cur.fetchall()
					sequences_list.extend(identifiant)				
				for i in sequences_list:
					try:
						cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(i['chromosome_id']),))
					except psycopg2.Error as e:
						print (e.pgerror)
					chr_name = cursor.fetchall()
					i['chromosome_name'] = chr_name[0]
					try:
						cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(k,))
					except psycopg2.Error as e:
						print (e.pgerror)
					taxon_name = cursor.fetchall()
					i['taxon_name'] = taxon_name[0]
					seq_id_list.append(i['sequence_id'])
					seq_id.append(i)
		#~ elif info[0] == "taxon":
			#~ print("taxon")
			#~ cur.execute("SELECT sequence_id FROM Taxon WHERE taxon_id = %s;",(info[1],))
			#~ identifiant = cursor.fetchone()
			#~ seq_id.append(identifiant)
	#~ print("seq_id_list")
	#~ print(seq_id_list)
	return json.dumps(seq_id)
	

@app.route("/datagrid_blocks", methods=['POST', 'GET'])
def blocks_data():
	global seq_id_list
	global block_id_list
	block_id_list = []
	selection = request.args.getlist('selection[]')
	block_length = request.args.get('search[block_length]')
	block_list = list()
	length = None
	length_max = None
	length_min = None
	#Taille des blocks
	if block_length:
		#~ print("hello")
		monomers = []
		list_length = block_length.rstrip("\n").split(":")
		if block_length.isdigit():
			length = int(block_length)
		elif len(list_length) == 2:
			if list_length[0].isdigit():
				length_min = int(list_length[0])
			if list_length[1].isdigit():
				length_max = int(list_length[1])
	seq_id = []
	#~ for i in selection:
		#~ if length: 
			#~ try:
				#~ cur.execute("SELECT block_id,block_name,block_begin,block_end,block_length FROM Block WHERE sequence_id = %s AND block_length = %s;",(int(i),length,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)			
			#~ identifiant = cur.fetchall()
		#~ elif length_min and length_max:
			#~ try:
				#~ cur.execute("SELECT block_id,block_name,block_begin,block_end,block_length FROM Block WHERE sequence_id = %s AND block_length >= %s AND block_length <= %s;",(int(i),length_min,length_max,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ identifiant = cur.fetchall()
		#~ for i in identifiant:
			#~ seq_id.append(i)
	if not selection:
		selection = list(map(int,seq_id_list))
		#~ print(selection)
		
	else:
		selection = list(map(int,selection))
	if length:
		#~ print("trololo")
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s AND block_length = %s;",(idt,length,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			for i in seq_id:
				i['sequence_name'] = sequence_result['sequence_name']
				i['chromosome_name'] = chr_name[0]
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])
			block_list.extend(seq_id)
	elif length_min and length_max:
		#~ print("djfskdj")
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s AND block_length >= %s AND block_length <= %s;",(idt,length_min,length_max,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			for i in seq_id:
				i['sequence_name'] = sequence_result['sequence_name']
				i['chromosome_name'] = chr_name[0]
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])
			block_list.extend(seq_id)
	elif length_min and not length_max:
		#~ print("fedfkdl")
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s AND block_length >= %s;",(idt,length_min,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			for i in seq_id:
				i['sequence_name'] = sequence_result['sequence_name']
				i['chromosome_name'] = chr_name[0]
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])
			block_list.extend(seq_id)
	elif not length_min and length_max:
		#~ print("gogo")
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s AND block_length <= %s;",(idt,length_max,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			for i in seq_id:
				i['sequence_name'] = sequence_result['sequence_name']
				i['chromosome_name'] = chr_name[0]
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])
			block_list.extend(seq_id)
	else:
		#~ print("coucoucou")
		for idt in selection:
			try:
				cur.execute("SELECT block_id,block_name,block_length,block_begin,block_end,block_strand,number_monomers FROM Block WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			seq_id = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			sequence_result = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(int(sequence_result['chromosome_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			chr_name = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(int(sequence_result['taxon_id']),))
			except psycopg2.Error as e:
				print (e.pgerror)
			taxon_name = cursor.fetchone()
			cursor.execute('commit')
			j = 0
			for i in seq_id:
				j +=1
				i['chromosome_name'] = chr_name[0]
				i['sequence_name'] = sequence_result['sequence_name']
				i['taxon_name'] = taxon_name[0]
				block_id_list.append(i['block_id'])
			#~ print(str(j))
			block_list.extend(seq_id)
	return json.dumps(block_list)
	#selection = request.args.get('selection',None)
	#~ table = request.args.get('table',None)
	#~ print(table)
	#~ table_element = table.split("?selection=")
	#~ table = table_element[0].replace('"','')
	#~ print("element 1")
	#~ print(table_element[1])
	#~ new_selection = table_element[1][1:len(table_element[1])-1].replace('"','')	
	#~ print(table)
	#~ print(new_selection)
	#~ print("type selection")
	#~ if new_selection:
		#~ print("selection")
		#~ input_data = new_selection.split(",")
		#~ for i in input_data:
			#~ info = i.split("_")
			#~ if info[0] == "chromosome":
				#~ print("chromosome")
				#~ chr_name = dict()
				#~ seq = list()
				#~ print(input_data)
				#~ for i in input_data:
					#~ element = i.split("_")
					#~ chr_name[element[1]] = element[0]
				#~ for i in chr_name:
					#~ cursor.execute("SELECT sequence_id FROM Sequence WHERE taxon_id = %s AND chromosome_name = %s;",(i,chr_name[i],))
					#~ identifiant = cursor.fetchone()
					#~ seq.append(identifiant[0])
				#~ print(seq)
				#~ cur.execute("SELECT block_id,block_name,block_begin,block_end FROM Block WHERE sequence_id = ANY(%s);",(seq,))
				#~ res = cur.fetchall()
			#~ elif info[1] == "taxon":
				#~ print("taxon")
				#~ seq_id = []
				#~ taxon_id = list(map(int,input_data))
				#~ cursor.execute("SELECT sequence_id FROM Taxon WHERE taxon_id = ANY(%s);",(taxon_id,))
				#~ for i in cursor:
					#~ seq_id.append(i[0])
				#~ print(seq_id)
				#~ cur.execute("SELECT block_id,block_name,block_begin,block_end FROM Block WHERE sequence_id = ANY(%s);",(seq_id,))
				#~ res = cur.fetchall()
		#~ if table == "sequence":
		#~ print("sequence")
		#~ sequence_id = list(map(int,input_data))
		#~ url = request.args.get('url_data')
		#~ print(url)
		#~ input_data = url.split("=")
		#~ print(input_data[1])
		#~ cur.execute("SELECT block_id,block_name,block_begin,block_end FROM Block WHERE sequence_id = ANY(%s);",(sequence_id,))
		#~ res = cur.fetchall()
	#~ print(res)
	#~ return json.dumps(res)
@app.route("/datagrid_families", methods=['POST', 'GET'])
def families_data():
	global block_id_list
	global family_id_list
	family_id_list = []
	#test = ({'Family_id':'1','Family_name':'toto','Sequence_id':'2'})
	#return json.dumps(test)
	selection = request.args.getlist('selection[]')
	#~ print(selection)
	seq_id = []
	families_id = []
	if not selection:
		selection = list(map(int,block_id_list))
	else:
		selection = list(map(int,selection))
	#~ print(selection)
	try:
		cursor.execute("SELECT monomer_id FROM Monomer WHERE block_id = ANY(%s);",(selection,))
	except psycopg2.Error as e:
		print (e.pgerror)
	monomers_id = cursor.fetchall()
	cursor.execute('commit')
	#~ print("monomers_id")
	#~ print(monomers_id)
	for monomer in monomers_id:
		try:
			cursor.execute("SELECT family_id FROM Belongs WHERE monomer_id = %s;",(int(monomer['monomer_id']),))
		except psycopg2.Error as e:
			print (e.pgerror)
		for family in cursor:
			#~ print(family)
			families_id.append(family['family_id'])
		cursor.execute('commit')
	#~ print("families_id")
	#~ print(families_id)
	families_id = list(map(int,families_id))
	try:
		cur.execute("SELECT family_id,family_name,sequence_id FROM Family WHERE family_id = ANY(%s);",(families_id,))
	except psycopg2.Error as e:
		print (e.pgerror)
	identifiant = cur.fetchall()
	for i in identifiant:
		seq_id.append(i)
		family_id_list.append(i['family_id'])
	cursor.execute('commit')
	return json.dumps(seq_id)


@app.route("/datagrid_monomers", methods=['POST', 'GET'])
def monomers_data():
	global block_id_list
	global monomer_id_list
	global monomers_list
	monomer_id_list = []
	monomers_list = []
	selection = request.args.getlist('selection[]',None)
	monomer_length = request.args.get('search[monomer_length]')
	monomer_position = request.args.get('search[monomer_position]')
	length = None
	length_max = None
	length_min = None
	position = None
	position_min = None
	position_max = None
	seq_id = []
	families_id = []
	#Taille des blocks
	if monomer_length:
		list_length = monomer_length.rstrip("\n").split(":")
		if monomer_length.isdigit():
			length = int(monomer_length)
		elif len(list_length) == 2:
			if list_length[0].isdigit():
				length_min = int(list_length[0])
			if list_length[1].isdigit():
				length_max = int(list_length[1])
	if monomer_position:
		#~ print("hello")
		list_position = monomer_position.rstrip("\n").split(":")
		if monomer_position.isdigit():
			position = int(monomer_position)
		elif len(list_position) == 2:
			if list_position[0].isdigit():
				position_min = int(list_position[0])
			if list_length[1].isdigit():
				position_max = int(list_position[1])				

	if not selection:
		#~ print("okokok")
		selection = list(map(int,block_id_list))
		#~ print(selection)
		#~ print(str(len(selection)))
	else:
		#~ print("nonono")
		selection = list(map(int,selection))
	if length:
		#~ print("azerty")
		for idt in selection:
			#~ print(str(idt))
			try:
				cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length = %s;",(idt,length,))
			except psycopg2.Error as e:
				print (e.pgerror)
			monomers_infos = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			block_name = cursor.fetchone()
			cursor.execute('commit')
			#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
			if monomers_infos:
				try:
					cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				sequence_infos = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				chr_name = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				tx_name = cursor.fetchone()
				cursor.execute('commit')
			for monomer in monomers_infos:
				cursor.execute('commit')
				monomer['block_name'] = block_name[0]
				monomer['sequence_name'] = sequence_infos['sequence_name']
				monomer['chromosome_name'] = chr_name[0]
				monomer['taxon_name'] = tx_name[0]
				monomer_id_list.append(monomer['monomer_id'])
			monomers_list.extend(monomers_infos)
	elif length_min and length_max:
		for idt in selection:
			try:
				cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length >= %s AND monomer_length <= %s;",(idt,length_min,length_max,))
			except psycopg2.Error as e:
				print (e.pgerror)
			monomers_infos = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			block_name = cursor.fetchone()
			cursor.execute('commit')
			#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
			if monomers_infos:
				try:
					cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				sequence_infos = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				chr_name = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				tx_name = cursor.fetchone()
				cursor.execute('commit')
			for monomer in monomers_infos:
				cursor.execute('commit')
				monomer['block_name'] = block_name[0]
				monomer['sequence_name'] = sequence_infos['sequence_name']
				monomer['chromosome_name'] = chr_name[0]
				monomer['taxon_name'] = tx_name[0]
				monomer_id_list.append(monomer['monomer_id'])
			monomers_list.extend(monomers_infos)
	elif length_min and not length_max:
		for idt in selection:
			try:
				cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length >= %s;",(idt,length_min,))
			except psycopg2.Error as e:
				print (e.pgerror)
			monomers_infos = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			block_name = cursor.fetchone()
			cursor.execute('commit')
			#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
			if monomers_infos:
				try:
					cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				sequence_infos = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				chr_name = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				tx_name = cursor.fetchone()
				cursor.execute('commit')
			for monomer in monomers_infos:
				cursor.execute('commit')
				monomer['block_name'] = block_name[0]
				monomer['sequence_name'] = sequence_infos['sequence_name']
				monomer['chromosome_name'] = chr_name[0]
				monomer['taxon_name'] = tx_name[0]
				monomer_id_list.append(monomer['monomer_id'])
			monomers_list.extend(monomers_infos)
	elif not length_min and length_max:
		for idt in selection:
			try:
				cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_length <= %s;",(idt,length_max,))
			except psycopg2.Error as e:
				print (e.pgerror)
			monomers_infos = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			block_name = cursor.fetchone()
			cursor.execute('commit')
			#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
			if monomers_infos:
				try:
					cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				sequence_infos = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				chr_name = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				tx_name = cursor.fetchone()
				cursor.execute('commit')
			for monomer in monomers_infos:
				cursor.execute('commit')
				monomer['block_name'] = block_name[0]
				monomer['sequence_name'] = sequence_infos['sequence_name']
				monomer['chromosome_name'] = chr_name[0]
				monomer['taxon_name'] = tx_name[0]
				monomer_id_list.append(monomer['monomer_id'])
			monomers_list.extend(monomers_infos)
	#~ if position:
		#~ for idt in selection:
			#~ print(str(idt))
			#~ try:
				#~ cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_begin = %s;",(idt,position,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ monomers_infos = cur.fetchall()
			#~ cur.execute('commit')
			#~ try:
				#~ cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ block_name = cursor.fetchone()
			#~ cursor.execute('commit')
			#~ #Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
			#~ if monomers_infos:
				#~ try:
					#~ cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ sequence_infos = cursor.fetchone()
				#~ cursor.execute('commit')
				#~ try:
					#~ cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ chr_name = cursor.fetchone()
				#~ cursor.execute('commit')
				#~ try:
					#~ cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ tx_name = cursor.fetchone()
				#~ cursor.execute('commit')
			#~ for monomer in monomers_infos:
				#~ cursor.execute('commit')
				#~ monomer['block_name'] = block_name[0]
				#~ monomer['sequence_name'] = sequence_infos['sequence_name']
				#~ monomer['chromosome_name'] = chr_name[0]
				#~ monomer['taxon_name'] = tx_name[0]
				#~ monomer_id_list.append(monomer['monomer_id'])
			#~ monomers_list.extend(monomers_infos)
	#~ elif position_min and position_max:
		#~ for idt in selection:
			#~ try:
				#~ cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_begin >= %s AND monomer_end <= %s;",(idt,position_min,position_max,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ monomers_infos = cur.fetchall()
			#~ cur.execute('commit')
			#~ try:
				#~ cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ block_name = cursor.fetchone()
			#~ cursor.execute('commit')
			#~ #Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
			#~ if monomers_infos:
				#~ try:
					#~ cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ sequence_infos = cursor.fetchone()
				#~ cursor.execute('commit')
				#~ try:
					#~ cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ chr_name = cursor.fetchone()
				#~ cursor.execute('commit')
				#~ try:
					#~ cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ tx_name = cursor.fetchone()
				#~ cursor.execute('commit')
			#~ for monomer in monomers_infos:
				#~ cursor.execute('commit')
				#~ monomer['block_name'] = block_name[0]
				#~ monomer['sequence_name'] = sequence_infos['sequence_name']
				#~ monomer['chromosome_name'] = chr_name[0]
				#~ monomer['taxon_name'] = tx_name[0]
				#~ monomer_id_list.append(monomer['monomer_id'])
			#~ monomers_list.extend(monomers_infos)
	#~ elif position_min and not position_max:
		#~ for idt in selection:
			#~ try:
				#~ cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_begin >= %s;",(idt,position_min,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ monomers_infos = cur.fetchall()
			#~ cur.execute('commit')
			#~ try:
				#~ cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ block_name = cursor.fetchone()
			#~ cursor.execute('commit')
			#~ #Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
			#~ if monomers_infos:
				#~ try:
					#~ cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ sequence_infos = cursor.fetchone()
				#~ cursor.execute('commit')
				#~ try:
					#~ cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ chr_name = cursor.fetchone()
				#~ cursor.execute('commit')
				#~ try:
					#~ cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ tx_name = cursor.fetchone()
				#~ cursor.execute('commit')
			#~ for monomer in monomers_infos:
				#~ cursor.execute('commit')
				#~ monomer['block_name'] = block_name[0]
				#~ monomer['sequence_name'] = sequence_infos['sequence_name']
				#~ monomer['chromosome_name'] = chr_name[0]
				#~ monomer['taxon_name'] = tx_name[0]
				#~ monomer_id_list.append(monomer['monomer_id'])
			#~ monomers_list.extend(monomers_infos)
	#~ elif not position_min and position_max:
		#~ for idt in selection:
			#~ try:
				#~ cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s AND monomer_end <= %s;",(idt,position_max,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ monomers_infos = cur.fetchall()
			#~ cur.execute('commit')
			#~ try:
				#~ cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ block_name = cursor.fetchone()
			#~ cursor.execute('commit')
			#~ #Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
			#~ if monomers_infos:
				#~ try:
					#~ cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ sequence_infos = cursor.fetchone()
				#~ cursor.execute('commit')
				#~ try:
					#~ cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ chr_name = cursor.fetchone()
				#~ cursor.execute('commit')
				#~ try:
					#~ cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
				#~ except psycopg2.Error as e:
					#~ print (e.pgerror)
				#~ tx_name = cursor.fetchone()
				#~ cursor.execute('commit')
			#~ for monomer in monomers_infos:
				#~ cursor.execute('commit')
				#~ monomer['block_name'] = block_name[0]
				#~ monomer['sequence_name'] = sequence_infos['sequence_name']
				#~ monomer['chromosome_name'] = chr_name[0]
				#~ monomer['taxon_name'] = tx_name[0]
				#~ monomer_id_list.append(monomer['monomer_id'])
			#~ monomers_list.extend(monomers_infos)
	else:
		for idt in selection:
			try:
				cur.execute("SELECT monomer_id, monomer_name, sequence_id, monomer_length, monomer_begin_sequence, monomer_end_sequence, monomer_strand FROM Monomer WHERE block_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			monomers_infos = cur.fetchall()
			cur.execute('commit')
			try:
				cursor.execute("SELECT block_name FROM Block WHERE block_id = %s;",(idt,))
			except psycopg2.Error as e:
				print (e.pgerror)
			block_name = cursor.fetchone()
			cursor.execute('commit')
			#Récupération des infos de sequence une seule fois car les mêmes pour les autres monomers
			if monomers_infos:
				try:
					cursor.execute("SELECT sequence_name,taxon_id,chromosome_id FROM Sequence WHERE sequence_id = %s;",(monomers_infos[0]['sequence_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				sequence_infos = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT chromosome_name FROM Chromosome WHERE chromosome_id = %s;",(sequence_infos['chromosome_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				chr_name = cursor.fetchone()
				cursor.execute('commit')
				try:
					cursor.execute("SELECT taxon_name FROM Taxon WHERE taxon_id = %s;",(sequence_infos['taxon_id'],))
				except psycopg2.Error as e:
					print (e.pgerror)
				tx_name = cursor.fetchone()
				cursor.execute('commit')
			for monomer in monomers_infos:
				cursor.execute('commit')
				monomer['block_name'] = block_name[0]
				monomer['sequence_name'] = sequence_infos['sequence_name']
				monomer['chromosome_name'] = chr_name[0]
				monomer['taxon_name'] = tx_name[0]
				monomer_id_list.append(monomer['monomer_id'])
			monomers_list.extend(monomers_infos)
	return json.dumps(monomers_list)
	
def hidden_encode(data):
	return base64.b64encode(json.dumps(data).encode('utf-8')).decode('ascii')
	
def hidden_decode(text):
	return json.loads(base64.b64decode(text).decode('utf-8'))
		
@app.route("/taxons/chromosomes", methods=['POST', 'GET'])
#affiche tous les taxons de la base
#def taxon_all():
def taxon_chromosomes():
	T1 = time.time()
	T2 = time.clock()
	#Noms des taxons
	list_taxon_name = request.form.getlist('Taxons')
	print("taille liste",str(len(list_taxon_name)))	
	#print(list_taxon_name)
	#Liste des identifiants des taxons
	taxon_list = request.form.getlist('taxon_dict')[0]
	chromosomes_id_list = request.form.getlist('Chromosomes')
	#Dictionnaire des noms des taxons et de leurs identifiants
	taxon_decode = loads(taxon_list)
	#print("taille dico",str(len(taxon_decode)))
	#print(taxon_decode)
	if not list_taxon_name:
		try:
			cursor.execute("SELECT taxon_name from Taxon;")
		except psycopg2.Error as e:
			print (e.pgerror)
		info_taxons = cursor.fetchall()
		cursor.execute('commit')
		for i in info_taxons:
			name = i[0]
			list_taxon_name.append(name)
			#~ print("taille new liste",str(len(list_taxon_name)))	
			#print(list_taxon_name)
	chromosomes_list = dict()
	family_name = list()
	family = dict()
	chromosome_name_id = dict()
	taxon_dict2 = dict()  
	#Identifiant du taxon selectionne
	taxon_id = [taxon_decode[name] for name in list_taxon_name]
	#print(taxon_id)
	#Requetes liste des chromosomes avec l'identifiant du taxon selectionne
	#chromosomes = list()
	#chromosomes_id = dict()
	all_chromosomes_id = list()
	dict2 = dict()
	chromosome_list = dict()
	#Dictionnaire des noms des taxons et de leurs identifiants
	dict2 = {str(taxon_decode[d]) : d for d in taxon_decode}
	F1 = time.time()
	try:
		cursor.execute("SELECT taxon_id, taxon_name from Taxon ;")
	except psycopg2.Error as e:
		print (e.pgerror)
	FF1 = time.time()
	print( "temps de la requete 1 %f" %(FF1 - F1))
	results = cursor.fetchall()
	cursor.execute('commit')
	resultsf = dict()
	taxon_name = list()
	for i in results:
		identifiant, name = i
		taxon_name.append(name)
		resultsf[name] =  identifiant
	taxon_encode = dumps(resultsf)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction list/Taxon : %f" %(FT1 - T1))
	print("Temps CPU de la fonction list/Taxon : %f" %(FT2 - T2))
	#return render_template("taxons_chromosomes_test.html", taxon_name = taxon_name, taxon_dict = taxon_encode)

	T1 = time.time()
	T2 = time.clock()
	F1 = time.time()
	try:
		cursor.execute("SELECT sequence_id, taxon_id, sequence_name, chromosome_name FROM Sequence WHERE taxon_id IN %s ;", (tuple(taxon_id),))
	except psycopg2.Error as e:
		print (e.pgerror)
	FF1 = time.time()
	print(" temps de la requete 1 %f" %(FF1 - F1))
	A1 = time.time()
	sequenceId_list = list()
	for i in cursor:
		(sequence_id,taxon_id,sequence_name,chromosome_name) = i
		#sequenceId_list.append(sequence_id)
		#chromosomes_id[i[1]] = i[0]
		all_chromosomes_id.append(sequence_id)
		#chromosome_list[taxon_id][chromosome_name]={}
		#if dict2[str(taxon_id)] not in chromosome_list:
			#chromosome_list[dict2[str(taxon_id)]] ={}
		#Taxon_name recupéré avec le dict2 a partir du taxon_id
		chromosome_list["%s : %s : %s" %(dict2[str(taxon_id)],chromosome_name,sequence_name)] = sequence_id
		#if "%s : %s" %(dict2[str(taxon_id)],chromosome_name) not in chromosome_list:
			#chromosome_list["%s : %s" %(dict2[str(taxon_id)],chromosome_name)] = [sequence_id]
		#else:
			#chromosome_list["%s : %s" %(dict2[str(taxon_id)],chromosome_name)].append(sequence_id)
	#print(chromosome_list)
	cursor.execute('commit')
	#print(chromosome_list)
	print(type(chromosome_list))
	A2 = time.time()
	#print(chromosome_list)
	print(" temps du remplissage %f" %(A2 - A1))
	family_id = dict()
	t1 = time.time()
	if not chromosomes_id_list:
		try:
			cursor.execute("SELECT family_id,family_name FROM Family WHERE sequence_id IN %s;",(tuple(all_chromosomes_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
	else:
		try:
			cursor.execute("SELECT family_id,family_name FROM Family WHERE sequence_id IN %s;",(tuple(chromosomes_id_list),))
		except psycopg2.Error as e:
			print (e.pgerror)
	for i in cursor:
		family_id[i[1]] = i[0]
	#~ if not family_id:
		#~ family_name = [d for d in family_id]
		#~ family_name = list({v['_id']:v for v in family_name}.values())
		#~ family_name_id = {str(d['_id']) : d['name'] for d in family_id}
		#~ for g in family_name : 
			#~ family[g['name']] = g['_id']

	blockName_dict = dict()
	if not chromosomes_id_list:
		try:
			cursor.execute("SELECT block_id,block_name FROM Block WHERE sequence_id IN %s;",(tuple(all_chromosomes_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
	else:
		try:
			cursor.execute("SELECT block_id,block_name FROM Block WHERE sequence_id IN %s;",(tuple(chromosomes_id_list),))
		except psycopg2.Error as e:
			print (e.pgerror)	
	for i in cursor:
		blockName_dict[i[1]] = i[0]
	cursor.execute('commit')
	#print(blockName_dict)
	#print(type(blockName_dict))
	#print(chromosomes)
	#print(chromosome_list)
	#Dictionnaire des noms et identifiants des chromosomes
	#chromosomes_id = {d[1] : d[0] for d in chromosomes}
	#Liste des identifiants des chromosomes
	#all_chromosomes_id = [d[0] for d in chromosomes]
	#Dictionnaire des identifiants des taxons : noms des chromosomes
	#Modifications:
	#Liste de tri croissant des caracteres ASCII des clés du dictionnaire
	#Dictionnaire des noms des chromosomes (nomEspece+nomChro) et des idenfiants des chromosomes
	#chromosome_list = {"%s : %s" %(dict2[str(d[2])], d[1]) : d[0]for d in chromosomes}
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction /taxons/chromosomes : %f" %(FT1 - T1))
	print("Temps CPU de la fonction /taxons/chromosomes : %f" %(FT2 - T2))
	return render_template("taxons_chromosomes_test_2.html", taxon_name = taxon_name, taxon_dict = taxon_encode, chromosome = chromosome_list,list_taxon = taxon_id, chro_dict = dumps(all_chromosomes_id), block_dict = blockName_dict, fmaily_id = family_id )

@app.route("/taxons/monomers_output", methods=['POST', 'GET'])
def monomers_output():
	monomerlist = request.form.getlist('Monomers')
	ipdb.set_trace()
	resultsf = list()
	for name in monomerlist:
		ligne = {}
		try:
			cursor.execute("SELECT monomer_name, monomer_begin, monomer_end, monomer_strand FROM Monomere where monomer_name = %s;",(name,))
		except psycopg2.Error as e:
			print (e.pgerror)
		monomer_info = cursor.fetchone()
		cursor.execute('commit')
		ligne['name'],ligne['begin'],ligne['end'],ligne['strand'] = monomer_info
		ligne['chromosome_name'] = monomerlist[i][j]['name'].split("_")[0]
		resultsf.append(ligne)
		conn.commit()
	return render_template("taxons_monomers_output.html", monomer = resultsf)
	
def gen_chromosome_id_list(chromosome_list):
	T1 = time.time()
	T2 = time.clock()
	chromosome_id = list()
	for name in chromosome_list:
		(taxon_id, sequence_name, chromosome_name) = name.split(" : ")
		#cursor.execute("SELECT taxon_id FROM Taxon WHERE taxon_name = %s ;", (taxon_name,))
		#taxon_identifiant = cursor.fetchall()
		taxon_id = taxon_identifiant[0]
		try:
			cursor.execute("SELECT sequence_id FROM Sequence WHERE chromosome_name = %s AND taxon_id = %s ;", (chromosome_name, taxon_id,))
		except psycopg2.Error as e:
			print (e.pgerror)
		chr_id = cursor.fetchall()
		cursor.execute('commit')
		chromosome_id.append(chr_id)
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction gen_chromosome_id_list() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction gen_chromosome_id_list() : %f" %(FT2 - T2))
	return chromosome_id
   
def find_blocks(chromosome_list):
	T1 = time.time()
	T2 = time.clock()
	F1 = time.time()
	print("taille liste")
	print(str(len(chromosome_list)))
	try:
		cursor.execute("SELECT block_id FROM Block where sequence_id IN %s ;", (tuple(chromosome_list),))
	except psycopg2.Error as e:
		print (e.pgerror)
	FF1 = time.time()
	print("Temps de la requete blocks %f" %(FF1 - F1))
	block_identifiant = cursor.fetchall()
	cursor.execute('commit')
	blocks = [d[0] for d in block_identifiant]
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction find_blocks() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction find_blocks() : %f" %(FT2 - T2))
	return blocks

def find_monomers(chromosome_list):
	## Voir pour passer directement par chromosome ##
	T1 = time.time()
	T2 = time.clock()
	try:
		cursor.execute("SELECT block_id FROM Block WHERE chromosome_id IN %s ;", (chromosome_list,))
	except psycopg2.Error as e:
		print (e.pgerror)
	block_identifiant = cursor.fetchall()
	cursor.execute('commit')
	blocks = [d for d in block_identifiant]
	try:
		cursor.execute("SELECT monomer_id FROM Monomer WHERE chromosome_id IN %s ;", (chromosome_list,))
	except psycopg2.Error as e:
		print (e.pgerror)
	monomer_identifiant = cursor.fetchall()
	cursor.execute('commit')
	monomers = [d[0] for d in monomer_identifiant]
	FT1 = time.time()
	FT2 = time.clock()
	print("Temps total de la fonction find_monomers() : %f" %(FT1 - T1))
	print("Temps CPU de la fonction find_monomers() : %f" %(FT2 - T2))
	return monomers

@app.route("/taxons/filters", methods=['POST', 'GET'])
def filters():
	T1 = time.time()
	T2 = time.clock()
	#Liste de tous les chromosomes selectionnes de la requete choix des chromosomes 
	chromosome_list = request.form.getlist('Chromosomes')
	family_list = request.form.getlist('Family')
	phase = request.form.getlist('phase')[0]
	base_begin = request.form.getlist('base_pair_begin')[0]
	base_end = request.form.getlist('base_pair_end')[0]
	distance_centro = request.form.getlist('distance_from_centromer')[0]
	block_id = request.form.getlist('block_name')
	taxon_list = request.form.getlist('list_taxon')
	block_length = request.form.getlist('block_length')[0]
	monomer_length = request.form.getlist('monomer_length')[0]
	#Liste de tous les identifiants de chromosomes de l'espece
	all_chromosomes_id = loads(request.form.getlist('list_chromosomes')[0])
	#aa = [str(i) for i in all_chromosomes_id]
	#print(aa[1:10])
	#liste_chrs = open("list_chrs", "w")
	#liste_chrs.write(",".join(aa))
	#liste_chrs.close()
	chromosome_id = list()
	monomers = list()
	blocks = list()
	family_name_show = list()
	block_length_id = list()
	monomer_length_id = list()
	monomers_id = list()
	##Conditions si les differents champs sont remplis##
	if phase.isdigit():
		phase = int(phase)
		print("convertible en int")
	elif not phase.isdigit() or not phase:
		phase = 1
	if base_begin.isdigit():
		b_begin = int(base_begin)
		print("convertible en int")
	#Attribution d'une valeur par défaut
	elif not base_begin.isdigit() or not base_begin:
		b_begin = 0
		print("case vide")
	else:
		print("champs non rempli")
	if base_end.isdigit():	
		b_end = int(base_end)
		print("pas un str initial")
	elif not base_begin.isdigit() or not base_end:
		b_end = 4000000000
		print("champs non rempli")
	else:
		print("cas inconnu")
	if distance_centro.isdigit():
		distance_centro = int(distance_centro)
	##Options##
	#Selection par noms de chromosomes
	if chromosome_list:
		chromosome_list = [d for d in chromosome_list]
		print("liste chro")
	else:
		print(" pas une liste chros")
		chromosome_list = all_chromosomes_id
		chromosome_list = [d for d in chromosome_list]
		#Tri de la liste de chromosomes selectionnes
		chromosome_list2 = sorted(chromosome_list)
	#Fonction find_blocks()
	blocks = find_blocks(chromosome_list)
	#print("la liste de chro",str(len(chromosome_list)))
	#cursor.execute("SELECT block_id FROM Block where chromosome_id IN %s ;", (tuple(all_chromosomes_id),))
	#all_blocks_id = list()
	#for i in cursor:
	#	all_blocks_id.append(i[0])
	"""	
	if distance_centro.strip() != '':
		print("centro pas vide")
		##
		cursor.execute("SELECT * FROM Chromosome WHERE chromosome_id IN %s;",(chromosome_list,))
		chromosomes_array = cursor.fetchall()
		#chromosomes_array = list(c.find({'_id' : {'$in': chromosome_list}}))[0]
		if 'centromer' not in chromosomes_array:
			monomers = find_monomers(chromosome_id)
			print("centro pas ds le chro")
		else : 
			print("centro ds le chro")
			info_centromer = chromosomes_array['centromer']
			centromer_begin_list = [int(d['begin']) for d in info_centromer]
			centromer_begin = min(centromer_begin_list)
			centromer_end_list = [int(d['end']) for d in info_centromer]
			centromer_end = max(centromer_end_list)
			##
			cursor.execute("SELECT monomer_id FROM Monomer WHERE chromosome_id IN %s AND monomer_begin >= (%s - %s) AND monomer_end <= (%s - %s) ;", (chromosome_list, centromer_begin, distance_centro, centromer_end, distance_centro,))
			monomers_identifiant = cursor.fetchall()
			#v = list(m.find( {'Chromosome_id' : {'$in': chromosome_list}, 'begin' : {"$gte" : (centromer_begin - distance_centro)} , 'end' : {"$lte" : (centromer_end + distance_centro)} }))
			monomers = [d[0] for d in v]
			blocks = find_blocks(chromosome_id)
	else:
		print("centro vide")

	#?#Apparement quand champs vide on a bien une liste de familles
	if family_list:
		#ObjectId() est utlise pr transforme en id pr mongoDB
		family_list = [ObjectId(d) for d in family_list]
		##
		v = list(m.find({'families' : {"$elemMatch" :{'family_id' :  {"$in" : family_list}}}}))
		monomers_list_family =  [d['_id'] for d in v]
		monomers = list(set(monomers)& set(monomers_list_family))
		##
		cursor.execute("SELECT family_name FROM Family WHERE family_id IN  %s ;"), (family_list))
		family_list_to_name = cursor.fetchall()
		#family_list_to_name = list(f.find({'_id' : {"$in" : family_list} }))
		family_name_show = [d[0] for d in family_list_to_name]
		print("liste famille")
	else:
		print("vide")
	"""
	if not base_begin or not base_end:
		print("pas de begin end")
		try:
			cursor.execute("SELECT monomer_id FROM Monomer where sequence_id IN %s ;", (tuple(chromosome_list),))
		except psycopg2.Error as e:
			print (e.pgerror)
		monomer_identifiant = cursor.fetchall()
		cursor.execute('commit')
		monomers = [d[0] for d in monomer_identifiant]
	else:
		try:	
			cursor.execute("SELECT monomer_id FROM Monomer where sequence_id IN %s AND monomer_begin >= %s AND monomer_end <= %s ;", (tuple(chromosome_list), b_begin, b_end,))
		except psycopg2.Error as e:
			print (e.pgerror)
		monomer_identifiant = cursor.fetchall()
		cursor.execute('commit')
		monomers = [d[0] for d in monomer_identifiant]
	#Selection par noms de blocks
	if block_id:
		print("block_id coucou")
		try:
			cursor.execute("SELECT monomer_id FROM Monomer WHERE block_id IN %s ;", (tuple(block_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
		monomers = cursor.fetchall()
		cursor.execute('commit')
		monomers = [d[0] for d in monomers]
		monomers = [element for element in monomers if element in monomers]
		try:
			cursor.execute("SELECT DISTINCT block_name FROM Block WHERE block_id IN %s ;", (tuple(block_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
		block_name = list()
		for i in cursor:
			block_name.append(i[0])
		cursor.execute('commit')
	else:
		block_name = []
		print("une chaine block")
	#Taille des blocks
	if block_length:
		monomers =[]
		print("block coucou")
		list_length = block_length.rstrip("\n").split(":")
		print(list_length[0])
		print(list_length[1])
		if block_length.isdigit():
			try:
				cursor.execute("SELECT block_id FROM Block WHERE block_length = %s;",(block_length,))
			except psycopg2.Error as e:
				print (e.pgerror)
				for i in cursor:
					block_length_id.append(i[0])
			cursor.execute('commit')
		elif len(list_length) == 2:
			if list_length[0] == "" and list_length[1].isdigit():
				try:
					cursor.execute("SELECT block_id FROM Block WHERE block_length =< %s;",(list_length[1],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					block_length_id.append(i[0])
				cursor.execute('commit')
			elif list_length[1] == "" and list_length[0].isdigit():
				try:
					cursor.execute("SELECT block_id FROM Block WHERE block_length >= %s;",(list_length[0],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					block_length_id.append(i[0])
				cursor.execute('commit')
			elif list_length[0].isdigit() and list_length[1].isdigit():
				try:
					cursor.execute("SELECT block_id FROM Block WHERE block_length >= %s and block_length <= %s ;",(list_length[0],list_length[1],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					block_length_id.append(i[0])
				cursor.execute('commit')
		try:
			cursor.execute("SELECT monomer_id FROM Monomer WHERE block_id IN %s ;",(tuple(block_length_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			monomers.append(i[0])
	#Taille des monomers
	if monomer_length:
		monomers = []
		print("coucou")
		list_length2 = monomer_length.rstrip("\n").split(":")
		if monomer_length.isdigit():
			try:
				cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_length = %s;",(monomer_length,))
			except psycopg2.Error as e:
				print (e.pgerror)
				for i in cursor:
					monomer_length_id.append(i[0])
			cursor.execute('commit')
		elif len(list_length2) == 2:
			if list_length2[0] == "" and list_length2[1].isdigit():
				try:
					cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_length <= %s;",(list_length2[1],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					monomer_length_id.append(i[0])
				cursor.execute('commit')
			elif list_length2[1] == "" and list_length2[0].isdigit():
				try:
					cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_length >= %s;",(list_length2[0],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					monomer_length_id.append(i[0])
				cursor.execute('commit')
			elif list_length2[0].isdigit() and list_length2[1].isdigit():
				try:
					cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_length >= %s and monomer_length <= %s ;",(list_length2[0],list_length2[1],))
				except psycopg2.Error as e:
					print (e.pgerror)
				for i in cursor:
					monomer_length_id.append(i[0])
				cursor.execute('commit')
		try:
			cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_id IN %s ;",(tuple(monomer_length_id),))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			monomers.append(i[0])
		print("tyrala")
		print(monomers)
	##Donnees communes##
	#Comptage des monomers
	try:
		cursor.execute("SELECT count(monomer_id) FROM Monomer where sequence_id IN %s ;", (tuple(all_chromosomes_id),))
	except psycopg2.Error as e:
		print (e.pgerror)
	monomerstot = cursor.fetchone()
	cursor.execute('commit')
	#Comptage des blocks
	cursor.execute("SELECT count(block_id) FROM Block where sequence_id IN %s ;", (tuple(all_chromosomes_id),))
	blockstot = cursor.fetchone()
	cursor.execute('commit')
	#Liste des identifiants des chromosomes selectionnes	
	try:
		cursor.execute("SELECT DISTINCT chromosome_name FROM Sequence WHERE sequence_id IN %s ;", (tuple(chromosome_list),))
	except psycopg2.Error as e:
		print (e.pgerror)
	chromosome_list_to_name = cursor.fetchall()
	cursor.execute('commit')
	#Liste des noms des chromosomes selectionnes
	chromosome_name_show = [d[0] for d in chromosome_list_to_name]
	#Tri des caracteres ASCII des noms des chromosomes par ordre croissant
	chromosome_name_show = sorted(chromosome_name_show)
	try:
		cursor.execute("SELECT DISTINCT sequence_name FROM Sequence WHERE sequence_id IN %s ;", (tuple(chromosome_list),))
	except psycopg2.Error as e:
		print (e.pgerror)
	sequence_name_list = cursor.fetchall()
	cursor.execute('commit')
	#Liste des noms des chromosomes selectionnes
	sequence_name_show = [d[0] for d in sequence_name_list]
	#Variable nombre de monomers
	monomers_number = len(monomers)
	#Variable nombre de blocks
	blocks_number = len(blocks)
	#Attention si l'objet est une liste de liste, la fonction dump() la transforme en dictionnaire
	#Json de la liste de monomers
	monomers_list_encode = dumps([d for d in monomers])
	#Json de la liste de blocks
	blocks_list_encode = dumps([d for d in blocks])
	#Envoi des variables
	return render_template("show_filters.html", phase = phase, chromosomes = chromosome_list, family = family_list, blocks = blocks_number, blockstot = blockstot[0], blocks_list_encode = blocks_list_encode,
	base_begin = base_begin, base_end = base_end, monomers = monomers_number, monomerstot = monomerstot[0], monomers_list_encode = monomers_list_encode, distance_centro = distance_centro,
	block_name = block_name, family_name_show = family_name_show, chromosome_name_show = chromosome_name_show, sequence_name_show = sequence_name_show )


@app.route("/results/monomers",  methods=['POST', 'GET'])
def results():
	monomers_list = request.form.getlist("monomers_list_encode")[0]
	blocks_list = request.form.getlist("blocks_list_encode")[0]
	retrieve_monomers = request.form.getlist("retrieve_monomers")
	retrieve_blocks = request.form.getlist("retrieve_blocks")
	phase = request.form.getlist("monomer_phase")[0]
	#print(retrieve_monomers)
	#print(retrieve_blocks)
	if retrieve_monomers == ['retrieve_monomers']:
		list_encode = monomers_list
		retrieve_type = "monomers"
	else:
		list_encode = blocks_list
		retrieve_type = "blocks"
	return render_template("results_monomers.html", list_encode = list_encode, retrieve_type = retrieve_type, phase = phase)

def monomer_name(name_format,identifiant,separator_format):
	liste_name= []
	print(separator_format)
	if name_format == "database_name":
		try:
			cursor.execute("SELECT monomer_name FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		monomer_database = cursor.fetchone()
		cursor.execute('commit')
		name = monomer_database['monomer_name']
	else:
		if "block_name" in name_format:
			try:
				cursor.execute("SELECT block_id FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)		
			block_id = cursor.fetchone()
			cursor.execute('commit')					
			try:
				cursor.execute("SELECT block_name FROM Block WHERE block_id = %s ;",(block_id[0],) )
			except psycopg2.Error as e:
				print (e.pgerror)
			block_name = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(block_name['block_name'])
		if "monomer_id" in name_format:
			liste_name.append(identifiant)
		if "begin" in name_format:
			try:
				cursor.execute("SELECT monomer_begin FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_begin = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_begin['monomer_begin']))
		if "end" in name_format:
			try:
				cursor.execute("SELECT monomer_end FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_end = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_end['monomer_end']))
		if "strand" in name_format:
			try:
				cursor.execute("SELECT monomer_strand FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_strand = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_strand['monomer_strand']))
		if "monomer_length" in name_format:
			try:
				cursor.execute("SELECT monomer_length FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_length = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_length['monomer_length']))
		liste_name2 = list(map(str,liste_name))
		name = separator_format.join(liste_name2)
	return(name)
	
def monomer_name(name_format,identifiant,separator_format):
	liste_name= []
	print(separator_format)
	if name_format == "database_name":
		try:
			cursor.execute("SELECT monomer_name FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
		except psycopg2.Error as e:
			print (e.pgerror)
		monomer_database = cursor.fetchone()
		cursor.execute('commit')
		name = monomer_database['monomer_name']
	else:
		if "block_name" in name_format:
			try:
				cursor.execute("SELECT block_id FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)		
			block_id = cursor.fetchone()
			cursor.execute('commit')					
			try:
				cursor.execute("SELECT block_name FROM Block WHERE block_id = %s ;",(block_id[0],) )
			except psycopg2.Error as e:
				print (e.pgerror)
			block_name = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(block_name['block_name'])
		if "monomer_id" in name_format:
			liste_name.append(identifiant)
		if "begin" in name_format:
			try:
				cursor.execute("SELECT monomer_begin FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_begin = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_begin['monomer_begin']))
		if "end" in name_format:
			try:
				cursor.execute("SELECT monomer_end FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_end = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_end['monomer_end']))
		if "strand" in name_format:
			try:
				cursor.execute("SELECT monomer_strand FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_strand = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_strand['monomer_strand']))
		if "monomer_length" in name_format:
			try:
				cursor.execute("SELECT monomer_length FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_length = cursor.fetchone()
			cursor.execute('commit')
			liste_name.append(str(monomer_length['monomer_length']))
		liste_name2 = list(map(str,liste_name))
		name = separator_format.join(liste_name2)
	return(name)
	
@app.route("/results/monomers&blocks", methods=['POST', 'GET'])
def results_monomers_blocks():
	list_encode = loads(request.form.getlist("list_encode")[0])
	retrieve_type = request.form.getlist("retrieve_type")[0]
	format_output = request.form.getlist("format")[0]
	fasta_output = request.form.getlist("output")[0]
	csv_output = request.form.getlist("csv")[0]
	phase = int(request.form.getlist("monomer_phase")[0])
	name_option = request.form.getlist("name_option")
	name_format = request.form.getlist("name_format")
	separator_format = request.form.getlist("separator_format")[0]
	results = io.StringIO()
	family_string = str()
	coll = {"monomers" : Monomer, "blocks" : Block}
	mono_counter = 0
	block_counter = 0
	liste_name = list()
	if coll[retrieve_type] == Monomer:
		if format_output == 'format_fasta':
			for identifiant in list_encode:
				name = monomer_name(name_format,identifiant,separator_format)
				try:
					cursor.execute("SELECT monomer_index,monomer_nucleotide,monomer_length FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				index = info['monomer_index']
				nucleotide = info['monomer_nucleotide']
				length = info['monomer_length']
				results.write(">%s\n" %name)		
				if phase == 1:
					try:
						cursor.execute("SELECT monomer_sequence FROM Monomer WHERE monomer_id = %s ;",(identifiant,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					monomer_seq = cursor.fetchone()
					cursor.execute('commit')
					monomer_unique = monomer_seq['monomer_sequence']
					sequence2 = '\n'.join((monomer_unique)[pos:pos+60] for pos in range(0,len(monomer_unique), 60))
					results.write( "%s\n" %sequence2 )
				else:
					begin = None
					end = None
					ii = 0
					while ii < (len(index)-1) and index[ii] < phase:
						begin = ii + 1
						ii += 1
					jj = begin - 1
					while jj >= 0 and index[jj] <= 0:
						begin = jj
						jj -= 1
					yy = length
					while yy < (len(index)-1) and index[yy] < phase:
						end = yy
						yy += 1
					zz = end
					while zz >= 0  and index[zz] <= 0:
						end = zz
						zz -= 1
					monomer = nucleotide[begin:end+1]
					sequence2 = '\n'.join(monomer[pos:pos+60] for pos in range(0, len(monomer),60))
					results.write( "%s\n" %sequence2 )					
			sequence_output = results.getvalue()
			if fasta_output == 'fasta.gzip':
				response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				filename = "%s.fasta.gz" %retrieve_type
			else:
				filename = "%s.fasta" %retrieve_type
				response=Response(sequence_output)
		else:
			#Fichier CSV
			for identifiant in list_encode:
				name = monomer_name(name_format,identifiant,separator_format)
				separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
				try:
					cursor.execute("SELECT monomer_nucleotide,monomer_begin,monomer_end,monomer_strand,sequence_id FROM Monomer WHERE monomer_id = %s ;", (identifiant,))
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				(nucleotide,begin,end,strand,sequence_id) = info				
				try:
					cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()
				cursor.execute('commit')
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name[0], separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				"""if 'families' in monomer_info:
					family_results = set()
					for families_name in monomer_info['families']:
						family_results.add(families_name['name'])
					if len(family_results) == 1:
						family_string = str(family_results)
					else:
						family_string = '-'.join(family_results)
					results.write("%s%s" % (family_string, separators[csv_output]))"""
				results.write("%s\n" % nucleotide)
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.txt" %retrieve_type
	elif coll[retrieve_type] == Block:
		if format_output == 'format_fasta':
			for identifiant in list_encode:
				try:
					cursor.execute("SELECT block_name,block_nucleotide FROM Block WHERE block_id = %s ;",(identifiant,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				info = cursor.fetchone()
				cursor.execute('commit')
				(name,nucleotide) = info
				results.write(">%s\n" %name)
				sequence2 = '\n'.join((nucleotide)[pos:pos+60] for pos in range(0, len(nucleotide), 60))
				results.write( "%s\n" %sequence2 )
			sequence_output = results.getvalue()
			if fasta_output == 'fasta.gzip':
				response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))
				filename = "%s.fasta.gz" %retrieve_type
			else:
				filename = "%s.fasta" %retrieve_type
				response=Response(sequence_output)
		else:
			for identifiant in list_encode:
				separators = {'comma': ',', 'space': ' ', 'tab': "\t"}
				if name_format == "database_name":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(name,nucleotide,begin,end,strand,sequence_id) = info
				#chromosome_name = info[0].split('_',1)[0]
				elif name_format == "chr_begin_end_strand":
					try:
						cursor.execute("SELECT block_name,block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(block_name,nucleotide,begin,end,strand,sequence_id) = info
					try:
						cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
					except psycopg2.Error as e:
						print (e.pgerror)
					chromosome_name = cursor.fetchone()
					cursor.execute('commit')
					name = chromosome_name[0]+"_"+str(begin)+"_"+str(end)+"_"+str(strand)			
				elif name_format == "simple_numbering":
					try:
						cursor.execute("SELECT block_nucleotide,block_begin,block_end,block_strand,sequence_id FROM Block WHERE block_id = %s ;", (identifiant,))
					except psycopg2.Error as e:
						print (e.pgerror)
					info = cursor.fetchone()
					cursor.execute('commit')
					(nucleotide,begin,end,strand,sequence_id) = info
					block_counter += 1
					name = str(block_counter)
				try:
					cursor.execute("SELECT chromosome_name FROM Sequence WHERE sequence_id = %s ;",(sequence_id,) )
				except psycopg2.Error as e:
					print (e.pgerror)
				chromosome_name = cursor.fetchone()
				cursor.execute('commit')
				results.write("%s%s" % (name, separators[csv_output]))
				results.write("%s%s" % (chromosome_name, separators[csv_output]))
				results.write("%s%s" % (begin, separators[csv_output]))
				results.write("%s%s" % (end, separators[csv_output]))
				results.write("%s%s" % (strand, separators[csv_output]))
				results.write("%s\n" % nucleotide)
			sequence_output = results.getvalue()
			#a garder si on veut pouvoir faire des gzip par la suite
			"""response = Response(gzip.compress(bytes(sequence_output, 'utf-8')))"""
			response=Response(sequence_output)
			filename = "%s.txt" %retrieve_type		
	response.headers.add('Content-type', 'application/octet-stream')
	response.headers.add('Content-Disposition', 'attachment; filename="%s"' % filename)
	return response

@app.route("/alphasat/accueil" , methods=['POST', 'GET'])
def accueil():
	return render_template("alphasat_accueil.html")

@app.route("/alphasat/contact", methods=['POST', 'GET'])
def contact():
	return render_template("contact.html")
	
@app.route("/alphasat/help_and_documentation", methods=['POST', 'GET'])
def documentation():
	return render_template("help_and_documentation.html")
	
if __name__ == "__main__":
	app.run(port=PORT, host="0.0.0.0", debug=True)

