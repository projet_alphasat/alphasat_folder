#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import io
import time 
import sys
import argparse
import re
import psycopg2
import psycopg2.extras
from parsing_fonctions import (memory_usage, absolue_location, insert_chromosome_chunk)
import subprocess

extended_help = """Several modes:

	MONOMER MODE:
	============
	
	BEWARE: in this mode, if you choose -clean Y you clean the entiere database

	For this mode, -t -c -m -k -clean options are required
	This mode can be associated with Family mode and BigFamily mode
	
	eg:
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -k N -clean Y
	to enter a taxon name : hg19, clean the data base but not entered the sequences associated
	
	FAMILY MODE:
	===========
	
	BEWARE : if clean option is choose in this mode, only the monomers' families
	dependences are removed
	
	For this mode, -t -f -clean options are required
	This mode can be associated with Monomer mode and BigFamily mode 
	
	eg:
	-t hg19 -f family_data.fst -clean N
	
	Here, you enter families data but you keep the informations 
	
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -clean Y
	
	If you want to clean the database you have to enter chromosome data 
	and family data
	
	BIGFAMILY MODE:
	==============
	
	BEWARE : if clean option is choose in this mode, the entiere family data
	are removed and the monomers' families dependances
	
	For this mode, -t -clean options are required
	This mode can be associated with Family mode and Monomer/Family mode
	
	eg:
	-t hg19 -F bigfamily_data.fst -clean N
	
	Here, you enter bigfamilies data but you keep the informations and the families data
	are updated 
	
	-t hg19 -f family_data.fst -F bigfamily_data.fst -clean N
	
	Here, you keep monomers' and chromosomes' informations and you add 
	families and bigfamilies informations. The monomers' informations are
	updated
	
	
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -F bigfamily_data.fst-clean Y
	
	Here, you create en entiere new base with all the informations
	
	CYTOBAND MODE:
	==============
	
	For this mode, -t -clean options are required
	This mode can be associated with Family mode and Monomer/Family/BigFamily mode
	
	Here, you keep monomers', chromosomes' and families' informations 
	and you add cytobands informations. The chromosomes' informations are
	updated
	
	e.g :
	
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -f family_data.fst -F bigfamily_data.fst -cy cytoband_data.fst -k N -clean Y
	
	Here, you create en entiere new base with all the informations
	
	-t hg19 -cy cytoband_data.fst -k N -clean Y
	
	Here, you clean the chromosomes' informations about the cytoband and update with new one
	
	-t hg19 -c chromosome_data.fst -m monomer_data.fst -cy cytoband_data.fst -clean Y
	"""

parser = argparse.ArgumentParser(
epilog = extended_help, formatter_class = argparse.RawTextHelpFormatter)
parser.add_argument("-t", help="taxon name", type=str) #required=True,
parser.add_argument("-T", help="path taxonparents name", type=str)
parser.add_argument("-r", help="path reference' file", type=str)
parser.add_argument("-i", help="path index' file", type=str)
parser.add_argument("-c", help="path chromosomes' file", type=str)
parser.add_argument("-m", help="path monomers' file",  type=str)
parser.add_argument("-f", help="path family file", type=str)
parser.add_argument("-F", help="path familyparents file", type=str)
parser.add_argument("-cy", help="path cytoband file", type=str)
parser.add_argument("-clean", help="the database will be totally cleaned, Y/N",required=True, type=str)
parser.add_argument("-k", help="the sequence will be entered or not Y/N", type=str)
parser.add_argument("-v", help="verbose", type=str)
args = parser.parse_args()
"""Connexion a la base de donnees"""
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
	#conn = psycopg2.connect(database = args.db)
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)


def clean_banque(banque):
	"""clean all the Collections - drop the database"""
	T1 = time.time()
	banque = banque.lower()
	if banque == 'y':
		try:
			cursor.execute("SELECT tablename FROM pg_tables WHERE tablename !~ '^pg_' AND tablename !~ '^sql_';")
		except psycopg2.Error as e:
			print (e.pgerror)
		listeTables = list()
		for row in cursor:
			for element in row:
				listeTables.append(element)
				print (str(element))
		cursor.execute('commit')
		for i in listeTables:
			cursor.execute("DROP TABLE "+i+" CASCADE;")
		cursor.execute("""create table Taxon(
		taxon_id serial UNIQUE, 
		taxon_name varchar(50),
		supertaxon_id int,
		primary key(taxon_id),
		foreign key(supertaxon_id) references Taxon(taxon_id) on delete CASCADE);""")
		cursor.execute('commit')
		cursor.execute("""create table Sequence(
		sequence_id serial UNIQUE,
		sequence_name varchar(30),
		sequence_length int,
		sequence_nucleotide text,
		chromosome_name varchar(50), 
		primary key(Sequence_id), 
		taxon_id int,
		foreign key(taxon_id) references Taxon(taxon_id) on delete CASCADE );""")
		cursor.execute('commit')
		cursor.execute("""create table Reference(
		reference_id serial UNIQUE,
		reference_name varchar(30),
		reference_nucleotide text,
		reference_comments text,
		primary key(reference_id));""")
		cursor.execute('commit')
		cursor.execute("""create table Block(
		block_id serial unique,
		block_name varchar(60),
		sequence_id int,
		block_begin int, 
		block_end int, 
		block_strand int,
		block_length int,
		block_nucleotide text,
		block_index integer[],
		reference_id int,
		primary key(block_id),
		foreign key(reference_id) references Reference(reference_id) on delete CASCADE,
		foreign key(sequence_id) references Sequence(sequence_id) on delete CASCADE);""")
		cursor.execute('commit')
		cursor.execute("""create table Monomer(
		monomer_id serial UNIQUE,
		block_id int,
		sequence_id int,
		monomer_name text,
		monomer_begin int, 
		monomer_end int,
		monomer_strand int,
		monomer_length int,
		monomer_nucleotide text,
		monomer_index integer[],
		monomer_sequence text,
		primary key(monomer_id),
		foreign key(sequence_id) references Sequence(sequence_id) on delete CASCADE,
		foreign key(block_id) references Block(block_id) on delete CASCADE);""")
		cursor.execute('commit')
		cursor.execute("""create table Family(
		family_id serial UNIQUE,
		family_name varchar(30),
		sequence_id int,
		superfamily_id int,
		primary key(family_id),
		foreign key(sequence_id) references Sequence(sequence_id) on delete CASCADE,
		foreign key(superfamily_id) references Family(family_id) on delete CASCADE);""")
		cursor.execute('commit')
		cursor.execute("""create table Belongs(
		monomer_id int,
		foreign key(monomer_id) references Monomer(monomer_id) on delete CASCADE,
		family_id int,
		foreign key(family_id) references Family(family_id) on delete CASCADE,
		primary key(monomer_id,family_id));""")
		cursor.execute('commit')
	elif banque not in ('n','y'):
		sys.exit("Syntax Error. Please check the help option -h")
	conn.commit()
	FT1 = time.time()
	print("Temps de la fonction clean_bank() :  %f" %(FT1 - T1))

def insert_taxon(taxon_name):
	"""insert the taxon"""
	T1 = time.time()
	#MV : Fichiers de taxons?
	try:
		cursor.execute("select taxon_name FROM Taxon WHERE taxon_name = (%s) ;", (taxon_name,))
		monCursor = cursor.fetchall()
		cursor.execute('commit')
	except psycopg2.Error as e:
			print (e.pgerror)	
	if not monCursor:
		#print("Error, this taxon is already in the database")
		#sys.exit(0)
		try:
			cursor.execute("insert into Taxon (taxon_name) values (%s) ;", (taxon_name,))
			cursor.execute('commit')
		except psycopg2.Error as e:
			print (e.pgerror)
	try:
		cursor.execute("select taxon_id from Taxon where taxon_name = %s ;", (taxon_name,))
	except psycopg2.Error as e:
		print (e.pgerror)
	for row in cursor:
		for i in row:
			taxon_id = i
	cursor.execute('commit')
	conn.commit()
	FT1 = time.time()
	print("Temps de la fonction insert_taxon() :  %f" %(FT1 - T1))
	return (taxon_id)

def clean_families(families):
	"""clean only family Collection"""
	families = families.lower()
	if families == 'y':
		try:
			cursor.execute("DROP TABLE Family CASCADE;")
		except psycopg2.Error as e:
			print (e.pgerror)
		cursor.execute('commit')
		cursor.execute("""create table Family(
		family_id serial UNIQUE,
		family_name varchar(30),
		sequence_id int,
		superfamily_id int,
		primary key(family_id),
		foreign key(sequence_id) references Sequence(sequence_id) on delete CASCADE,
		foreign key(superfamily_id) references Family(family_id) on delete CASCADE);""")
		cursor.execute('commit')
	conn.commit()
	
#~ def clean_monomer_dependency(dependency):
	#~ #à vérifier pour family ou families ? 
	#~ """clean monomers dependency to families"""
	#~ dependency = dependency.lower() 
	#~ if dependency == 'y':
		#~ M_MONGO.update({}, {"$unset" : {'families' : 1} }, upsert=True,multi=True)
		
def clean_cytoband_dependency(dependency):
	""" clean the cytobands informations on the chromosomes"""
	dependency = dependency.lower()
	if dependency == 'y':
		C_MONGO.update({}, { "$unset" :{'centromer' : 1} }, upsert=True, 
		multi=True)
		C_MONGO.update({}, { "$unset" : {'cytoband' : 1} }, upsert=True, 
		multi=True)


def insert_chromosome(sequence_chr_list, taxon_identifiant,description):
	""" insert chromsomome without their sequences.
	WARNING : the file enter cannot contain the sequence, 
	only chromosome name""" 
	T1 = time.time()
	buff = io.StringIO()
	liste1 = list()
	liste2 = list()
	taxon_id = str(taxon_identifiant)
	for i in sequence_chr_list:
		liste2.extend(i)
		liste2.append(taxon_id)
		liste1.extend(liste2)
		#print(liste1)
		liste2 = []
		buff.write("\t".join(liste1)+"\n")
		liste1 = []
	buff.seek(0)
	try:
		cursor.copy_from(buff,'Sequence',columns=('chromosome_name','sequence_name','taxon_id'))
		cursor.execute('commit')
	except psycopg2.Error as e:
		print (e.pgerror)
	conn.commit()
	FT1 = time.time()
	print("Temps de la fonction insert_chromosome() :  %f" %(FT1 - T1))



def gen_monomers_names(m_file):
	"""use by the generator, to get the monomers names"""
	with open(m_file) as monomer_file:
		for line in monomer_file:
			if not line.startswith('>'):
				continue
			yield line[1:-1]

def gen_monomers_dict_sequence(m_file):
	"""get a dict monomers name : sequence"""
	T1 = time.time()
	monomers = dict()
	for seq_record in SeqIO.parse(m_file, "fasta"):
		"""Dictionnaire nom_monomere : sequence_monomere"""
		monomers[seq_record.id] = str(seq_record.seq)
	FT1 = time.time()
	print("Temps de la fonction gen_monomers_dict_sequence() :  %f" %(FT1 - T1))
	return monomers
	

def gen_monomers_docs(m_file, seq_name_id):# ,i_file):
	""" generate the monomers docs for the generator"""
	T1 = time.time()
	t = io.StringIO()
	t2 = io.StringIO()
	liste1 = []
	"""Dictionnaire monomeres noms : sequences
	Appel de la fonction gen_monomers_dict_sequence"""
	block_sequence = gen_monomers_dict_sequence(m_file)
	for name in gen_monomers_names(m_file):
		parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1)$", name)
		if not parse:
			print ("Warning, invalid monomer %s " % name)
			continue
		"""Attribution des variables a leurs valeurs provenant du nom du monomere et separe par des "_" et recupere avec parse.groups()"""
		(sequence_name, block_begin, block_end, strand) = parse.groups()
		"""Obtention des positions sur le chromosome des monomeres a l'aide d'une fonction absolue_location()"""
		#(mono_begin, mono_end) = absolue_location(int(monomer_begin),int(monomer_end), int(block_begin), int(block_end), int(strand))
		"""Nom du block"""
		#block_name = '_'.join([chromosome_name, block_begin, block_end, strand])
		Sequence_id = seq_name_id[sequence_name]
		#sequence = block_sequence[name]
		#block_length = len(sequence)
		"""block_list[block_name] -> recuperation de block_id pour un nom de block donne
		block_sequence[name] -> recuperation de la sequence pour un nom de monomere donne"""
		liste1.extend([name,str(Sequence_id),block_begin,block_end,strand,block_sequence[name],str(len(block_sequence[name]))])
		#print(liste1)
		t.write("\t".join(liste1)+"\n")
		liste1 = []
	t.seek(0)
	#with open(i_file) as index_file:
		#for line in index_file:
			##print(line)
			#newline = line.split(" ")
			##print(newline)
			#block_name = newline[0]
			##print("nom")
			##print(block_name)
			#reference = newline[1]
			#index = newline[2]
			#for i in t:
				##bb +=1
				#bl_name = i.rstrip("\n").split("\t")[0]
				##print("buffer")
				##print(bl_name)
				#block_line = i.rstrip("\n")
				#if bl_name == block_name:
					#t2.write(block_line+"\t"+reference+"\t"+index+"\n")
			#t.seek(0)
	##print(t2.getvalue())
	#t2.seek(0)
	FT1 = time.time()
	print("Temps de la fonction gen_monomers_docs() :  %f" %(FT1 - T1))
	return(t)

def block_index_docs(i_file):
	ID1 = time.time()
	ID2 = time.clock()
	buffer_file = io.StringIO()
	#tmp = io.StringIO()	
	#liste_tmp = list()
	with open(i_file) as index_file:
		for line in index_file:
			newline = line.split(" ")
			block_name = newline[0]
			reference = newline[1]
			index = newline[2]
			index_array = index.rstrip("\n").lstrip(",").split(",")
			#print(index_array)
			index_array = list(map(int,index_array))
			#print(index_array)
			try:
				cursor.execute("SELECT reference_id FROM Reference WHERE reference_name = %s ;",(reference,))
			except psycopg2.Error as e:
				print (e.pgerror)
			reference_id = cursor.fetchone()
			cursor.execute('commit')
			try:
				cursor.execute("UPDATE Block SET block_index = %s, reference_id = %s WHERE block_name = %s ;",(index_array,reference_id[0],block_name,))
			except psycopg2.Error as e:
				print (e.pgerror)
			cursor.execute('commit')
			try:
				cursor.execute("SELECT block_name,block_id,sequence_id,block_nucleotide,block_index,block_strand FROM Block WHERE block_name = %s ;",(block_name,))
			except psycopg2.Error as e:
				print (e.pgerror)
			block_info = cursor.fetchone()
			block_name,block_id,sequence_id,block_sequence,block_index,block_strand = block_info
			cursor.execute('commit')
			#~ try:
				#~ cursor.execute("SELECT reference_nucleotide FROM Reference WHERE reference_id = %s ;",(reference_id[0],))
			#~ except psycopg2.Error as e:
				#~ print (e.pgerror)
			#~ for i in cursor:
				#~ reference = i
			#~ print(str(len(reference)))
			compteur_mono = False
			beginP = 0
			beginO = 0
			position = 0
			endP = None
			endO = None
			previous = -3
			#~ end_mono = 0
			#~ for j in block_index:
			#~ if j > end_mono:
				#~ end_mono = j
			"""Parcours de l'index"""
			for j in block_index:
				"""Condition fin d'un monomere :
				j inferieur au numero precedente
				j strictement superieur a 0
				position superieure a 0"""
				#if j < block_index[position - 1] and j > 0 and position != 0:
				if j < previous and j > 0:
					"""Condition fin d'un duo de monomeres"""
					if compteur_mono:
						endP = position - 1
						monomer_seq = block_sequence[beginO:endP+1]
						monomer = block_index[beginO:endP+1]
						monomer_unique = block_sequence[beginO:endO+1]
						monoName = "_".join((block_name,str(beginO),str(endO)))
						length = ((endO + 1) - (beginO + 1)) + 1
						buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginO + 1),str(endO + 1),str(block_strand),str(length)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\t"+monomer_unique+"\n")
						beginO = beginP
						beginP = position
						endO = endP
						"""Condition fin d'un monomere mais pas d'un duo de monomere"""
					else:
						compteur_mono = True
						endO = position - 1
						beginO = beginP
						beginP = position
				if position < (len(block_index)-1):
					position += 1
				if j > 0:
					previous = j
			"""Fin de la boucle"""
			if endO is None:
				endO = len(block_index) - 1
			monomer_seq = block_sequence[beginO:]
			monomer = block_index[beginO:]
			monomer_unique = block_sequence[beginO:endO+1]
			monoName = "_".join((block_name,str(beginO),str(endO)))
			length = ((endO + 1) - (beginO + 1)) + 1
			buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginO + 1),str(endO + 1),str(block_strand),str(length)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\t"+monomer_unique+"\n")
			if beginO != beginP:
				monomer_seq = block_sequence[beginP:]
				monomer = block_index[beginP:]
				monomer_unique = block_sequence[beginO:endO+1]
				monoName = "_".join((block_name,str(beginP),str(len(block_index))))
				length = (len(block_index) - (beginP + 1)) + 1
				buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginP + 1),str(len(block_index)),str(block_strand),str(length)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\t"+monomer_unique+"\n")
	buffer_file.seek(0)
	try:
		cursor.copy_expert("COPY Monomer(monomer_name,block_id,sequence_id,monomer_begin,monomer_end,monomer_strand,monomer_length,monomer_index,monomer_nucleotide,monomer_sequence) FROM STDIN;",buffer_file)
	except psycopg2.Error as e:
		print (e.pgerror)
	#print(buffer_file.getvalue())
	#print(str(index_array))
	conn.commit()
	FID1 = time.time()
	FID2 = time.clock()
	print("Temps total de la fonction block_index_docs() :  %f" %(FID1 - ID1))
	print("Temps CPU de la fonction block_index_docs() :  %f" %(FID2 - ID2))
			#liste_tmp.extend([block_name,reference,index])
			#tmp.write("\t".join(liste_tmp)+"\n")
			#liste_tmp = []
	#tmp.seek(0)
	#return(tmp)
		
#def gen_infos(m_file,chr_name_id):
	#"""with the monomers names, get all the informations 
	#about the monomers : begin, end, strand, block_begin, block_end, 
	#chromosome"""
	#T1 = time.time()
	#t2 = io.StringIO()
	#liste1 = []
	#liste2 = list()
	#for name in gen_monomers_names(m_file):
		#parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1):(\d+)-(\d+)$", name)
		#if not parse:
			#print ("Warning, invalid monomer %s " % name)
			#continue
		##Idee faire un dico de dico
		##info = dict(zip(('chromosome', 'block_begin', 'block_end', 'strand', 'monomer_begin', 'monomer_end'), parse.groups()))
		#(chromosome, block_begin, block_end, strand, monomer_begin, monomer_end) = parse.groups()
		#"""Calcule la position genomique de debut et de fin d'un monomere"""
		#(monomer_begin, monomer_end) = absolue_location(int(monomer_begin),int(monomer_end), int(block_begin),int(block_end),int(strand))
		#Chromosome_id = chr_name_id[chromosome]
		#block_name = '_'.join([chromosome, block_begin, block_end, strand])
		#if block_name not in liste2:
			#liste2.append(block_name)
			#liste1.extend([str(block_begin),str(block_end),str(strand),str(block_name),str(Chromosome_id)])
			#t2.write("\t".join(liste1)+"\n")
			#liste1 = []
	#t2.seek(0)
	#FT1 = time.time()
	#print("Temps de la fonction gen_infos() :  %f" %(FT1 - T1))
	#return(t2)

def create_data(m_file, taxon_id, i_file):
	"""insert blocks and monomers informations with bulk insert"""
	T1 = time.time()
	seq_name_id = dict()
	cursor.execute("select sequence_name, sequence_id from Sequence where taxon_id = %s ;", (taxon_id,))
	for row in cursor:
		seq_name_id[row[0]] = row[1]
	cursor.execute('commit')
	print(memory_usage())
	Tc = time.time()
	try:
		"""Appel de la fonction gen_monomers_docs"""
		cursor.copy_from(gen_monomers_docs(m_file,seq_name_id),'Block',sep='\t',columns=('block_name','sequence_id','block_begin','block_end','block_strand','block_nucleotide','block_length',))#'reference_id','block_index',))
	except psycopg2.Error as e:
		print (e.pgerror)
	conn.commit()
	block_index_docs(i_file)
	Td = time.time()
	print("Temps de la requete monomer :  %f" %(Td - Tc))
	FT1 = time.time()	  
	print("Temps de la fonction create_data() :  %f" %(FT1 - T1))

def create_index():
	try:
		cursor.execute("CREATE INDEX block_id_idx ON Monomer(block_id);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX sequence_id_idx ON Block(sequence_id);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX taxon_id_idx ON Sequence(taxon_id);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX reference_id_idx ON Block(reference_id);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX reference_name_idx ON Reference(reference_name);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	try:
		cursor.execute("CREATE INDEX block_name_idx ON Block(block_name);")
	except psycopg2.Error as e:
		print (e.pgerror)
	cursor.execute('commit')
	conn.commit()
	
def monomer_mode(c_file, m_file, k_option, i_file):#, taxon_id):
	"""'mode' activate to enter blocks, monomers, chromosomes and taxons
	informations"""
	T1 = time.time()
	sequence_name = list()
	chr_name = list()
	chr_description = dict()
	liste_sequence = list()
	infos_sequence = list()
	k_option = k_option.lower()
	if k_option == 'y':
		for seq_record in SeqIO.parse(c_file, "fasta"):
			chromosomeid[seq_record.id] = insert_chromosome_chunk(str(seq_record.id), taxon_id, str(seq_record.description), str(seq_record.seq).upper())
			"""Appel de la fonction create_data"""
			create_data(m_file, taxon_id, i_file)
	else : 
		with open(c_file, "r") as read_file:
			for line in read_file:
				new_line = line.rstrip("\n").split(";")
				taxon_id = insert_taxon(new_line[0])
				#new_line[-1] = new_line[-1].replace("\n", "")
				#~ if " " in new_line:
					#~ (seqname,chrname, chr_descr) = new_line.split(" ", 1)
					#~ liste_sequence.extend(seqname,chrname)
					#~ infos_sequence.append(liste_sequence)
					#~ liste_sequence = []
					#~ #sequence_name.append(seqname)
					#~ #chr_name.append(chrname)
					#~ chr_description[chrname] = chr_descr
				if not new_line[1]:
					#Construire une liste de liste
					chr_name = "unknown"
					liste_sequence.extend([chr_name,new_line[2]])
					infos_sequence.append(liste_sequence)
					liste_sequence = []
					#Construction d'un dictionnaire de description sans infos de description
					chr_description[new_line[2]] = new_line
				if new_line[1] == 'chrUn':
					chr_name = "unknown"
					#Construire une liste de liste
					liste_sequence.extend([chr_name,new_line[2]])
					infos_sequence.append(liste_sequence)
					liste_sequence = []
					#Construction d'un dictionnaire de description sans infos de description
					chr_description[new_line[2]] = new_line					
				else:
					#Construire une liste de liste
					liste_sequence.extend([new_line[1],new_line[2]])
					infos_sequence.append(liste_sequence)
					liste_sequence = []
					#chr_name.append(new_line[1])
					#sequence_name.append(new_line[2])
					#Construction d'un dictionnaire de description sans infos de description
					chr_description[new_line[2]] = new_line
				#print(infos_sequence)
				"""Appel de la fonction insert_chromosome"""
				insert_chromosome(infos_sequence, taxon_id, chr_description)
	try:
		cursor.execute("select taxon_id FROM Taxon;")
		taxons_id = cursor.fetchall()
		cursor.execute('commit')
	except psycopg2.Error as e:
		print (e.pgerror)
	for i in taxons_id:		
		"""Appel de la fonction create_data"""
		create_data(m_file, i['taxon_id'], i_file)
	"""Appel de la fonction create_index"""
	create_index()
	FT1 = time.time()
	print("Temps de la fonction monomer_mode() :  %f" %(FT1 - T1))

def reference_mode(r_file):
	for seq_record in SeqIO.parse(r_file, "fasta"):
		infos = str(seq_record.description)
		seq = str(seq_record.seq)
		infos = infos.split(" ")
		reference_name = infos[0]
		#print(reference_name)
		reference_comments = " ".join(infos[1:])
		try:
			cursor.execute("select reference_name FROM Reference WHERE reference_name = (%s) ;", (reference_name,))
			monCursor = cursor.fetchall()
			cursor.execute('commit')
		except psycopg2.Error as e:
				print (e.pgerror)	
		if monCursor:
			print("Error, this reference (%s) is already in the database" %(reference_name))
			sys.exit(0)
		try:
			cursor.execute("insert into Reference (reference_name,reference_comments,reference_nucleotide) values (%s,%s,%s) ;", (reference_name,reference_comments,seq,))
			cursor.execute('commit')
		except psycopg2.Error as e:
			print (e.pgerror)
	conn.commit()
TPS1 = time.time()  
TPC1 = time.clock()

def update_cytoband(cyto):
	"""add the cytobands informations(name, end, begin, giemsa stain)
	 on the chromosomes documents"""
	with open(cyto, "r") as cyto_file:
		for line in cyto_file:
			(c_name, begin, end, name, g_s) = (line.replace("\n", "").
			replace("    ", "\t").split("\t", 4))
			g_s = g_s.replace("\t", "")
			if C_MONGO.find_one({'name' : c_name}) is None:
				continue
			if g_s == "acen":
				C_MONGO.update({'name': c_name}, 
				{"$addToSet": {'centromer':{ "name":name , "end":end, 
				"begin":begin}}}, multi=True)
			else:
				C_MONGO.update({'name' : c_name}, 
				{"$addToSet": {"cytoband" : { "name": name, "end": end, 
			"begin":begin,"giemsa_stain":g_s}}},multi=True)

def family_infos(familyfile):
	"""create informations about families file for the generator"""
	with open (familyfile) as family_file:
		for name in family_file:
			infos_list = list()
			name = name.replace("\n", "")
			parse = re.match(r"^(.*)(	(\w+))+$", name)
			if not parse:
				#print ("Warning, invalid format %s " % name)
				continue
			infos_list = [ i for i in name.rstrip("\n").split("\t") ]
			if len(infos_list) >= 3:
				try:
					cursor.execute("SELECT family_id FROM Family WHERE family_name = %s;",(infos_list[2],))
				except psycopg2.Error as e:
					print (e.pgerror)
				superfamily_id = cursor.fetchone()
				try:
					cursor.execute("INSERT INTO Family (family_name,superfamily_id) VALUES (%s,%s);",(infos_list[1],superfamily_id[0],))
					cursor.execute('commit')
				except psycopg2.Error as e:
					print (e.pgerror)
			else:		
				try:
					cursor.execute("INSERT INTO Family (family_name) VALUES (%s);",(infos_list[1],))
					cursor.execute('commit')
				except psycopg2.Error as e:
					print (e.pgerror)
			try:
				cursor.execute("SELECT family_id FROM Family WHERE family_name = %s;",(infos_list[1],))
			except psycopg2.Error as e:
				print (e.pgerror)
			family_id = cursor.fetchone()
			try:
				cursor.execute("SELECT monomer_id FROM Monomer WHERE monomer_name = %s;",(infos_list[0],))
			except psycopg2.Error as e:
				print (e.pgerror)
			monomer_id = cursor.fetchone()
			try:
				cursor.execute("INSERT INTO Belongs (family_id,monomer_id) VALUES (%s,%s);",(family_id[0],monomer_id[0],))
			except psycopg2.Error as e:
				print (e.pgerror)
				
#~ def create_family(familyfile):
	#~ """generator of families informations"""
	#~ for family in family_infos(familyfile):
		#~ yield  {
						#~ 'name' : family['family_name'],
				#~ }


#~ def update_superfamilies(familiesfile):
	#~ """add informations about superfamilies to the families"""
	#~ with open(familiesfile, "r") as family_file:
		#~ for line in family_file:
			#~ (f_child, f_parent) =line.replace("\n", "").split(" ")
			#~ superfamily_id = F_MONGO.insert({'name':f_parent},continue_on_error=True)
			#~ F_MONGO.update({'name':f_child},{"$addToSet" :{'Superfamily' :{ 'Superfamily_ids':superfamily_id }}},multi=True)

def families_mode(families):
	"""add families informations to monomers, create families Collection
	and create an array of chromosome_ids in families Collection"""
	family_infos(families)
	#~ F_MONGO.insert(create_family(families), continue_on_error=True)
	#~ dic_mono_family = {d['monomer_name']:d for d in family_infos(families)}
	#~ for name in dic_mono_family:
		#~ M_MONGO.update({'name' : dic_mono_family[name]['monomer_name']}, 
		#~ {"$push" : {'families' : { 'name' : 
		#~ dic_mono_family[name]['family_name']}}}, 
		#~ continue_on_error=True, upsert=True, multi=True)
	#~ 
	#~ for name in dic_mono_family:
		#~ dic_mono_family[name]['Chromosome_id'] = list(M_MONGO.find(
		#~ {'name' : name}))[0]['Chromosome_id']
	#~ for name in dic_mono_family:
		#~ F_MONGO.update({'name' : dic_mono_family[name]['family_name']}, 
		#~ {"$addToSet" :{'Chromosome_ids' :{ 'Chromosome_id':
		#~ dic_mono_family[name]['Chromosome_id'] }}},multi=True)
		
def bigfamilies_mode(superfamilies):
	"""mode to add informations about superfamilies"""
	update_superfamilies(superfamilies)

#à finir + à tester
def cytoband_mode(cyto):
	"""mode to add informations about cytoband"""
	update_cytoband(cyto)

def supertaxon_mode(supertaxon_file):
	try:
		cursor.execute("UPDATE Taxon SET supertaxon_id = '';")
		cursor.execute('commit')
	except psycopg2.Error as e:
		print (e.pgerror)	
	with open(supertaxon_file, "r") as taxon_file:
		for line in taxon_file:
			infos = line.rstrip("\n").split("\t")
			try:
				cursor.execute("select taxon_name FROM Taxon WHERE taxon_name = (%s) ;", (infos[0],))
				monCursor = cursor.fetchall()
				cursor.execute('commit')
			except psycopg2.Error as e:
				print (e.pgerror)	
			if monCursor:
				print("This taxon is already in the database")
			else:
				try:
					cursor.execute("insert into Taxon (taxon_name) values (%s) ;", (infos[0],))
					cursor.execute('commit')
				except psycopg2.Error as e:
					print (e.pgerror)
			try:
				cursor.execute("SELECT taxon_id FROM Taxon WHERE taxon_name = (%s) ;", (infos[0],))
			except psycopg2.Error as e:
				print (e.pgerror)
			supertaxon_id = cursor.fetchone()
			cursor.execute('commit')
			supertaxon_id = supertaxon_id['taxon_id']
			try:
				cursor.execute("UPDATE Taxon SET supertaxon_id = %s WHERE taxon_name = %s;", (supertaxon_id, infos[1],))
				cursor.execute('commit')
			except psycopg2.Error as e:
				print (e.pgerror)
				
if args.m and args.c and args.i and args.k and args.F is None and args.f is None \
and args.cy is None:
	print("Monomer Mode")
	print(memory_usage())
	T1 = time.time()
	"""Appel de la fonction clean_banque"""
	clean_banque(args.clean)
	FT1 = time.time()
	print("Temps de l'appel de la fonction clean_banque() : %f" %(FT1 - T1))
	#T2 = time.time() 
	"""Appel de la fonction insert_taxon"""
	#TAXON_ID = insert_taxon(args.t)
	#TF2 = time.time()
	#print("Temps de l'appel de la fonction insert_taxon() : %f" %(TF2 - T2))	
	print("New Taxon created")
	T3 = time.time()
	"""Appel de la fonction monomer_mode"""
	monomer_mode(args.c, args.m, args.k, args.i)#, TAXON_ID)
	TF3 = time.time()
	print("Temps de l'appel de la fonction monomer_mode() : %f" %(TF3 - T3))
	cursor.close()
	conn.close()
	TPS2 = time.time() 
	print("temps s %f" %(TPS2 - TPS1))
	TPC2 = time.clock()
	print("temps CPU final %f" %(TPC2 - TPC1))
	sys.exit(0)
	
if args.r and args.c is None and args.m is None and args.F is None \
	and args.cy is None and args.k is None:
	print("Reference Mode")
	F1 = time.time()
	"""Appel de la fonction clean_banque"""
	clean_banque(args.clean)
	F2 = time.time()
	print("Temps de l'appel de la fonction clean_banque() : %f" %(F2 - F1))
	T1 = time.time()
	reference_mode(args.r)
	cursor.close()
	conn.close()
	T2 = time.time()
	print("temps s %f" %(T2 - T1))
	print(memory_usage())
	sys.exit(0)

if args.f and args.c is None and args.m is None and args.F is None \
	and args.cy is None and args.k is None:
	print("Family Mode")
	#clean_monomer_dependency(args.clean)
	clean_families(args.clean)
	families_mode(args.f)
	TPS2 = time.time() 
	print("temps seconde %s" %(TPS2 - TPS1))
	sys.exit(0)
	
if args.f and args.c and args.m and args.k and args.F is None \
	and args.cy is None:
	print("Monomer and Family Mode")
	clean_banque(args.clean)
	TAXON_ID = insert_taxon(args.t)
	monomer_mode(args.c, args.m, args.k, TAXON_ID)
	families_mode(args.f)
	TPS2 = time.time() 
	print("temps seconde : %s" %(TPS2 - TPS1))
	sys.exit(0)

if args.f and args.F and args.m is None and args.c is None \
	and args.cy is None and args.k is None:
	print("Family and BigFamily Mode")
	#clean_families(args.clean)
	#clean_monomer_dependency(args.clean)
	families_mode(args.f)
	bigfamilies_mode(args.F)
	TPS2 = time.time() 
	print("temps seconde %s" %(TPS2 - TPS1))
	sys.exit(0)

if args.F and args.f is None and args.m is None and args.c is None \
	and args.cy is None and args.k is None:
	print("BigFamily Mode")
	clean_families(args.clean)
	#clean_monomer_dependency(args.clean)
	bigfamilies_mode(args.F)
	TPS2 = time.time() 
	print("temps seconde %s" %(TPS2 - TPS1))
	sys.exit(0)

if args.T and args.f is None and args.m is None and args.c is None \
	and args.cy is None and args.k is None:
	print("Supertaxon Mode")
	supertaxon_mode(args.T)
	TPS2 = time.time() 
	print("temps seconde %s" %(TPS2 - TPS1))
	sys.exit(0)
		
if args.c and args.m and args.f and args.F and args.cy is None \
	and args.k:
	print("Monomer, Family and BigFamily Mode")
	clean_banque(args.clean)
	TAXON_ID = insert_taxon(args.t)
	monomer_mode(args.c, args.m, args.k, TAXON_ID)
	families_mode(args.f)
	bigfamilies_mode(args.F)
	TPS2 = time.time() 
	print("temps seconde %s" %TPS2 - TPS1)
	sys.exit(0)

if args.c and args.m and args.f and args.F and args.cy and args.k:
	print("Monomer, Family, BigFamily Mode, Cytoband Mode")
	clean_banque(args.clean)
	TAXON_ID = insert_taxon(args.t)
	monomer_mode(args.c, args.m, args.k, TAXON_ID)
	families_mode(args.f)
	bigfamilies_mode(args.F)
	cytoband_mode(args.cy)
	TPS2 = time.time() 
	print("temps seconde %s" %TPS2 - TPS1)
	sys.exit(0)

if args.c and args.m and args.cy and args.f is None and args.F is None \
	and args.k:
	print("Monomer and Cytoband Mode")
	clean_banque(args.clean)
	TAXON_ID = insert_taxon(args.t)
	print("New Taxon created")
	monomer_mode(args.c, args.m, args.k, TAXON_ID)
	cytoband_mode(args.cy)
	TPS2 = time.time() 
	print("temps s %f" %(TPS2 - TPS1))
	TPC2 = time.clock()
	print("temps CPU final %s" %(TPC2-TPC1))
	sys.exit(0)

if args.cy and args.m is None and args.c is None and args.f is None \
 and args.F is None and args.k is None:
	print("Cytoband Mode")
	#à voir pour $unset mongo ou gestion liste python ?? 
	clean_cytoband_dependency(args.clean)
	cytoband_mode(args.cy)
	sys.exit(0)
else:
	sys.exit("Syntax Error. Please check the help option -h")
