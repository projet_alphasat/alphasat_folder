#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import io
import time 
import sys
import argparse
import re
import psycopg2
import psycopg2.extras
from parsing_fonctions import (memory_usage, absolue_location, insert_chromosome_chunk)
import subprocess

try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
	#conn = psycopg2.connect(database = args.db)
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)

def delete():
	try:
		cursor.execute("UPDATE Taxon SET number_sequences = NULL, number_blocks = NULL, number_monomers = NULL;")
	except psycopg2.Error as e:
		print (e.pgerror)
	try:
		cursor.execute("UPDATE Chromosome SET number_sequences = NULL, number_blocks = NULL, number_monomers = NULL;")
	except psycopg2.Error as e:
		print (e.pgerror)	
	try:
		cursor.execute("UPDATE Sequence SET number_blocks = NULL, number_monomers = NULL;")
	except psycopg2.Error as e:
		print (e.pgerror)	
	try:
		cursor.execute("UPDATE Block SET number_monomers = NULL;")
	except psycopg2.Error as e:
		print (e.pgerror)

def count_taxons():
	try:
		cursor.execute("select taxon_id FROM Taxon;")
	except psycopg2.Error as e:
		print (e.pgerror)
	taxons_id = cursor.fetchall()
	cursor.execute('commit')
	for taxon in taxons_id:
		nb_sequences = 0
		nb_blocks = 0
		nb_monomers = 0
		try:
			cursor.execute("select count(sequence_id) FROM Sequence WHERE taxon_id = %s;",(taxon['taxon_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			nb_sequences = int(i[0])
		cursor.execute('commit')
		try:
			cursor.execute("select sequence_id FROM Sequence WHERE taxon_id = %s;",(taxon['taxon_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)
		sequences_id = cursor.fetchall()
		cursor.execute('commit')
		for j in sequences_id:					
			try:
				cursor.execute("select count(block_id) FROM Block WHERE sequence_id = %s;",(j['sequence_id'],))
			except psycopg2.Error as e:
				print (e.pgerror)
			for i in cursor:
				nb_blocks += int(i[0])
			cursor.execute('commit')
			try:
				cursor.execute("select count(monomer_id) FROM Monomer WHERE sequence_id = %s;",(j['sequence_id'],))
			except psycopg2.Error as e:
				print (e.pgerror)
			for i in cursor:
				nb_monomers += int(i[0]) 
			cursor.execute('commit')
		try:
			cursor.execute("UPDATE Taxon SET number_sequences = %s,number_blocks = %s,number_monomers = %s WHERE taxon_id = %s",(nb_sequences,nb_blocks,nb_monomers,taxon['taxon_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)			
		cursor.execute('commit')
	conn.commit()

def count_chromosomes():
	try:
		cursor.execute("select chromosome_id FROM Chromosome;")
	except psycopg2.Error as e:
		print (e.pgerror)
	chromosomes_id = cursor.fetchall()
	cursor.execute('commit')
	for chromosome in chromosomes_id:
		nb_sequences = 0
		nb_blocks = 0
		nb_monomers = 0
		try:
			cursor.execute("select count(sequence_id) FROM Sequence WHERE chromosome_id = %s;",(chromosome['chromosome_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			nb_sequences = int(i[0])
		cursor.execute('commit')
		try:
			cursor.execute("select sequence_id FROM Sequence WHERE chromosome_id = %s;",(chromosome['chromosome_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)
		sequences_id = cursor.fetchall()
		cursor.execute('commit')
		for j in sequences_id:					
			try:
				cursor.execute("select count(block_id) FROM Block WHERE sequence_id = %s;",(j['sequence_id'],))
			except psycopg2.Error as e:
				print (e.pgerror)
			for i in cursor:
				nb_blocks += int(i[0])
			cursor.execute('commit')
			try:
				cursor.execute("select count(monomer_id) FROM Monomer WHERE sequence_id = %s;",(j['sequence_id'],))
			except psycopg2.Error as e:
				print (e.pgerror)
			for i in cursor:
				nb_monomers += int(i[0]) 
			cursor.execute('commit')
		try:
			cursor.execute("UPDATE Chromosome SET number_sequences = %s,number_blocks = %s,number_monomers = %s WHERE chromosome_id = %s",(nb_sequences,nb_blocks,nb_monomers,chromosome['chromosome_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)			
		cursor.execute('commit')
	conn.commit()

def count_sequences():
	nb_blocks = 0
	nb_monomers = 0
	try:
		cursor.execute("select sequence_id FROM Sequence;")
	except psycopg2.Error as e:
		print (e.pgerror)
	sequences_id = cursor.fetchall()
	cursor.execute('commit')
	for sequence in sequences_id:			
		try:
			cursor.execute("select count(block_id) FROM Block WHERE sequence_id = %s;",(sequence['sequence_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			nb_blocks = int(i[0])
		cursor.execute('commit')
		try:
			cursor.execute("select count(monomer_id) FROM Monomer WHERE sequence_id = %s;",(sequence['sequence_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			nb_monomers = int(i[0])
		cursor.execute('commit')
		try:
			cursor.execute("UPDATE Sequence SET number_blocks = %s,number_monomers = %s WHERE sequence_id = %s",(nb_blocks,nb_monomers,sequence['sequence_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)			
		cursor.execute('commit')
	conn.commit()

def count_blocks():
	nb_monomers = 0
	try:
		cursor.execute("select block_id FROM Block;")
	except psycopg2.Error as e:
		print (e.pgerror)
	blocks_id = cursor.fetchall()
	cursor.execute('commit')
	for block in blocks_id:			
		try:
			cursor.execute("select count(monomer_id) FROM Monomer WHERE block_id = %s;",(block['block_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)
		for i in cursor:
			nb_monomers = int(i[0])
		cursor.execute('commit')
		try:
			cursor.execute("UPDATE Block SET number_monomers = %s WHERE block_id = %s",(nb_monomers,block['block_id'],))
		except psycopg2.Error as e:
			print (e.pgerror)			
		cursor.execute('commit')
	conn.commit()
	
	
delete()
count_blocks()
count_taxons()
count_chromosomes()
count_sequences()

