FILES:

nt_011630.fst:                  contig sequence (Human)
nt_011630_region.region.fst:    block sequences
nt_011630_region.monomers.fst:  monomer sequences

Pt_chr1.fa:                    chromosome sequence (Chimp)
Pt_chr1.region.fa:             block sequences


NAMING: 
NT_011630.14_230270_262823_-1                  contigName_begin_end_strand
NT_011630.14_230270_262823_-1:29740-29910      contigName_begin_end_strand:begin_end 

strand 1 and -1 (reverse strand)
begin/end for blocks:   are positions on the contig
begin/end for monomers: are positions on the block (becarefull when the block is in the the reverse strand)

# to check user/db:
psql -U alpha alphasats

grep ">"  /usm503/ponger/Gibbon_repeat/nomLeu3.fa | sed 's/>//' | awk '{split($1,t,"_");} {print "Nomascus leucogenys;" t[1] ";" $1}' |sort -u > nomLeu3.species_chr_seq.dat
 
 
cd ~/analyse_lauriane_data


awk '$1 ~ ">" {print nm "_1_" length(sq) "_1", sq; nm=$1;sq=""} $1 !~ ">" {sq=sq $1}' ~/analyse_lauriane_data/X10*rev.fst  |awk '{ print $1; print $2}' > X10.fa.regions.fst_np_L30.fst
awk '$1 ~ ">" {print nm "_1_" length(sq) "_1", sq; nm=$1;sq=""} $1 !~ ">" {sq=sq $1}' ~/analyse_lauriane_data/X7*rev.fst   |awk '{ print $1; print $2}' > X7.fa.regions.fst_np_L30.fst
awk '$1 ~ ">" {print nm "_1_" length(sq) "_1", sq; nm=$1;sq=""} $1 !~ ">" {sq=sq $1}' ~/analyse_lauriane_data/X9*rev.fst   |awk '{ print $1; print $2}' > X9.fa.regions.fst_np_L30.fst
awk '$1 ~ ">" {print nm "_1_" length(sq) "_1", sq; nm=$1;sq=""} $1 !~ ">" {sq=sq $1}' ~/analyse_lauriane_data/X8*rev.fst   |awk '{ print $1; print $2}' > X8.fa.regions.fst_np_L30.fst

awk '$1 ~ ">" {print nm, nm "_1_" length(sq) "_1"; nm=$1; sq=""} $1 !~ ">" {sq=sq $1}' ~/analyse_lauriane_data/X*rev.fst |sed 's/>//g' >q

awk 'BEGIN { while (getline < "q") {nm[$1]=$2}}  {print nm[$1], "momomer_A", $2}' ~/analyse_lauriane_data/X10*rc > X10.fa.regions.fst_np_L30.fst.rc.index
awk 'BEGIN { while (getline < "q") {nm[$1]=$2}}  {print nm[$1], "momomer_A", $2}' ~/analyse_lauriane_data/X9*rc  > X9.fa.regions.fst_np_L30.fst.rc.index
awk 'BEGIN { while (getline < "q") {nm[$1]=$2}}  {print nm[$1], "momomer_A", $2}' ~/analyse_lauriane_data/X8*rc  > X8.fa.regions.fst_np_L30.fst.rc.index
awk 'BEGIN { while (getline < "q") {nm[$1]=$2}}  {print nm[$1], "momomer_A", $2}' ~/analyse_lauriane_data/X7*rc  > X7.fa.regions.fst_np_L30.fst.rc.index

awk '{print "X10;chrUn;" $1}' q |grep X10 > X10.species_chr_seq.dat
awk '{print "X9;chrUn;" $1}' q |grep X9   > X9.species_chr_seq.dat
awk '{print "X8;chrUn;" $1}' q |grep X8   > X8.species_chr_seq.dat
awk '{print "X7;chrUn;" $1}' q |grep X7   > X7.species_chr_seq.dat


cat X10_cluster*.seq.tag.clean.fst2.rev.monomer.rc ~/alphasat_folder/data/data_for_DB/X10.fa.regions.fst_np_L30.index.dat
cat X9_cluster*.seq.tag.clean.fst2.rev.monomer.rc ~/alphasat_folder/data/data_for_DB/X9.fa.regions.fst_np_L30.index.dat
cat X8_cluster*.seq.tag.clean.fst2.rev.monomer.rc ~/alphasat_folder/data/data_for_DB/X8.fa.regions.fst_np_L30.index.dat
cat X7_cluster*.seq.tag.clean.fst2.rev.monomer.rc ~/alphasat_folder/data/data_for_DB/X7.fa.regions.fst_np_L30.index.dat
cat X6_cluster*.seq.tag.clean.fst2.rev.monomer.rc ~/alphasat_folder/data/data_for_DB/X6.fa.regions.fst_np_L30.index.dat
awk '$1 ~ ">" {print nm, sq}    {nm=$1;sq=""} $1 !~ ">" {sq=sq $1}' ~/analyse_lauriane_data/X9_cluster*.seq.tag.clean.fst2.rev.fst





cd ~/alphasat_folder/data/data_for_DB
 
# modifier  createPGDB_26_03.py pour varchar(100)
 
 
# To import example data:
cd data/data_for_DB
python3.4 ../../fill_pgSQL/createPGDB_26_03.py -r reference_alpha.fst -clean Y
python3.4 ../../fill_pgSQL/createPGDB_26_03.py -t "Nomascus_leucogenys"         -i Nomascus_leucogenys.fa.regions.fst_np_L30.index.dat        -m Nomascus_leucogenys.fa.regions.fst_np_L30.fst   -c Nomascus_leucogenys.species_chr_seq.dat -clean N -k N
python3.4 ../../fill_pgSQL/createPGDB_26_03.py -t "Pan troglodytes"             -i Pan_troglodytes.fa.regions.fst_np_L30.index.dat            -m Pan_troglodytes.fa.regions.fst_np_L30.fst       -c Pan_troglodytes.species_chr_seq.dat     -clean N -k N
python3.4 ../../fill_pgSQL/createPGDB_26_03.py -t "Homo sapiens (HGSC)"         -i Homo_sapiens_HGSC.fa.regions.fst_np_L30.index.dat          -m Homo_sapiens_HGSC.fa.regions.fst_np_L30.fst     -c Homo_sapiens_HGSC.species_chr_seq.dat   -clean N -k N
python3.4 ../../fill_pgSQL/createPGDB_26_03.py -t "Homo sapiens (HuRef)"        -i Homo_sapiens_HuRef.fa.regions.fst_np_L30.index.dat         -m Homo_sapiens_HuRef.fa.regions.fst_np_L30.fst    -c Homo_sapiens_HuRef.species_chr_seq.dat  -clean N -k N
python3.4 ../../fill_pgSQL/createPGDB_26_03.py -t "X10bis"                      -i X10.fa.regions.fst_np_L30.fst.rc.index                     -m X10.fa.regions.fst_np_L30.fst                   -c X10.species_chr_seq.dat                 -clean N -k N
python3.4 ../../fill_pgSQL/createPGDB_26_03.py -t "X9"                          -i X9.fa.regions.fst_np_L30.fst.rc.index                      -m X9.fa.regions.fst_np_L30.fst                    -c X9.species_chr_seq.dat                  -clean N -k N
python3.4 ../../fill_pgSQL/createPGDB_26_03.py -t "X8"                          -i X8.fa.regions.fst_np_L30.fst.rc.index                      -m X8.fa.regions.fst_np_L30.fst                    -c X8.species_chr_seq.dat                  -clean N -k N
python3.4 ../../fill_pgSQL/createPGDB_26_03.py -t "X7"                          -i X7.fa.regions.fst_np_L30.fst.rc.index                      -m X7.fa.regions.fst_np_L30.fst                    -c X7.species_chr_seq.dat                  -clean N -k N
 


# residuals connection to MongoDB in ORM:
#       - comment ORM imports   
#       - include PORT = 8000 in site_web*.py
cd 
python3.4 ./test_site_web_30_03.py
URL: http://10.8.128.240:8000/alphasat/accueil
ALTER TABLE block ALTER COLUMN block_name TYPE varchar(100);
ALTER TABLE sequence ALTER COLUMN sequence_name TYPE varchar(100);
ALTER TABLE sequence ALTER COLUMN sequence_name TYPE varchar(100) USING substr(sequence_name, 1, 100);
SELECT atttypmod FROM pg_attribute WHERE attrelid = 'sequence'::regclass AND attname = 'sequence_name';
SELECT atttypmod FROM pg_attribute WHERE attrelid = 'block'::regclass AND attname = 'block_name';
