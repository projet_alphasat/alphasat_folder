#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import time 
import sys
import argparse
import pymongo
import ORM
import re
from parsing_fonctions import (memory_usage, absolue_location, insert_chromosome_chunk)

T_MONGO = ORM.CONNECTOR['mongo'][ORM.DB]['Taxon']
B_MONGO = ORM.CONNECTOR['mongo'][ORM.DB]['Block']
M_MONGO = ORM.CONNECTOR['mongo'][ORM.DB]['Monomer']
C_MONGO = ORM.CONNECTOR['mongo'][ORM.DB]['Chromosome']
F_MONGO = ORM.CONNECTOR['mongo'][ORM.DB]['Family']

#Permet d'assurer l'unicité de chaque entrée selon la combinaison précisée
C_MONGO.ensure_index([('Taxon_id', 1), ('name', 1)], unique = True)
B_MONGO.ensure_index([('Chromosome_id', 1), ('name', 1 )], unique = True)
M_MONGO.ensure_index([( 'Block_id', 1), ('name', 1), 
('Chromosome_id', 1 )], unique = True)
F_MONGO.ensure_index([('name', 1)], unique = True)
T_MONGO.ensure_index([('name' , 1)], unique = True)
M_MONGO.ensure_index([('name', pymongo.HASHED)])
F_MONGO.ensure_index([('name', pymongo.HASHED)])


def clean_banque(banque):
    """clean all the Collections - drop the database"""
    banque = banque.lower() 
    if banque == 'y':
        F_MONGO.remove()
        C_MONGO.remove()
        T_MONGO.remove()
        M_MONGO.remove()
        B_MONGO.remove()
    elif banque not in ('n','y'):
        sys.exit("Syntax Error. Please check the help option -h")
            
def clean_families(families):
    """clean only family Collection"""
    families = families.lower()
    if families == 'y':
        F_MONGO.remove()
        
def clean_monomer_dependency(dependency):
    #à vérifier pour family ou families ? 
    """clean monomers dependency to families"""
    dependency = dependency.lower() 
    if dependency == 'y':
        M_MONGO.update({}, {"$unset" : {'families' : 1} }, upsert=True,
        multi=True)
        
def clean_cytoband_dependency(dependency):
    """ clean the cytobands informations on the chromosomes"""
    dependency = dependency.lower()
    if dependency == 'y':
        C_MONGO.update({}, { "$unset" :{'centromer' : 1} }, upsert=True, 
        multi=True)
        C_MONGO.update({}, { "$unset" : {'cytoband' : 1} }, upsert=True, 
        multi=True)
        
        
def insert_taxon(taxon_name):
    """insert the taxon"""
    taxon_id = T_MONGO.insert({'name' : taxon_name})
    return (taxon_id)


def insert_chromosome(chromosome_name, taxon_id, description):
    """ insert chromsomome without their sequences.
    WARNING : the file enter cannot contain the sequence, 
    only chromosome name"""
    # à modifier avec un générateur? 
    chr_tot = list()
    chromosome_id = list()
    for i in chromosome_name : 
        chromosome_document = {
                        'name': i,
                        'Taxon_id': taxon_id,
                        'Description' : description[i]
                    }
        chr_tot.append(chromosome_document)
    chromosome_id = C_MONGO.insert(chr_tot)
    return chromosome_id

def gen_monomers_names(m_file):
    """use by the generator, to get the monomers names"""
    with open(m_file) as monomer_file:
        for line in monomer_file:
            if not line.startswith('>'):
                continue
            yield line[1:-1]

def gen_monomers_docs(m_file, block_list):
    """ generate the monomers docs for the generator"""
    for name in gen_monomers_names(m_file):
        parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1):(\d+)-(\d+)$", name)
        if not parse:
            print ("Warning, invalid monomer %s " % name)
            continue
        (chromosome_name, block_begin, block_end, strand,
        monomer_begin, monomer_end) = parse.groups()
        (mono_begin, mono_end) = absolue_location(int(monomer_begin),
        int(monomer_end), int(block_begin), int(block_end), int(strand))
        block_name = '_'.join([chromosome_name, block_begin, block_end, 
        strand])
        yield {
            'name' : name,
            'begin' : mono_begin,
            'end' : mono_end,
            'strand' : strand,
            'block_id' : block_list[block_name]['_id'],
            'Chromosome_id' : block_list[block_name]['Chromosome_id'],
            }
            
def gen_infos(m_file):
    """with the monomers names, get all the informations 
    about the monomers : begin, end, strand, block_begin, block_end, 
    chromosome"""
    for name in gen_monomers_names(m_file):
        parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1):(\d+)-(\d+)$", name)
        if not parse:
            print ("Warning, invalid monomer %s " % name)
            continue
        info = dict(zip(('chromosome', 'block_begin', 'block_end', 
        'strand', 'monomer_begin', 'monomer_end'), parse.groups()))
        (monomer_begin, monomer_end) = absolue_location(
        int(info['monomer_begin']),int(info['monomer_end']), 
        int(info['block_begin']),int(info['block_end']),
        int(info['strand']))
        info['monomer_begin'] = monomer_begin
        info['monomer_end'] = monomer_end
        yield info

def gen_block_docs(m_file, chr_name_id):
    """with the monomers informations, insert the block document """
    for monomer_info in gen_infos(m_file):
        #yield {k:v for (k, v) in monomer_info.items() if k in 
        #('chromosome', 'block_begin', 'block_end', 'strand')}
        yield {
            'block_begin': monomer_info['block_begin'],
            'block_end': monomer_info['block_end'],
            'strand': monomer_info['strand'],
            'name': '_'.join([monomer_info['chromosome'], 
            monomer_info['block_begin'], monomer_info['block_end'], 
            monomer_info['strand']]),
            'Chromosome_id': chr_name_id[monomer_info['chromosome']],
        }
        
def create_data(m_file, taxon_id):
    """insert blocks and monomers informations with bulk insert"""
    chromosome_id_taxon = [info['_id'] for info in C_MONGO.find(
    {'Taxon_id' : taxon_id})]
    chr_name_id = { info['name'] : info['_id'] for info in C_MONGO.find(
    {'Taxon_id' : taxon_id})}
    B_MONGO.insert(gen_block_docs(m_file, chr_name_id), continue_on_error=True)
    block_list = dict()
    for i in chromosome_id_taxon:
        for info in B_MONGO.find({'Chromosome_id' : i}):
            block_list[info['name']] = info
    print(memory_usage())
    M_MONGO.insert(gen_monomers_docs(m_file, block_list),
    continue_on_error=True)

def update_cytoband(cyto):
    """add the cytobands informations(name, end, begin, giemsa stain)
     on the chromosomes documents"""
    with open(cyto, "r") as cyto_file:
        for line in cyto_file:
            (c_name, begin, end, name, g_s) = (line.replace("\n", "").
            replace("    ", "\t").split("\t", 4))
            g_s = g_s.replace("\t", "")
            if C_MONGO.find_one({'name' : c_name}) is None:
                continue
            if g_s == "acen":
                C_MONGO.update({'name': c_name}, 
                {"$addToSet": {'centromer':{ "name":name , "end":end, 
                "begin":begin}}}, multi=True)
            else:
                C_MONGO.update({'name' : c_name}, 
                {"$addToSet": {"cytoband" : { "name": name, "end": end, 
            "begin":begin,"giemsa_stain":g_s}}},multi=True)

def family_infos(familyfile):
    """create informations about families file for the generator"""
    with open (familyfile) as family_file:
        for name in family_file: 
            name = name.replace("\n", "")
            parse = re.match(r"^(.*) (\w+)$", name)
            if not parse:
                print ("Warning, invalid format %s " % name)
                continue
            info = dict(zip(('monomer_name','family_name'), parse.groups()))
            yield info

def create_family(familyfile):
    """generator of families informations"""
    for family in family_infos(familyfile):
        yield  {
                        'name' : family['family_name'],
                }


def update_superfamilies(familiesfile):
    """add informations about superfamilies to the families"""
    with open(familiesfile, "r") as family_file:
        for line in family_file:
            (f_child, f_parent) =line.replace("\n", "").split(" ")
            superfamily_id = F_MONGO.insert({'name':f_parent}, 
            continue_on_error=True)
            F_MONGO.update({'name':f_child}, 
            {"$addToSet" :{'Superfamily' :{ 'Superfamily_ids' 
            :superfamily_id }}},multi=True)

def monomer_mode(c_file, m_file, k_option, taxon_id):
    """'mode' activate to enter blocks, monomers, chromosomes and taxons
    informations"""
    chr_name = list()
    chr_description = dict()
    k_option = k_option.lower()
    if k_option == 'y':
        for seq_record in SeqIO.parse(c_file, "fasta"):
            chromosomeid[seq_record.id] = insert_chromosome_chunk(
            str(seq_record.id), taxon_id, str(seq_record.description), 
            str(seq_record.seq).upper())
            create_data(m_file, taxon_id)
    else : 
        with open(c_file, "r") as read_file:
            for line in read_file:
                new_line = line.split(">")[-1].replace("\n", "")
                if " " in new_line:
                    (chrname, chr_descr) = new_line.split(" ", 1)
                    chr_name.append(chrname)
                    chr_description[chrname] = chr_descr
                else:
                    chr_name.append(new_line)
                    chr_description[new_line] = new_line
    insert_chromosome(chr_name, taxon_id, chr_description)
    create_data(m_file, taxon_id)

def families_mode(families):
    """add families informations to monomers, create families Collection
    and create an array of chromosome_ids in families Collection"""
    F_MONGO.insert(create_family(families), continue_on_error=True)
    dic_mono_family = {d['monomer_name']:d for d in family_infos(families)}
    for name in dic_mono_family:
        M_MONGO.update({'name' : dic_mono_family[name]['monomer_name']}, 
        {"$push" : {'families' : { 'name' : 
        dic_mono_family[name]['family_name']}}}, 
        continue_on_error=True, upsert=True, multi=True)
    
    for name in dic_mono_family:
        dic_mono_family[name]['Chromosome_id'] = list(M_MONGO.find(
        {'name' : name}))[0]['Chromosome_id']
    for name in dic_mono_family:
        F_MONGO.update({'name' : dic_mono_family[name]['family_name']}, 
        {"$addToSet" :{'Chromosome_ids' :{ 'Chromosome_id':
        dic_mono_family[name]['Chromosome_id'] }}},multi=True)
        
def bigfamilies_mode(superfamilies):
    """mode to add informations about superfamilies"""
    update_superfamilies(superfamilies)

#à finir + à tester
def cytoband_mode(cyto):
    """mode to add informations about cytoband"""
    update_cytoband(cyto)



TPS1 = time.time()  
TPC1 = time.clock()
extended_help = """Several modes:

    MONOMER MODE:
    ============
    
    BEWARE: in this mode, if you choose -clean Y you clean the entiere database

    For this mode, -t -c -m -k -clean options are required
    This mode can be associated with Family mode and BigFamily mode
    
    eg:
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -k N -clean Y
    to enter a taxon name : hg19, clean the data base but not entered the sequences associated
    
    FAMILY MODE:
    ===========
    
    BEWARE : if clean option is choose in this mode, only the monomers' families
    dependences are removed
    
    For this mode, -t -f -clean options are required
    This mode can be associated with Monomer mode and BigFamily mode 
    
    eg:
    -t hg19 -f family_data.fst -clean N
    
    Here, you enter families data but you keep the informations 
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -clean Y
    
    If you want to clean the database you have to enter chromosome data 
    and family data
    
    BIGFAMILY MODE:
    ==============
    
    BEWARE : if clean option is choose in this mode, the entiere family data
    are removed and the monomers' families dependances
    
    For this mode, -t -clean options are required
    This mode can be associated with Family mode and Monomer/Family mode
    
    eg:
    -t hg19 -F bigfamily_data.fst -clean N
    
    Here, you enter bigfamilies data but you keep the informations and the families data
    are updated 
    
    -t hg19 -f family_data.fst -F bigfamily_data.fst -clean N
    
    Here, you keep monomers' and chromosomes' informations and you add 
    families and bigfamilies informations. The monomers' informations are
    updated
    
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -F bigfamily_data.fst-clean Y
    
    Here, you create en entiere new base with all the informations
    
    CYTOBAND MODE:
    ==============
    
    For this mode, -t -clean options are required
    This mode can be associated with Family mode and Monomer/Family/BigFamily mode
    
    Here, you keep monomers', chromosomes' and families' informations 
    and you add cytobands informations. The chromosomes' informations are
    updated
    
    e.g :
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -f family_data.fst -F bigfamily_data.fst -cy cytoband_data.fst -k N -clean Y
    
    Here, you create en entiere new base with all the informations
    
    -t hg19 -cy cytoband_data.fst -k N -clean Y
    
    Here, you clean the chromosomes' informations about the cytoband and update with new one
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -cy cytoband_data.fst -clean Y
    """
    
parser = argparse.ArgumentParser(
epilog=extended_help, formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("-t", help="taxon name", required=True, type=str)
parser.add_argument("-c", help="path chromosomes' file", type=str)
parser.add_argument("-m", help="path monomers' file",  type=str)
parser.add_argument("-f", help="path family file", type=str)
parser.add_argument("-F", help="path familyparents file", type=str)
parser.add_argument("-cy", help="path cytoband file", type=str)
parser.add_argument("-clean", help="the database will be totally cleaned, Y/N",
 required=True, type=str)
parser.add_argument("-k", help="the sequence will be entered or not Y/N", 
type=str)

args = parser.parse_args()

if args.m and args.c and args.k and args.F is None and args.f is None \
    and args.cy is None:
    print("Monomer Mode")
    print(memory_usage())
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    print("New Taxon created")
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    TPS2 = time.time() 
    print("temps s %f" %(TPS2 - TPS1))
    TPC2 = time.clock()
    print("temps CPU final %f" %(TPC2-TPC1))
    sys.exit(0)

if args.f and args.c is None and args.m is None and args.F is None \
    and args.cy is None and args.k is None:
    print("Family Mode")
    clean_monomer_dependency(args.clean)
    families_mode(args.f)
    TPS2 = time.time() 
    print("temps seconde %s" %(TPS2 - TPS1))
    sys.exit(0)
    
if args.f and args.c and args.m and args.k and args.F is None \
    and args.cy is None:
    print("Monomer and Family Mode")
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    families_mode(args.f)
    TPS2 = time.time() 
    print("temps seconde : %s" %TPS2 - TPS1)
    sys.exit(0)

if args.f and args.F and args.m is None and args.c is None \
    and args.cy is None and args.k is None:
    print("Family and BigFamily Mode")
    clean_families(args.clean)
    clean_monomer_dependency(args.clean)
    families_mode(args.f)
    bigfamilies_mode(args.F)
    TPS2 = time.time() 
    print("temps seconde %s" %TPS2 - TPS1)
    sys.exit(0)

if args.F and args.f is None and args.m is None and args.c is None \
    and args.cy is None and args.k is None:
    print("BigFamily Mode")
    clean_families(args.clean)
    clean_monomer_dependency(args.clean)
    bigfamilies_mode(args.F)
    TPS2 = time.time() 
    print("temps seconde %s" %TPS2 - TPS1)
    sys.exit(0)

if args.c and args.m and args.f and args.F and args.cy is None \
    and args.k:
    print("Monomer, Family and BigFamily Mode")
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    families_mode(args.f)
    bigfamilies_mode(args.F)
    TPS2 = time.time() 
    print("temps seconde %s" %TPS2 - TPS1)
    sys.exit(0)

if args.c and args.m and args.f and args.F and args.cy and args.k:
    print("Monomer, Family, BigFamily Mode, Cytoband Mode")
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    families_mode(args.f)
    bigfamilies_mode(args.F)
    cytoband_mode(args.cy)
    TPS2 = time.time() 
    print("temps seconde %s" %TPS2 - TPS1)
    sys.exit(0)

if args.c and args.m and args.cy and args.f is None and args.F is None \
    and args.k:
    print("Monomer and Cytoband Mode")
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    print("New Taxon created")
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    cytoband_mode(args.cy)
    TPS2 = time.time() 
    print("temps s %f" %(TPS2 - TPS1))
    TPC2 = time.clock()
    print("temps CPU final %s" %(TPC2-TPC1))
    sys.exit(0)

if args.cy and args.m is None and args.c is None and args.f is None \
 and args.F is None and args.k is None:
    print("Cytoband Mode")
    #à voir pour $unset mongo ou gestion liste python ?? 
    clean_cytoband_dependency(args.clean)
    cytoband_mode(args.cy)
    sys.exit(0)
    
else:
    sys.exit("Syntax Error. Please check the help option -h")

