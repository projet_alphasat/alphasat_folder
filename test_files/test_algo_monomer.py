#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import io
import psycopg2
import psycopg2.extras
import time

try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
reference = "GATC"
buffer_file = io.StringIO()
block_id = 1
sequence_id = 1
block_name = "block"
#block_sequence = "TTAGTGCATGCATGCATGC"
#block_sequence = "GAAAGTACttggtgatgtgtgtattcaactcacagacttgaacctttcttttgatagagcactgtTGCAAcgcactttttgtagaatctgcaagtgatcatttggagcgctttgctgcctatggtggaaaaagaaatatcttcacataaaactagacagaagcattttcagaaattcctttttgtgtgtgttcaattcacagagttgaaccgttcttttgatagagcagttttgaaacactgcctTTGtaggatctgcttgtggatatttggagctctttaaggaattcgttgtaaacgggataacttcacatacaaactagacagaagcattctcagaaactgctttgtgacgtgtgcattcaactcacagacttgagcctttcttttgatagaacagttttggaacacagatttttttagaatctgcaagtgttcatttggagttctttgttgcctatgttggaaaaagaattatattcatataaaaactagacggaagcattctcagaaactcctttgtgatgtgtgtgttcaattcataGAGTTGAAACATtcctttcttagagcagttttgaaacactgcttctgtagaatctgcttgtgtatatttggagctctttgaggaattcacTGTAAATGGGATAACTTCAGAtataaactggacagaagcattgtcagaaactgctttgtgatttgtgcattaaactcacagagtagcacattccttttgaaagagccgttttgaaacagtcttttggtag"
#block_index = [88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,1,2,3,4,5,6,7,8,9,10,11,12,13,14,0,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,103,104,105,106,107,108,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,1,2,3,4,5,6,7,8,9,10,11,12,0,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,0,163,164,165,166,167,168,169,170,171,1,2,3,4,5,6,7,8,9,10,11,12,13,14,0,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,1,2,3,4,5,6,7,8,9,10,11,12,0,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171]
#block_sequence = "TTTCAtatagcaggtttgaaacactctttttgtattatctcgatgtggacatttggagcgctttcaggcctatggtgaaaaaggaaatatcttctcctgaaaactagacagaagcattctcagaatcttatttgtgatgtgcgccctcaactaacagtgttgaagctatcttttgatagagcagttttgaaacactctttttgtaaaatctgcaagaggatatttgcatagctttgaggatttcattggaaacgggattgtcttcatataaactctagacagaagcattctgagaagcttcattgggatgtttcaattgaagtcacagtgttgaacagtccctttcatagagcaggtttgaaacactctttttgtagtatctggaagtggacatttggagcgctctcaggactacggtgaaaaaggaaatatcttccaataaaagctacatagaagcaatgtcagaaactttttcatgatgtatctactcagctaacagagttgaacctttcttttgagagagcagttttgaacactctttttg"
#block_index = [137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,-1,1,2,3,4,5,6,7,8,9,10,11,12,0,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,1,2,3,4,5,6,7,8,9,10,11,12,0,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,1,2,3,4,5,6,7,8,9,10,11,12,0,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,158,159,160,161,162,163,164,165,166,167,168]
block_sequence ="AATCGTATCCGTATCGATAAACGATCGAA"
block_index = [-1,-1,2,3,4,-1,1,-1,2,3,4,0,0,0,0,1,2,3,4,1,2,3,4,1,2,3,4,-2, -2]

print(str(len(block_sequence)))
#block_index = [-1,-1,1,0,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4]
#~ block_sequence = "TATCGATCGATCGT"
#~ block_index = [-1,1,2,3,4,1,2,3,4,1,2,3,4,-2]
print(str(len(block_index)))
#a = -1000
compteur_mono = 0
beginP = 0
beginO = 0
position = 0
endP = 0
endO = 0
counter_end = False
previous = -3
#~ end_mono = 0
#~ for j in block_index:
	#~ if j > end_mono:
		#~ end_mono = j
for j in block_index:
	#~ if j != -1 and j != -2:
	print(str(position))
	#if block_index[position - 1]:
	"""Condition fin de blocks"""
	#if position == (len(block_index)-1):
	#if j == -2 and not counter_end or position == (len(block_index)-1):
		#~ print("bb")
		#~ monomer_seq = block_sequence[beginO:]
		#~ monomer = block_index[beginO:]
		#~ monoName = "_".join((block_name,str(beginO),str(len(block_index))))
		#~ buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginO),str(len(block_sequence))))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\n")
		#~ monomer_seq = block_sequence[beginP:]
		#~ monomer = block_index[beginP:]
		#~ monoName = "_".join((block_name,str(beginP),str(len(block_index))))
		#~ buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginP),str(len(block_sequence))))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\n")
		#~ counter_end = True
	#else:
	"""Condition fin d'un duo de monomeres"""
	#if j < block_index[position - 1] and j > 0 and position != 0:
	if j < previous and j > 0:
		print("gg")
		if compteur_mono == 1:
			print("cc")
			endP = position - 1
			monomer_seq = block_sequence[beginO:endP+1]
			monomer = block_index[beginO:endP+1]
			#print(str(beginO))
			print(str(len(monomer)))
			monoName = "_".join((block_name,str(beginO),str(endO)))
			buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginO),str(endO)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\n")
			beginO = beginP
			#print(str(beginO))
			beginP = position
			endO = endP
			"""Condition fin d'un monomere mais pas d'un duo de monomere"""
		else:
			print("aa")
			compteur_mono += 1
			endO = position - 1
			beginO = beginP
			beginP = position
			#print(str(beginP))
			#print(str(position))
	#~ else:
		#~ print("tt")
	#print(str(position))
	position += 1
	if j > 0:
		previous = j
	#index = list(map(str,index))
	#sequence2 = ','.join(index[position_index:len(index)])#[pos:pos+60] #for pos in range(position_index, len(index), 60))
print("bb")
monomer_seq = block_sequence[beginO:]
monomer = block_index[beginO:]
print(str(len(monomer)))
monoName = "_".join((block_name,str(beginO),str(endO)))
buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginO),str(endO)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\n")
monomer_seq = block_sequence[beginP:]
print(str(len(monomer_seq)))
monomer = block_index[beginP:]
print(str(len(monomer)))
monoName = "_".join((block_name,str(beginP),str(len(block_index))))
buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginP),str(len(block_sequence)-1)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\n")
#counter_end = True
	#print(str(block_index[position + 1]))	
	#~ else:
		#~ print("zz")
		#~ beginP += 1
		#~ position += 1
				#~ if j < a and j > 0:
					#~ if compteur_mono == 1:
						#~ print("cc")
						#~ endMono = position
						#~ monomer_seq = block_sequence[beginMono-1:endMono]
						#~ print(monomer_seq)
						#~ monomer = block_index[beginMono-1:endMono]
						#~ print(str(monomer))
						#~ monoName = "_".join((block_name,str(beginMono),str(end)))
						#~ buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginMono),str(end)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\n")
						#~ monomer = []
						#~ compteur_mono = 0
						#~ monomer.append(j)
						#~ end = position
						#~ beginMono = begin
						#~ position += 1
						#~ begin = position
						#~ if j > 0:
							#~ a = j
					#~ else:
						#~ print("aa")
						#~ compteur_mono += 1
						#~ if j > 0:
							#~ a = j
						#~ end = position
						#~ beginMono = begin
						#~ position += 1
						#~ begin = position
				#~ else:
					#~ print("bb")
					#~ position += 1
					#~ if j > 0:
						#~ a = j
				#~ print(str(a))
				#~ print(str(compteur_mono))
#print(str(block_index[-1]))
buffer_file.seek(0)
print(buffer_file.getvalue())
conn.commit()
cursor.close()
conn.close()



#~ a = -1000
			#~ compteur_mono = 0
			#~ begin = 0
			#~ beginMono = 0
			#~ position = 0
			#~ end = 0
			#~ for j in block_index:
				#~ if j < a and j > 0:
					#~ if compteur_mono == 2:
						#~ monomer_seq = block_sequence[beginMono-1:end-1]
						#~ monomer = block_index[beginMono-1:end-1]
						#~ monoName = "_".join((block_name,str(beginMono),str(end)))
						#~ buffer_file.write("\t".join((monoName,str(block_id),str(sequence_id),str(beginMono),str(end)))+"\t"+"{"+",".join(map(str,monomer))+"}"+"\t"+monomer_seq+"\n")
						#~ monomer = []
						#~ compteur_mono = 0
						#~ compteur_mono += 1
						#~ monomer.append(j)
						#~ end = position
						#~ beginMono = begin
						#~ position += 1
						#~ begin = position
						#~ if j > 0:
							#~ a = j
					#~ else:
						#~ compteur_mono += 1
						#~ if j > 0:
							#~ a = j
						#~ end = position
						#~ beginMono = begin
						#~ position += 1
						#~ begin = position
				#~ else:
					#~ position += 1
					#~ if j > 0:
						#~ a = j
