#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import time 
import sys
import argparse
import re
import psycopg2
import psycopg2.extras
#from parsing_fonctions import (memory_usage, absolue_location, insert_chromosome_chunk)
		
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)

cursor.execute("SELECT tablename FROM pg_tables WHERE tablename !~ '^pg_' AND tablename !~ '^sql_';")
listeTables = list()
for row in cursor:
	for element in row:
		listeTables.append(element)
		print (str(element))
for i in listeTables:		
	cursor.execute("DROP TABLE "+i+" CASCADE;")
