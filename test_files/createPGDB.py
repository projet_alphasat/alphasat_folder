#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import time 
import sys
import argparse
import re
from parsing_fonctions import (memory_usage, absolue_location, insert_chromosome_chunk)
import psycopg2

try:
	conn = psycopg2.connect("dbname='test' user='test' host='localhost' password=''")
except:
	print "I am unable to connect to the database"	
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
cursor.execute("""create table Taxon(
taxon_id serial UNIQUE, 
taxon_name varchar(50), 
primary key(taxon_id));""")
cursor.execute('commit')
cursor.execute("""create table Chromosome(
chromosome_id serial UNIQUE,
chromosome_name varchar(30), 
primary key(chromosome_id), 
taxon_id varchar,
foreign key(taxon_id) references Taxon(taxon_id) on delete CASCADE );""")
cursor.execute('commit')
cursor.execute("""create table Chunk(
chunk_id serial UNIQUE,
chunk_begin int, 
chunk_end int
chunk_sequence text);""")
cursor.execute('commit')
cursor.execute("""create table Block(
block_id serial unique,
block_name varchar(30),
chromosome_id varchar,
block_begin int, 
block_end int, 
block_strand int
foreign key(chromosome_id) references Chromosome(chromosome_id) on delete CASCADE);""")
cursor.execute('commit')
cursor.execute("""create table Monomer(
monomer_id serial UNIQUE,
block_id varchar,
monomer_name varchar(30),
monomer_begin int, 
monomer_end int, 
monomer_strand int, 
monomer_sequence text
foreign key(block_id) references Block(block_id) on delete CASCADE);""")
cursor.execute('commit')
cursor.execute("""create table Family(
family_name varchar(30),
HOR_id text[][],
monomers_id text[][],
foreign key(HOR_id) references HOR(HOR_id) on delete CASCADE
foreign key(monomer_id) references monomer(monomer_id) on delete CASCADE
 );""")
 cursor.execute('commit')
cursor.execute("""create table HOR(
HOR_id serial UNIQUE,
ordre int,
monomers_id text[][],
foreign key(monomers_id) references monomer(monomer_id) on delete CASCADE);""")
cursor.execute('commit')

#Fonction utilisee
def clean_banque(banque):
    """clean all the Collections - drop the database"""
    banque = banque.lower() 
    if banque == 'y':
		cursor.execute("SELECT * FROM test;")
		for row in cursor:
			for col in row:
				cursor.execute("DROP TABLE "+col+" CASCADE;")
	cursor.execute('commit')			
    elif banque not in ('n','y'):
        sys.exit("Syntax Error. Please check the help option -h")

def clean_families(families):
    """clean only family Collection"""
    families = families.lower()
    if families == 'y':
        cursor.execute("DROP TABLE familie CASCADE;")

#Fonction utilisee   
def insert_taxon(taxon_name):
	"""insert the taxon"""
	#MV : Fichiers de taxons?
	try:
		cursor.execute("insert into Taxon (taxon_name) values ('" + taxon_name + "')")
		cursor.execute('commit')
	except psycopg2.Error as e:
			print e.pgerror
	cursor.execute("select taxon_id from Taxon where taxon_name = '" + taxon_name + "'")
	for row in cursor:
		row = taxon_id
	cursor.execute('commit')
    return (taxon_id)

#Fonction utilisee
def insert_chromosome(chromosome_file, taxon_id, description):
    """ insert chromsomome without their sequences.
    WARNING : the file enter cannot contain the sequence, 
    only chromosome name""" 
    inputFile = open(chromosome_file,"r")
    chromosome_id = list()
	for line in inputFile:
		try:
			cursor.execute("insert into Chromosome (chromosome_name,taxon_id) values ('" + line + "','" + taxon_id + "');")
			cursor.execute('commit')
			cursor.execute("select chromosome_id from Chromosome where chromosome_name = '" + line + "';")
			for row in cursor:
				chromosome_id.append(row)
			cursor.execute('commit')
		except psycopg2.Error as e:
			print e.pgerror
	inputFile.close()
    return chromosome_id   

#Fonction utilisee
def gen_monomers_names(m_file):
    """use by the generator, to get the monomers names"""
    with open(m_file) as monomer_file:
        for line in monomer_file:
            if not line.startswith('>'):
                continue
            yield line[1:-1]

#Fonction utilisee
def gen_monomers_dict_sequence(m_file):
    """get a dict monomers name : sequence"""
    monomers = dict()
    for seq_record in SeqIO.parse(m_file, "fasta"):
        monomers[seq_record.id] = str(seq_record.seq)
    return monomers

#Fonction utilisee
def gen_monomers_docs(m_file, block_list): #, block_list2):
    """ generate the monomers docs for the generator"""
    monomers_sequence = gen_monomers_dict_sequence(m_file)
    for name in gen_monomers_names(m_file):
        parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1):(\d+)-(\d+)$", name)
        if not parse:
            print ("Warning, invalid monomer %s " % name)
            continue
		#Attribution des variables a leurs valeurs provenant du nom du monomere et separe par des "_" et recupere avec parse.groups()
        (chromosome_name, block_begin, block_end, strand,monomer_begin, monomer_end) = parse.groups()
        #Obtention des positions sur le chromosome des monomeres a l'aide d'une fonction "absolue_location"
        (mono_begin, mono_end) = absolue_location(int(monomer_begin),int(monomer_end), int(block_begin), int(block_end), int(strand))
        #Nom du block
        block_name = '_'.join([chromosome_name, block_begin, block_end, strand])
        yield {
            name,
            mono_begin,
            mono_end,
            strand,
            block_list[block_name],
            #block_list2[block_name],
            monomers_sequence[name],
            }

#Fonction utilisee
def gen_infos(m_file):
    """with the monomers names, get all the informations 
    about the monomers : begin, end, strand, block_begin, block_end, 
    chromosome"""
    for name in gen_monomers_names(m_file):
        parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1):(\d+)-(\d+)$", name)
        if not parse:
            print ("Warning, invalid monomer %s " % name)
            continue
        info = dict(zip((chromosome, block_begin, block_end, strand, monomer_begin, monomer_end), parse.groups()))
        (monomer_begin, monomer_end) = absolue_location(int(info[monomer_begin]),int(info[monomer_end]), int(info[block_begin]),int(info[block_end]),int(info[strand]))
        info[monomer_begin] = monomer_begin
        info[monomer_end] = monomer_end
        yield info
 
#Fonction utilisee      
def gen_block_docs(m_file, chr_name_id):
    """with the monomers informations, insert the block document """
    for monomer_info in gen_infos(m_file):
        #yield {k:v for (k, v) in monomer_info.items() if k in 
        #('chromosome', 'block_begin', 'block_end', 'strand')}
        yield {
            block_begin = monomer_info[block_begin],
            block_end = monomer_info[block_end],
            strand = monomer_info[strand],
            name = '_'.join([monomer_info[chromosome], monomer_info[block_begin], monomer_info[block_end], monomer_info[strand]]),Chromosome_id: chr_name_id[monomer_info[chromosome]],
        }

#Fonction utilisee
def create_data(m_file, taxon_id):
    """insert blocks and monomers informations with bulk insert"""
    cursor.execute("select chomosome_id from Chromosome where taxon_id = '" + taxon_id + "';")
    chromosome_id_taxon = [row for row in cursor]
    cursor.execute('commit')
    cursor.execute("select name, chomosome_id from Chromosome where taxon_id = '" + taxon_id + "';")
    for row in cursor:
		chr_name_id = { row[0] : row[1] }
	cursor.execute('commit')
	try:
		cursor.execute("insert into Block (block_begin,block_end,block_strand,block_name) values ('" + gen_block_docs(m_file, chr_name_id) + "');")
		cursor.execute('commit')
	except psycopg2.Error as e:
		print e.pgerror
    block_list = dict() 
    for chr_id in chromosome_id_taxon:
		cursor.execute("select * from Block where chromosome_id = '" + chr_id + "';")
		for row in cursor:
			block_list[row[1]] = row[0]
			block_list2[row[1]] = row[2]
	cursor.execute('commit')
    print(memory_usage())
    try:
		cursor.execute("insert into Monomer (monomer_name,monomer_begin,monomer_end,monomer_strand,block_id,monomer_sequence) values ('" + gen_monomers_docs(m_file, block_list) + "');")
		cursor.execute('commit')
	except psycopg2.Error as e:
		print e.pgerror	
    
 
 def update_cytoband(cyto):
    """add the cytobands informations(name, end, begin, giemsa stain)
     on the chromosomes documents"""
    with open(cyto, "r") as cyto_file:
        for line in cyto_file:
            (c_name, begin, end, name, g_s) = (line.replace("\n", "").
            replace("    ", "\t").split("\t", 4))
            g_s = g_s.replace("\t", "")
            cursor.execute("select chromosome_name from Chromosome where chromosome_name = '" + c_name + "';")
            if cursor is None:
                continue
            if g_s == "acen":
				cursor.execute("update Chromosome SET chromosome_name = '" + c_name + "';")
                #C_MONGO.update({'name': c_name}, {"$addToSet": {'centromer':{ "name":name , "end":end, "begin":begin}}}, multi=True)
            else:
				cursor.execute("update Chromosome SET chromosome_name = '" + c_name + "';")
				#C_MONGO.update({'name' : c_name}, {"$addToSet": {"cytoband" : { "name": name, "end": end, "begin":begin,"giemsa_stain":g_s}}},multi=True)  
    
 def family_infos(familyfile):
    """create informations about families file for the generator"""
    with open (familyfile) as family_file:
        for name in family_file: 
            name = name.replace("\n", "")
            parse = re.match(r"^(.*) (\w+)$", name)
            if not parse:
                print ("Warning, invalid format %s " % name)
                continue
            info = dict(zip((monomer_name,family_name), parse.groups()))
            yield info   
 
def update_superfamilies(familiesfile):
    """add informations about superfamilies to the families"""
    with open(familiesfile, "r") as family_file:
        for line in family_file:
            (f_child, f_parent) =line.replace("\n", "").split(" ")
            cursor.execute("insert into Family (family_name) values ('" + f_parent + "');")
            cursor.execute("update Family SET 
            #F_MONGO.update({'name':f_child}, {"$addToSet" :{'Superfamily' :{ 'Superfamily_ids' :superfamily_id }}},multi=True)

#Fonction utilisee
def monomer_mode(c_file, m_file, k_option, taxon_id):
    """'mode' activate to enter blocks, monomers, chromosomes and taxons
    informations"""
    chr_name = list()
    chr_description = dict()
    k_option = k_option.lower()
    if k_option == 'y':
        for seq_record in SeqIO.parse(c_file, "fasta"):
            chromosomeid[seq_record.id] = insert_chromosome_chunk(
            str(seq_record.id), taxon_id, str(seq_record.description), 
            str(seq_record.seq).upper())
            create_data(m_file, taxon_id)
    else : 
        with open(c_file, "r") as read_file:
            for line in read_file:
                new_line = line.split(">")[-1].replace("\n", "")
                if " " in new_line:
                    (chrname, chr_descr) = new_line.split(" ", 1)
                    chr_name.append(chrname)
                    chr_description[chrname] = chr_descr
                else:
                    chr_name.append(new_line)
                    chr_description[new_line] = new_line
    insert_chromosome(chr_name, taxon_id, chr_description)
    create_data(m_file, taxon_id)


def families_mode(family_file):
    """add families informations to monomers, create families Collection
    and create an array of chromosome_ids in families Collection"""
	for family in family_infos(family_file):
		name = family[family_name]
		cursor.execute("insert into Family (family_name) values ('" + name + "');")
    dic_mono_family = {d[monomer_name]:d for d in family_infos(family_file)}
    for name in dic_mono_family:
		cursor.execute("insert into Family (family_name) values ('" + name + "');")
        #M_MONGO.update({'name' : dic_mono_family[name]['monomer_name']}, {"$push" : {'families' : { 'name' : dic_mono_family[name]['family_name']}}}, continue_on_error=True, upsert=True, multi=True)
    
    for name in dic_mono_family:
        dic_mono_family[name]['Chromosome_id'] = list(M_MONGO.find(
        {'name' : name}))[0]['Chromosome_id']
    for name in dic_mono_family:
        F_MONGO.update({'name' : dic_mono_family[name]['family_name']}, 
        {"$addToSet" :{'Chromosome_ids' :{ 'Chromosome_id':
        dic_mono_family[name]['Chromosome_id'] }}},multi=True)

def bigfamilies_mode(superfamilies):
    """mode to add informations about superfamilies"""
    update_superfamilies(superfamilies)

#à finir + à tester
def cytoband_mode(cyto):
    """mode to add informations about cytoband"""
    update_cytoband(cyto)


TPS1 = time.time()  
TPC1 = time.clock()
extended_help = """Several modes:

    MONOMER MODE:
    ============
    
    BEWARE: in this mode, if you choose -clean Y you clean the entiere database

    For this mode, -t -c -m -k -clean options are required
    This mode can be associated with Family mode and BigFamily mode
    
    eg:
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -k N -clean Y
    to enter a taxon name : hg19, clean the data base but not entered the sequences associated
    
    FAMILY MODE:
    ===========
    
    BEWARE : if clean option is choose in this mode, only the monomers' families
    dependences are removed
    
    For this mode, -t -f -clean options are required
    This mode can be associated with Monomer mode and BigFamily mode 
    
    eg:
    -t hg19 -f family_data.fst -clean N
    
    Here, you enter families data but you keep the informations 
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -clean Y
    
    If you want to clean the database you have to enter chromosome data 
    and family data
    
    BIGFAMILY MODE:
    ==============
    
    BEWARE : if clean option is choose in this mode, the entiere family data
    are removed and the monomers' families dependances
    
    For this mode, -t -clean options are required
    This mode can be associated with Family mode and Monomer/Family mode
    
    eg:
    -t hg19 -F bigfamily_data.fst -clean N
    
    Here, you enter bigfamilies data but you keep the informations and the families data
    are updated 
    
    -t hg19 -f family_data.fst -F bigfamily_data.fst -clean N
    
    Here, you keep monomers' and chromosomes' informations and you add 
    families and bigfamilies informations. The monomers' informations are
    updated
    
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -F bigfamily_data.fst-clean Y
    
    Here, you create en entiere new base with all the informations
    
    CYTOBAND MODE:
    ==============
    
    For this mode, -t -clean options are required
    This mode can be associated with Family mode and Monomer/Family/BigFamily mode
    
    Here, you keep monomers', chromosomes' and families' informations 
    and you add cytobands informations. The chromosomes' informations are
    updated
    
    e.g :
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -f family_data.fst -F bigfamily_data.fst -cy cytoband_data.fst -k N -clean Y
    
    Here, you create en entiere new base with all the informations
    
    -t hg19 -cy cytoband_data.fst -k N -clean Y
    
    Here, you clean the chromosomes' informations about the cytoband and update with new one
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -cy cytoband_data.fst -clean Y
    """
    
parser = argparse.ArgumentParser(
epilog=extended_help, formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("-t", help="taxon name", required=True, type=str)
parser.add_argument("-c", help="path chromosomes' file", type=str)
parser.add_argument("-m", help="path monomers' file",  type=str)
parser.add_argument("-f", help="path family file", type=str)
parser.add_argument("-F", help="path familyparents file", type=str)
parser.add_argument("-cy", help="path cytoband file", type=str)
parser.add_argument("-clean", help="the database will be totally cleaned, Y/N",required=True, type=str)
parser.add_argument("-k", help="the sequence will be entered or not Y/N", type=str)

args = parser.parse_args()

#Mode utilise
if args.m and args.c and args.k and args.F is None and args.f is None \
    and args.cy is None:
    print("Monomer Mode")
    print(memory_usage())
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    print("New Taxon created")
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    TPS2 = time.time() 
    print("temps s %f" %(TPS2 - TPS1))
    TPC2 = time.clock()
    print("temps CPU final %f" %(TPC2-TPC1))
    sys.exit(0)

if args.f and args.c is None and args.m is None and args.F is None \
    and args.cy is None and args.k is None:
    print("Family Mode")
    clean_monomer_dependency(args.clean)
    families_mode(args.f)
    TPS2 = time.time() 
    print("temps seconde %s" %(TPS2 - TPS1))
    sys.exit(0)
    
if args.f and args.c and args.m and args.k and args.F is None \
    and args.cy is None:
    print("Monomer and Family Mode")
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    families_mode(args.f)
    TPS2 = time.time() 
    print("temps seconde : %s" %TPS2 - TPS1)
    sys.exit(0)

if args.f and args.F and args.m is None and args.c is None \
    and args.cy is None and args.k is None:
    print("Family and BigFamily Mode")
    clean_families(args.clean)
    clean_monomer_dependency(args.clean)
    families_mode(args.f)
    bigfamilies_mode(args.F)
    TPS2 = time.time() 
    print("temps seconde %s" %TPS2 - TPS1)
    sys.exit(0)

if args.F and args.f is None and args.m is None and args.c is None \
    and args.cy is None and args.k is None:
    print("BigFamily Mode")
    clean_families(args.clean)
    clean_monomer_dependency(args.clean)
    bigfamilies_mode(args.F)
    TPS2 = time.time() 
    print("temps seconde %s" %TPS2 - TPS1)
    sys.exit(0)

if args.c and args.m and args.f and args.F and args.cy is None \
    and args.k:
    print("Monomer, Family and BigFamily Mode")
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    families_mode(args.f)
    bigfamilies_mode(args.F)
    TPS2 = time.time() 
    print("temps seconde %s" %TPS2 - TPS1)
    sys.exit(0)

if args.c and args.m and args.f and args.F and args.cy and args.k:
    print("Monomer, Family, BigFamily Mode, Cytoband Mode")
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    families_mode(args.f)
    bigfamilies_mode(args.F)
    cytoband_mode(args.cy)
    TPS2 = time.time() 
    print("temps seconde %s" %TPS2 - TPS1)
    sys.exit(0)

if args.c and args.m and args.cy and args.f is None and args.F is None \
    and args.k:
    print("Monomer and Cytoband Mode")
    clean_banque(args.clean)
    TAXON_ID = insert_taxon(args.t)
    print("New Taxon created")
    monomer_mode(args.c, args.m, args.k, TAXON_ID)
    cytoband_mode(args.cy)
    TPS2 = time.time() 
    print("temps s %f" %(TPS2 - TPS1))
    TPC2 = time.clock()
    print("temps CPU final %s" %(TPC2-TPC1))
    sys.exit(0)

if args.cy and args.m is None and args.c is None and args.f is None \
 and args.F is None and args.k is None:
    print("Cytoband Mode")
    #à voir pour $unset mongo ou gestion liste python ?? 
    clean_cytoband_dependency(args.clean)
    cytoband_mode(args.cy)
    sys.exit(0)
    
else:
    sys.exit("Syntax Error. Please check the help option -h")
