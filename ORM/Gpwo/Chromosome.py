# -*- coding: utf-8 -*-
from ..Mpwo.mpwo import MongoPersistentWebObject as Object, MongoPersistentWebObjects as Objects
from ..Mpwo.mpwf import MongoPersistentWebFolder as Folder, MongoPersistentWebFolders as Folders
import gzip
import re
import os
from tempfile import *
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_dna
from bson.objectid import ObjectId
import sys
import ipdb

from .Block import Blocks

class Chromosome(Object):
    def getCollection(self):
        return 'Chromosome'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)

    def splitSequence(self, sequence, width=10000):
        q, r = divmod(len(sequence), width)
        documents = []
        if q > 0:
            for i in range(0, q):
                documents.append({'n': i, 'sequence': sequence[i * width: (i+1) * width]})
        documents.append({'n': q, 'sequence': sequence[q * width:]})
        return documents

    def getChunks(self):
        return ChromosomeChunks(query=self.getForeignQuery(), sortby='n')

    def setChunks(self, sequence, width=10000, n=100):
        self.delChunks()
        documents = self.splitSequence(sequence, width)
        m = len(documents)
        q, r = divmod(m, n)
        for i in range(0, q):
            fewdocuments = documents[i * n: (i + 1) * n]
            chromosomeChunks = ChromosomeChunks(documents=fewdocuments)
            chromosomeChunks.setValues({'Chromosome_id': ObjectId(self.mid)})
            chromosomeChunks.insert()
        fewdocuments = documents[q * n:]
        chromosomeChunks = ChromosomeChunks(documents=fewdocuments)
        chromosomeChunks.setValues({'Chromosome_id': ObjectId(self.mid)})
        chromosomeChunks.insert()
        self.update({'chunkWidth': width})

    def delChunks(self):
        chromosomeChunks = ChromosomeChunks(query=self.getForeignQuery())
        chromosomeChunks.remove()

    chunks = property(getChunks, setChunks, delChunks, 'I am the chunks property')

    def getSequence(self):
        sequence = ''
        chromosomeChunks = self.getChunks()
        for document in chromosomeChunks.documents:
            sequence += document['sequence']
        return sequence

    def setSequence(self, sequence):
        self.setChunks(sequence)

    def delSequence(self):
        del self.chunks

    sequence = property(getSequence, setSequence, delSequence, 'I am the sequence property')
    
    def getDescription(self):
        return self.getValue('description')

    def setDescription(self, description):
        self.setValue('description', description)

    #def delDescription(self):
    #    del self.delValue('description')

    description = property(getDescription, setDescription, 'I am the description property')

    def getChunksFromQuery(self, subquery):
        query = self.getForeignQuery()
        for key in subquery:
            query[key] = subquery[key]
        return ChromosomeChunks(query=query, sortby='n')

    def getSubSequence(self, start, end):
        #width = self.getValue('chunkWidth')
        width = 10000
        """width est la taille d'un Chunk"""
        """permet de déterminer le chunk de début et de fin et le décalage dans les chunks"""
        #ipdb.set_trace()
        num_chunk_begin = start//width
        decalage_chunk_begin = start%width
        num_chunk_end = end//width
        decalage_chunk_end = end%width
        
        """sequence dans le même chunk"""
        if num_chunk_begin == num_chunk_end:
            subquery = {'n': num_chunk_begin}
            """requête dans la base mongo pour récupérer le chunk"""
            chromosomeChunks = self.getChunksFromQuery(subquery)
            return chromosomeChunks.documents[0]['sequence'][decalage_chunk_begin:decalage_chunk_end]
        else:
            subquery = {'n': {'$gte': num_chunk_begin, '$lte': num_chunk_end}}
            chromosomeChunks = self.getChunksFromQuery(subquery)
            startsequence = ''
            midsequence = ''
            endsequence = ''
            for document in chromosomeChunks.documents:
                if document['n'] == num_chunk_begin:
                    startsequence = document['sequence'][decalage_chunk_begin:]
                if document['n'] == num_chunk_end:
                    endsequence = document['sequence'][:decalage_chunk_end]
                if document['n'] > num_chunk_begin and document['n'] < num_chunk_end:
                    midsequence += document['sequence']
            return startsequence + midsequence + endsequence
            #voir pour avoir une meilleure syntaxe pour ne pas bouffer de la mémoire pour rien

    def resizeChunks(self, width):
        sequence = self.sequence
        self.setChunks(sequence, width)

    def setSequenceFromFasta(self, f, filepath):
        f = open(filepath, 'rb')
        sequence = ''#f.read()
        #description = ""
        name = ""
        #cette boucle permet de décoder le code récupéré :
        for line in f:
            s = line.decode()
            #ne récupère que les lignes qui ne commencent pas
            #par ">" (commentaire - ne nous intéresse pas)
            if s[0] != ">":
                #permet de supprimer les retour à la ligne dans la sequence
                sequence += s.replace('\n', '')
            else:
                name = s.split(" ")[0][1:]
                #description = s[1:].replace('\n', '')
        self.sequence = sequence
        #self.description = description
        self.name = name
        f.close()
        return len(sequence)
        
        
    def setSequenceFromFile(self, filepath, chromosome_name):
        """permet de récupérer le contenu d'un fichier fasta"""
        for record in SeqIO.parse("nt_14_15.fst", "fasta"):
            if chromosome_name == record.id:
                return str(record.seq)

    
    def setSequenceFromGzipFile(self, gzfilepath):
        """permet de récupérer le contenu d'un .gz"""
        f = gzip.open(gzfilepath, 'rb')
        return f
    
    def setSequenceFromFileTheTrueOne(self, filepath, chromosome_name):
        """ Permet de déterminer le type d'extention du fichier
            afin de savoir quel type d'action on va effectuer par la 
            suite (decompresser ou simple lecture)
            
            On définit ici une liste précise de type d'extensions
            acceptées pour les fichiers reçus
            
            """
        s = ""
        ext = os.path.splitext(filepath)
        fasta =  ['.fas', '.fast', '.fasta', '.fst', '.mult', '.txt']
        tar = ['.gz', '.bz2']
        if ext[1] in fasta :
            s = self.setSequenceFromFile(filepath, chromosome_name)
        elif ext[1] in tar:
            s = self.setSequenceFromGzipFile(filepath)
        lenght = self.setSequenceFromFasta(s, filepath)
        return lenght
        
    def setTaxonFromName(taxonName):
        return setParentFromField(Taxon, "name", taxonName, create = False)

    def getLength(self):
        m = 0
        for document in self.chunks.documents:
            m += len(document['sequence'])
        return m

    def searchForRegex(self, regex, chunknumber=2):
        d = {}
        r = []
        searchstart = 0
        for document in self.chunks.documents:
            d[document['n']] = document['sequence']
        m = len(d.keys())
        for n in range(0, m - chunknumber + 1):
            searchsequence = d[n]
            for i in range(1, chunknumber):
                try:
                    searchsequence += d[n + i]
                except:
                    print('too far!')
            result = re.finditer(regex, searchsequence)
            if result:
                for elem in result:
                    begin = elem.span()[0]
                    end = elem.span()[1]
                    r.append({'Chromosome_id': self.mid, 'location': [searchstart + begin, searchstart + end], 'string': elem.group()})
            searchstart += len(d[n])
        return r

    def getBlocks(self):
        return Blocks(query = self.getForeignQuery())
        

class Chromosomes(Objects):
    def getCollection(self):
        return 'Chromosome'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)

    def getChild(self, document):
        return Chromosome(document=document)

from .ChromosomeChunk import ChromosomeChunks
#from .Block import Blocks
