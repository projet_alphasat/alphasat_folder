from ..Mpwo.mpwo import MongoPersistentWebObject as Object, MongoPersistentWebObjects as Objects
from ..Mpwo.mpwf import MongoPersistentWebFolder as Folder, MongoPersistentWebFolders as Folders


class Block(Object):

    def getCollection(self):
        return 'Block'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
    
    def setChromosomeFromName(chromosomeName):
        return setParentFromField(Chromosome, "name", chromosomeName, create = True)
    
    """def getChromosomesFromName(self, taxon_id):
        #block_name = self.getValue('name')
        chromosome_name = '_'.join(self.getValue.split('_')[:-3])
        #gérer erreur si retourne 2 chromosomes, si 1 on gère, deux on retourne la liste
        chromosomes = Chromosomes(query= {'name' : chromosome_name, 'Taxon_id' : taxon_id} ) 
        if len(chromosomes.documents) == 1:
            return Chromosome(document = chromosomes.documents[0])
        else:
            return None 

    def getChromosomeIdFromName(self, taxon_id):
        return self.getChromosomeFromName(taxon_id).mid
    
    def setChromosomeIdFromName(self, taxon_id):
        chromosome_id = self.getChromosomeIdFromName(taxon_id)
        if chromosome_id == None:
            self.setValue("Chromosome_id", chromosome_id)
            return True
        else:
            return False"""
            
    def getMonomers(self):
        return Monomers(query = self.getForeignQuery())

class Blocks(Objects):

    def getCollection(self):
        return 'Block'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
    
    def setChromosomeFromName(chromosomeName):
        return setParentFromField(Chromosome, "name", chromosomeName, create = True)
        
    def getChild(self, document):
        return Block(document=document)

from .Monomer import Monomers
