# -*- coding: utf-8 -*-
GPWO_SCHEMA = {
    'Dsb': {
        'folder': {
	    'tabs': {
	    'selection':  {
                'title': 'Selection',
                'type': 'DataTable',
                'actionName': 'getDsbsTableData',
                'request': {},
                'toolbar': []
	    },
            'export': {
                'title': 'Export',
                'type': 'FieldForm',
                'fields': {
                    'correction': {
                        'type': 'radiobuttons',
                        'options': {
                            'raw': 'Raw',
                            'correction': 'Correction'
                        }
                     },
                    'mode': {
                        'type': 'radiobuttons',
                        'options': {
                            'blast': 'Blast',
                            'ssaha2': 'Ssaha 2'
                        }
                    },
                    'types': {
                        'type': 'select',
                        'multiple': 'multiple',
                        'options': {
                            'unreliables': 'Unreliables',
                            'deletions': 'Deletions',
                            'insertions': 'Insertions',
                            'mutations': 'Mutations',
                            'wildtype': 'Wildtype'
                        }
                    },
                    'format': {
                        'type': 'radiobuttons',
                        'options': {
                            'zip': 'Zipped folder',
                            'excel': 'Multi Tab Excel'
                        }
                    }
                },
                'callbacks': [],
                 'toolbar': [
                    {'caption': 'ROI Data', 'url': 'export', 'subUrl': 'roidata', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                    {'caption': 'Alignments Data', 'url': 'export', 'subUrl': 'alignmentsdata', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                    {'caption': 'Type and length change Data', 'url': 'export', 'subUrl': 'typeandlengthchangedata', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'}
                ]
            }
	  }
	},
        'default': {
            'title': 'New Dsb',
            'description': 'Describe this Dsb',
            'files': [],
            'folders': [],
            'parameters': {
                'blast': {
                    'reward': 1,
                    'penalty':-2,
                    'gapopen': 1,
                    'gapextend': 1,
                    'word_size': 11
                },
                'ssaha2': {

                },
                'correction': {
                  'C': {
                        'contraction': {'correct': 'on', 'letter': {'min': 1, 'max': 3}, 'gap': {'min': 1, 'max': 3}},
                        'extension': {'correct': 'on', 'letter': {'min': 1, 'max': 3}, 'gap': {'min': 1, 'max': 3}}
                  },
                  'A': {
                        'contraction': {'correct': 'on', 'letter': {'min': 1, 'max': 3}, 'gap': {'min': 1, 'max': 3}},
                        'extension': {'correct': 'on', 'letter': {'min': 1, 'max': 3}, 'gap': {'min': 1, 'max': 3}}
                  },
                  'T': {
                        'contraction': {'correct': 'on', 'letter': {'min': 1, 'max': 3}, 'gap': {'min': 1, 'max': 3}},
                        'extension': {'correct': 'on', 'letter': {'min': 1, 'max': 3}, 'gap': {'min': 1, 'max': 3}}
                  },
                  'G': {
                        'contraction': {'correct': 'on', 'letter': {'min': 1, 'max': 3}, 'gap': {'min': 1, 'max': 3}},
                        'extension': {'correct': 'on', 'letter': {'min': 1, 'max': 3}, 'gap': {'min': 1, 'max': 3}}
                  }
                },
                'minlength': 90
            }
        },
        'title': 'title',
        'description': 'description',
        'tabs': {
            'files': {
                'title': 'Files',
                'type': 'FieldForm',
                'fields': {
                    'files': {'type': 'file'},
                    'source': {'type': 'relation', 'className': 'Source'}
                },
                'callbacks': [],
                 'toolbar': [
                    {'caption': 'Blast', 'actionName': 'blast', 'request': {}, 'type': 'remote', 'color': 'warning', 'icon': 'calculator'},
                    {'caption': 'Ssaha 2', 'actionName': 'ssaha2', 'request': {}, 'type': 'remote', 'color': 'warning', 'icon': 'calculator'},
                    {'caption': 'Needle Blast', 'actionName': 'needle', 'request': {'target': 'blast'}, 'type': 'remote', 'color': 'inverse', 'icon': 'calculator'},
                    {'caption': 'Needle Ssaha 2', 'actionName': 'needle', 'request': {'target': 'ssaha2'}, 'type': 'remote', 'color': 'inverse', 'icon': 'calculator'},
                    {'caption': 'Remove all', 'actionName': 'removeFiles', 'request': {}, 'type': 'remote', 'color': 'danger', 'icon': 'calculator'},
                    {'caption': 'Download Fasta File', 'url': 'fasta', 'subUrl': 'sub', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                    {'caption': 'View HTML Report', 'url': 'report', 'subUrl': 'html', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'}
                ]
            },
            'parameters': {
                'title': 'Parameters',
                 'type': 'FieldForm',
                'fields': {
                    'parameters': {
                        'type': 'fieldset',
                        'elements': {
                            'minlength': {'type': 'number'},
                            'blast': {
                                'type': 'fieldset',
                                'elements': {
                                    'reward': {'type': 'number'},
                                    'penalty': {'type': 'number'},
                                    'gapopen': {'type': 'number'},
                                    'gapextend': {'type': 'number'},
                                    'word_size': {'type': 'number'}
                                }
                            },
                            'ssaha2': {
                                'type': 'fieldset',
                                'elements': {
                                    'reward': {'type': 'number'},
                                    'penalty': {'type': 'number'},
                                    'gapopen': {'type': 'number'},
                                    'gapextend': {'type': 'number'},
                                    'word_size': {'type': 'number'}
                                }
                            },
                            'correction': {
                                'type': 'fieldset',
                                'elements': {
                                    'A': {
                                        'type': 'fieldset',
                                        'elements': {
                                            'contraction': {
                                                'type': 'fieldset',
                                                'elements': {
                                                    'correct': {'type': 'checkbox'},
                                                    'letter': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    },
                                                    'gap': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    }
                                                }
                                            },
                                            'extension': {
                                                'type': 'fieldset',
                                                'elements': {
                                                    'correct': {'type': 'checkbox'},
                                                    'letter': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    },
                                                    'gap': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    'T': {
                                        'type': 'fieldset',
                                        'elements': {
                                            'contraction': {
                                                'type': 'fieldset',
                                                'elements': {
                                                    'correct': {'type': 'checkbox'},
                                                    'letter': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    },
                                                    'gap': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    }
                                                }
                                            },
                                            'extension': {
                                                'type': 'fieldset',
                                                'elements': {
                                                    'correct': {'type': 'checkbox'},
                                                    'letter': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    },
                                                    'gap': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    'C': {
                                          'type': 'fieldset',
                                        'elements': {
                                            'contraction': {
                                                'type': 'fieldset',
                                                'elements': {
                                                    'correct': {'type': 'checkbox'},
                                                    'letter': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    },
                                                    'gap': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    }
                                                }
                                            },
                                            'extension': {
                                                'type': 'fieldset',
                                                'elements': {
                                                    'correct': {'type': 'checkbox'},
                                                    'letter': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    },
                                                    'gap': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    'G': {
                                         'type': 'fieldset',
                                        'elements': {
                                            'contraction': {
                                                'type': 'fieldset',
                                                'elements': {
                                                    'correct': {'type': 'checkbox'},
                                                    'letter': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    },
                                                    'gap': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    }
                                                }
                                            },
                                            'extension': {
                                                'type': 'fieldset',
                                                'elements': {
                                                    'correct': {'type': 'checkbox'},
                                                    'letter': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    },
                                                    'gap': {
                                                        'type': 'fieldset',
                                                        'elements': {
                                                              'min': 'number',
                                                              'max': 'number'
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                'callbacks': [],
                'toolbar': []
            },
            'blastsearch': {
                'title': 'Blast Search Engine',
                'type': 'DataTable',
                'actionName': 'getSearchTableData',
                'request': {'target':'blast'},
                'toolbar': []
            },
            'ssahasearch': {
                'title': 'Ssaha 2 Search Engine',
                'type': 'DataTable',
                'actionName': 'getSearchTableData',
                'request': {'target':'ssaha2'},
                'toolbar': []
            },
            'needlesearch': {
                'title': 'Needle Search Engine',
                'type': 'DataTable',
                'actionName': 'getSearchTableData',
                'request': {'target':'needle'},
                'toolbar': []
            },
            'blastraw': {
                'title': 'Blast Raw',
                'type': 'TabView',
                'subtabs': {
                    'unreliables': {
                        'title': 'Unreliables',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'unreliable'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/raw/multalin/unreliable', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/raw/wise/unreliable', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/raw/wise/simple/unreliable', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/raw/wise/unreliable', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/raw/unreliable', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'deletions': {
                        'title': 'Deletions',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'deletion'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/raw/multalin/deletion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/raw/wise/deletion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/raw/wise/simple/deletion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/raw/wise/deletion', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/raw/deletion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'insertions': {
                        'title': 'Insertions',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'insertion'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/raw/multalin/insertion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/raw/wise/insertion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/raw/wise/simple/insertion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/raw/wise/insertion', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/raw/insertion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'wildtypes': {
                        'title': 'Wildtypes',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'wildtype'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/raw/multalin/wildtype', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/raw/wise/wildtype', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/raw/wise/simple/wildtype', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/raw/wise/wildtype', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/raw/wildtype', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'mutations': {
                        'title': 'Mutations',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'mutation'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/raw/multalin/mutation', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/raw/wise/mutation', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/raw/wise/simple/mutation', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/raw/wise/mutation', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/raw/mutation', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'unaligneds':{
                        'title': 'Blast Unaligneds',
                        'type': 'DataTable',
                        'actionName': 'getUnalignedSummaryTableData',
                        'request': {'target': 'blast'},
                        'toolbar': [
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/raw/wise/unaligneds', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'}
                        ]
                    },
                    'types':{
                        'title': 'Type repartition',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'blast/raw/type/html'
                    },
                    'lengths':{
                        'title': 'Length Change',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'blast/raw/length/html'
                    },
                    'gaps':{
                        'title': 'Gaps',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'blast/raw/gaps/html'
                    }
                }
            },
            'ssaharaw': {
                'title': 'Ssaha 2 Raw',
                'type': 'TabView',
                'subtabs': {
                    'unreliables': {
                        'title': 'Unreliables',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'unreliable'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/multalin/unreliable', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/wise/unreliable', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/wise/simple/unreliable', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/raw/wise/unreliable', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/raw/unreliable', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'deletions': {
                        'title': 'Deletions',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'deletion'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/multalin/deletion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/wise/deletion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/wise/simple/deletion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/raw/wise/deletion', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/raw/deletion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'insertions': {
                        'title': 'Insertions',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'insertion'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/multalin/insertion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/wise/insertion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/wise/simple/insertion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/raw/wise/insertion', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/raw/insertion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'wildtypes': {
                        'title': 'Wildtypes',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'wildtype'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/multalin/wildtype', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/wise/wildtype', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/wise/simple/wildtype', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/raw/wise/wildtype', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/raw/wildtype', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'mutations': {
                        'title': 'Mutations',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'mutation'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/multalin/mutation', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Table', 'url': 'pairs', 'subUrl': 'ssaha2/raw/wise/mutation', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/wise/simple/mutation', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/raw/wise/mutation', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/raw/mutation', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'unaligneds':{
                        'title': 'Ssaha 2 Unaligneds',
                        'type': 'DataTable',
                        'actionName': 'getUnalignedSummaryTableData',
                        'request': {'target': 'ssaha2'},
                        'toolbar': [
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/raw/wise/unaligneds', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'}
                        ]
                    },
                    'types':{
                        'title': 'Type repartition',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'ssaha2/raw/type/html'
                    },
                    'lengths':{
                        'title': 'Length Change',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'ssaha2/raw/length/html'
                    },
                    'gaps':{
                        'title': 'Gaps',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'ssaha2/raw/gaps/html'
                    }
                }
            },
            'blastcorrection': {
                'title': 'Blast Correction',
                'type': 'TabView',
                'subtabs': {
                    'unreliables': {
                        'title': 'Unreliables',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'unreliable', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/correction/multalin/unreliable', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/correction/wise/unreliable', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/correction/wise/simple/unreliable', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/correction/wise/unreliable', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/correction/unreliable', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'deletions': {
                        'title': 'Deletions',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'deletion', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/correction/multalin/deletion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/correction/wise/deletion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/correction/wise/simple/deletion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/correction/wise/deletion', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/correction/deletion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'insertions': {
                        'title': 'Insertions',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'insertion', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/correction/multalin/insertion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/correction/wise/insertion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/correction/wise/simple/insertion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/correction/wise/insertion', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/correction/insertion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'wildtypes': {
                        'title': 'Wildtypes',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'wildtype', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/correction/multalin/wildtype', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/correction/wise/wildtype', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/correction/wise/simple/wildtype', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/correction/wise/wildtype', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/correction/wildtype', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'mutations': {
                        'title': 'Mutations',
                        'type': 'DataTable',
                        'actionName': 'getBlastSummaryTableData',
                        'request': {'grouptype': 'mutation', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'blast/correction/multalin/mutation', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Table', 'url': 'pairs', 'subUrl': 'blast/correction/wise/mutation', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart', 'url': 'alignmentchart', 'subUrl': 'blast/correction/wise/simple/mutation', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/correction/wise/mutation', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'blast/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Blast Roi Csv', 'url': 'roicsv', 'subUrl': 'blast/correction/mutation', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'unaligneds':{
                        'title': 'Blast Unaligneds',
                        'type': 'DataTable',
                        'actionName': 'getUnalignedSummaryTableData',
                        'request': {'target': 'blast'},
                        'toolbar': [
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'blast/correction/wise/unaligneds', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'}
                        ]
                    },
                    'types':{
                        'title': 'Type repartition',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'blast/correction/type/html'
                    },
                    'lengths':{
                        'title': 'Length Change',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'blast/correction/length/html'
                    },
                    'gaps':{
                        'title': 'Gaps',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'blast/correction/gaps/html'
                    }
                }
            },
            'ssahacorrection': {
                'title': 'Ssaha 2 Correction',
                'type': 'TabView',
                'subtabs': {
                    'unreliables': {
                        'title': 'Unreliables',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'unreliable', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/multalin/unreliable', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/wise/unreliable', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/wise/simple/unreliable', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/correction/wise/unreliable', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/correction/unreliable', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'deletions': {
                        'title': 'Deletions',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'deletion', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/multalin/deletion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/wise/deletion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/wise/simple/deletion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/correction/wise/deletion', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/correction/deletion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'insertions': {
                        'title': 'Insertions',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'insertion', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/multalin/insertion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/wise/insertion', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/wise/simple/insertion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/correction/wise/insertion', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/correction/insertion', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'wildtypes': {
                        'title': 'Wildtypes',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'wildtype', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/multalin/wildtype', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/wise/wildtype', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/wise/simple/wildtype', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/correction/wise/wildtype', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/correction/wildtype', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'mutations': {
                        'title': 'Mutations',
                        'type': 'DataTable',
                        'actionName': 'getSsahaSummaryTableData',
                        'request': {'grouptype': 'mutation', 'correction': 'true'},
                        'toolbar': [
                            {'caption': 'Multalin Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/multalin/mutation', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Table', 'url': 'pairs', 'subUrl': 'ssaha2/correction/wise/mutation', 'request': {}, 'type': 'url', 'color': 'danger', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/wise/simple/mutation', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/correction/wise/mutation', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for all strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Chart correction for reliable strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/correction/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'},
                            {'caption': 'Ssaha 2 Roi Csv', 'url': 'roicsv', 'subUrl': 'ssaha2/correction/mutation', 'request': {}, 'type': 'url', 'color': 'warning', 'icon': 'ext-link'}
                        ]
                    },
                    'unaligneds':{
                        'title': 'Ssaha 2 Unaligneds',
                        'type': 'DataTable',
                        'actionName': 'getUnalignedSummaryTableData',
                        'request': {'target': 'ssaha2'},
                        'toolbar': [
                            {'caption': 'Fasta', 'url': 'fasta', 'subUrl': 'ssaha2/correction/wise/unaligneds', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'}
                        ]
                    },
                    'types':{
                        'title': 'Type repartition',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'ssaha2/correction/type/html'
                    },
                    'lengths':{
                        'title': 'Length Change',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'ssaha2/correction/length/html'
                    },
                    'gaps':{
                        'title': 'Gaps',
                        'type': 'urlContent',
                        'url': 'report',
                        'suburl': 'ssaha2/correction/gaps/html'
                    }
                }
            },
#             'synthesis': {
#                 'title': 'Synthesis',
#                 'type': 'report',
#                 'charts': [
#                     {'label': 'Type', 'field': 'type', 'type': 'Pie', 'mode': 'blast'},
#                     {'label': 'Sequence Length','field': 'slen', 'type': 'Hist', 'mode': 'blast'},
#                     {'label': 'Global Length Change','field': 'globalLengthChange', 'type': 'Hist', 'mode': 'blast'}
#                 ]
#             },
#             'groupby': {
#                 'title': 'Blast Statistics',
#                 'type': 'TabView',
#                 'subtabs': {
#                     'localLengthChange': {
#                         'title': 'ROI length change',
#                         'type': 'groupby',
#                         'actionName': 'groupby',
#                         'groupby': 'type',
#                         'key': 'localLengthChange',
#                         'toolbar': []
#                     },
#                     'globalLengthChange': {
#                         'title': 'Around ROI length change',
#                         'type': 'groupby',
#                         'actionName': 'groupby',
#                         'groupby': 'type',
#                         'key': 'globalLengthChange',
#                         'toolbar': []
#                     },
#                     'pident': {
#                         'title': 'Identity percentage',
#                         'type': 'groupby',
#                         'actionName': 'groupby',
#                         'groupby': 'type',
#                         'key': 'pident',
#                         'toolbar': []
#                     },
#                     'gaps': {
#                         'title': 'Number of gaps',
#                         'type': 'groupby',
#                         'actionName': 'groupby',
#                         'groupby': 'type',
#                         'key': 'gaps',
#                         'toolbar': []
#                     },
#                     'bitscore': {
#                         'title': 'Blast Bitscore',
#                         'type': 'groupby',
#                         'actionName': 'groupby',
#                         'groupby': 'type',
#                         'key': 'bitscore',
#                         'toolbar': []
#                     },
#                     'sstart': {
#                         'title': 'Alignment start',
#                         'type': 'groupby',
#                         'actionName': 'groupby',
#                         'groupby': 'type',
#                         'key': 'sstart',
#                         'toolbar': []
#                     },
#                     'send': {
#                         'title': 'Alignment end',
#                         'type': 'groupby',
#                         'actionName': 'groupby',
#                         'groupby': 'type',
#                         'key': 'send',
#                         'toolbar': []
#                     },
#                     'score': {
#                         'title': 'Blast Score',
#                         'type': 'groupby',
#                         'actionName': 'groupby',
#                         'groupby': 'type',
#                         'key': 'score',
#                         'toolbar': []
#                     },
#                     'slen': {
#                         'title': 'Sequence length',
#                         'type': 'groupby',
#                         'actionName': 'groupby',
#                         'groupby': 'type',
#                         'key': 'slen',
#                         'toolbar': []
#                     }
#                 }
#             }
        }
    },
    'Source': {
	'folder': {
	    'tabs': {
	    }
	  },
         'default': {
            'title': 'New Source',
            'description': 'Describe this Source',
            'folders': [],
            'roi': 1,
            'minus': 1,
            'plus': 1,
            'sid': '',
            'sequence': ''
        },
        'title': 'title',
        'description': 'description',
        'tabs': {
            'informations': {
                'title': 'Informations',
                'type': 'FieldForm',
                'fields': {
                    'title': {'type': 'text'},
                    'description': {'type': 'text'},
                    'roi': {'type': 'number'},
                    'minus': {'type': 'number'},
                    'plus': {'type': 'number'},
                    'sid': {'type': 'text'},
                    'sequence': {'type': 'textarea'}
                },
                'callbacks': [],
                'toolbar': [
                    {'caption': 'BTLC', 'url': 'data', 'subUrl': 'blast/typeAndLengthChange', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                    {'caption': 'STLC', 'url': 'data', 'subUrl': 'ssaha2/typeAndLengthChange', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                    {'caption': 'Blast Data', 'title': 'Blast Raw Alignment Events Data for Reliable Strands', 'url': 'alignmentcsv', 'subUrl': 'blast/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                    {'caption': 'Ssaha 2 Data', 'title': 'Ssaha 2 Alignment Events Data for Reliable Strands', 'url': 'alignmentcsv', 'subUrl': 'ssaha2/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                    {'caption': 'Blast Chart', 'title': 'Blast Alignment Events Chart for Reliable Strands', 'url': 'alignmentchart', 'subUrl': 'blast/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'},
                    {'caption': 'Ssaha 2 Chart', 'title': 'Ssaha 2 Alignment Events Chart for Reliable Strands', 'url': 'alignmentchart', 'subUrl': 'ssaha2/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'}
                ]
            },
            'schema': {
                'title': 'Schema',
                'type': 'htmlView',
                'url': 'scheme'
            },
            'export': {
                'title': 'Export',
                'type': 'DataTable',
                'actionName': 'getDsbTableData',
                'request': {},
                'toolbar': [
                    {'caption': 'Csv', 'url': 'alignmentcsv', 'subUrl': 'blast/raw/reliable/zoom/none/none', 'request': {}, 'type': 'url', 'color': 'primary', 'icon': 'ext-link'}
                ]
            }
        }
    },
    'Strand': {
	'folder': {
	    'tabs': {
	    }
	  },
         'default': {
            'record': {'id': '', 'sequence': ''}
        },
        'title': 'title',
        'description': 'description',
        'tabs': {
            'schema': {
                'title': 'Schema',
                'type': 'htmlView',
                'url': 'scheme'
            }
        }
    },
    'BlastAlignment': {
	'folder': {
	    'tabs': {
	    }
	  },
         'default': {

         },
        'title': 'title',
        'description': 'description',
        'tabs': {
            'schema': {
                'title': 'Schema',
                'type': 'htmlView',
                'url': 'scheme'
            }
        }
    }
}
