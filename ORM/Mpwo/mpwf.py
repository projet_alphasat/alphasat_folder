# -*- coding: utf-8 -*-
from .mpwo import *
from ..ObjectRouter.constructor import *


class MongoPersistentWebFolder(MongoPersistentWebObject):

    def getCollection(self):
        return 'webFolder'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)

    def getForeignQuery(self):
        self.fetch()
        q = {}
        q[self.getValue('field')] = self.mid
        return q

    def getChildrenObjects(self):
        return getCollectionFromQuery(self.getValue('collection'), self.getForeignQuery())

    def getWebPaths(self):
        if self.getValue('parents') == []:
            return ['/' + self.getValue('title')]
        else:
            paths = []
            for mid in self.getValue('parents'):
                pf = MongoPersistentWebFolder(mid=mid)
                pf.fetch()
                for ppath in pf.getWebPaths():
                    paths.append(ppath + '/' + self.getValue('title'))
            return paths

    def saveSelection(self, filter):
        self.fetch()
        self.update({'selection':filter})


class MongoPersistentWebFolders(MongoPersistentWebObjects):

    def getCollection(self):
        return 'webFolder'

    def setCollection(self, collection):
        self.collection = collection

    collection = property(getCollection, setCollection)
