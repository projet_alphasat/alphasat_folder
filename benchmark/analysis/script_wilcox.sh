#!/bin/bash

echo "Wilcoxon test for requetes iterations in databases"

R --save > R_2.log <<!

time=read.table("data_benchmark_3", head=T)
time_total=time[time\$cpu=="total", ]
by(time_total, INDICE=paste(time_total\$db,time_total\$size,time_total\$requete), FUN=function(x){wilcox.test(x\$time~x\$iteration)})
q()
!
