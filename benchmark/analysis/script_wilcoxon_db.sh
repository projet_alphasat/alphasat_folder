#!/bin/bash

echo "Wilcoxon test for requetes iterations in databases"

R --save > R_db.log <<!

time=read.table("data_benchmark_3", head=T)
time_total=time[time\$cpu=="total", ]
by(time_total, INDICE=paste(time_total\$size,time_total\$requete,time_total\$iteration), FUN=function(x){wilcox.test(x\$time~x\$db)})
q()
!
