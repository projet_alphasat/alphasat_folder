#!/bin/bash
while [[ -e /home/valade/alphasat_folder/run_script ]]
	do
	i=1
	while test $i != 6
		do
		python3.3 script_benchmark.py test$i &>> out_index${i}.log
		python3.3 script_benchmark_mongo_2.py -db $i &>> out_mongo_loop${i}.log
		i=$(($i + 1))
	done
done
