import time
import random
import psycopg2
import psycopg2.extras
import sys
import numpy as np

#Connexion a la base de donnees
try:
	conn = psycopg2.connect(database = sys.argv[1], user="testuser", password="testuser")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)

#Requete 1
print("########################## NOUVEAU TEST ##########################")
for t1 in range(2):
	A1 = time.time()
	A2 = time.clock()
	cursor.execute("SELECT * FROM Monomer;")
	for a1 in cursor:
		a = 0
	B1 = time.time()
	B2 = time.clock()
	print("Temps total de la requete 1 iteration %i : %f" %(t1, B1 - A1))
	print("Temps CPU de la requete 1 iteration %i : %f" %(t1, B2 - A2))
	cursor.execute("commit")

#Requete 2
for t2 in range(2):
	C1 = time.time()
	C2 = time.clock()
	cursor.execute("SELECT * FROM Chromosome;")
	chromosomes = cursor.fetchall()
	D1 = time.time()
	D2 = time.clock()
	print("Temps total de la requete 2 iteration %i : %f" %(t2, D1 - C1))
	print("Temps CPU de la requete 2 iteration %i : %f" %(t2, D2 - C2))

cursor.execute("commit")
#Requete 3
#Selection 1 element
i = random.randint(0, len(chromosomes) - 1)
for t3 in range(2):
	E1 = time.time()
	E2 = time.clock()
	cursor.execute("SELECT * FROM Chromosome WHERE chromosome_id = %s;",(chromosomes[i][0],))
	for a3 in cursor:
		a = 0
	F1 = time.time()
	F2 = time.clock()
	print("Temps total de la requete 3 iteration %i : %f" %(t3, F1 - E1))
	print("Temps CPU de la requete 3 iteration %i : %f" %(t3, F1 - E1))
	EE1 = time.time()
	EE2 = time.clock()
	cursor.execute("SELECT count(*) FROM Chromosome WHERE chromosome_id = %s;",(chromosomes[i][0],))
	FF1 = time.time()
	FF2 = time.clock()
	print("Temps total de la requete 3count iteration %i : %f" %(t3,FF1 - EE1))
	print("Temps CPU de la requete 3count iteration %i : %f" %(t3,FF2 - EE2))
	cursor.execute("commit")
cursor.execute("SELECT * FROM Taxon")
taxons = cursor.fetchall()
cursor.execute("commit")
for t4 in range(2):
	clock_time = 0
	total_time = 0
	for x in taxons:
		G1 = time.time()
		G2 = time.clock()
		cursor.execute("select * from Chromosome INNER JOIN Taxon ON Chromosome.taxon_id = Taxon.taxon_id WHERE taxon_name = %s AND Chromosome.chromosome_id = %s ;",(x[1],chromosomes[i][0],))
		for a4 in cursor:
			a = 0
		H1 = time.time()
		H2 = time.clock()
		total_time += (H1 - G1)
		clock_time += (H2 - G2)
	print("Temps total de la requete 3bis iteration %i : %f" %(t4, total_time))
	print("Temps CPU de la requete 3bis iteration %i : %f" %(t4, clock_time))
	cursor.execute("commit")
#Requete 4
#Selection de 1000 elements
random = np.random.choice((len(chromosomes) - 1),1000,replace=False)
random = random.tolist()
random_list = [chromosomes[j][0] for j in random]
for t6 in range(2):
	I1 = time.time()
	I2 = time.clock()
	cursor.execute("SELECT * FROM Chromosome WHERE chromosome_id IN %s;",(tuple(random_list),))
	for a5 in cursor:
		a = 0
	J1 = time.time()
	J2 = time.clock()
	print("Temps total de la requete 4 iteration %i : %f" %(t6,J1 - I1))
	print("Temps CPU de la requete 4 iteration %i : %f" %(t6,J2 - I2))
	II1 = time.time()
	II2 = time.clock()
	cursor.execute("SELECT count(*) FROM Chromosome WHERE chromosome_id IN %s;",(tuple(random_list),))
	JJ1 = time.time()
	JJ2 = time.clock()
	print("Temps total de la requete 4count iteration %i : %f" %(t6,JJ1 - II1))
	print("Temps CPU de la requete 4count iteration %i : %f" %(t6,JJ2 - II2))
cursor.execute("commit")
for t7 in range(2):
	clock_time2 = 0
	total_time2 = 0
	for y in taxons:
		K1 = time.time()
		K2 = time.clock()
		cursor.execute("select * from Chromosome INNER JOIN Taxon ON Chromosome.taxon_id = Taxon.taxon_id WHERE taxon_name = %s AND Chromosome.chromosome_id IN %s ;",(y[1],tuple(random_list),))
		for a6 in cursor:
			a = 0
		L1 = time.time()
		L2 = time.clock()
		total_time2 += (L1 - K1)
		clock_time2 += (L2 - K2)
	print("Temps total de la requete 4bis iteration %i : %f" %(t7,total_time2))
	print("Temps CPU de la requete 4bis iteration %i : %f" %(t7,clock_time2))
	cursor.execute("commit")
#Requete 5
#Selection de 10000 elements
random2 = np.random.choice((len(chromosomes) - 1),10000,replace=False)
random2 = random2.tolist()
random_list2 = [chromosomes[k][0] for k in random2]
for t9 in range(2):
	M1 = time.time()
	M2 = time.clock()
	cursor.execute("SELECT * FROM Chromosome WHERE chromosome_id IN %s;",(tuple(random_list2),))
	for a7 in cursor:
		a = 0
	N1 = time.time()
	N2 = time.clock()
	print("Temps total de la requete 5 iteration %i : %f" %(t9,N1 - M1))
	print("Temps CPU de la requete 5 iteration %i : %f" %(t9,N2 - M2))
	MM1 = time.time()
	MM2 = time.clock()
	cursor.execute("SELECT count(*) FROM Chromosome WHERE chromosome_id IN %s;",(tuple(random_list2),))
	NN1 = time.time()
	NN2 = time.clock()
	print("Temps total de la requete 5count iteration %i : %f" %(t9,NN1 - MM1))
	print("Temps CPU de la requete 5count iteration %i : %f" %(t9,NN2 - MM2))
	cursor.execute("commit")
for t10 in range(2):
	clock_time3 = 0
	total_time3 = 0
	for z in taxons:
		O1 = time.time()
		O2 = time.clock()
		cursor.execute("select * from Chromosome INNER JOIN Taxon ON Chromosome.taxon_id = Taxon.taxon_id WHERE taxon_name = %s AND Chromosome.chromosome_id IN %s ;",(z[1],tuple(random_list2),))
		for a8 in cursor:
			a = 0
		P1 = time.time()
		P2 = time.clock()
		total_time3 += (P1 - O1)
		clock_time3 += (P2 - O2)
	print("Temps total de la requete 5bis iteration %i : %f" %(t10,total_time3))
	print("Temps CPU de la requete 5bis iteration %i : %f" %(t10,clock_time3))
	cursor.execute("commit")
