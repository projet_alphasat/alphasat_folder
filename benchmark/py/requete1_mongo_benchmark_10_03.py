import time
import random
import argparse
import ORM
import numpy as np

parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("-db", help="database", type=int)
args = parser.parse_args()

if args.db == 1:
	t = ORM.CONNECTOR['mongo'][ORM.DB1]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB1]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB1]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB1]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB1]['Family']
elif args.db == 2:
	t = ORM.CONNECTOR['mongo'][ORM.DB2]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB2]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB2]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB2]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB2]['Family']
elif args.db == 3:
	t = ORM.CONNECTOR['mongo'][ORM.DB3]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB3]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB3]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB3]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB3]['Family']
elif args.db == 4:
	t = ORM.CONNECTOR['mongo'][ORM.DB4]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB4]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB4]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB4]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB4]['Family']
elif args.db == 5:
	t = ORM.CONNECTOR['mongo'][ORM.DB5]['Taxon']
	b = ORM.CONNECTOR['mongo'][ORM.DB5]['Block']
	m = ORM.CONNECTOR['mongo'][ORM.DB5]['Monomer']
	c = ORM.CONNECTOR['mongo'][ORM.DB5]['Chromosome']
	f = ORM.CONNECTOR['mongo'][ORM.DB5]['Family']

#Requete 1
print("########################## NOUVEAU TEST ##########################")
for t1 in range(2):
	A1 = time.time()
	A2 = time.clock()
	monomers = m.find({})
	for a1 in monomers:
		a = 0
	B1 = time.time()
	B2 = time.clock()
	print("Temps total de la requete 1 iteration %i : %f" %(t1,B1 - A1))
	print("Temps CPU de la requete 1 iteration %i : %f" %(t1,B2 - A2))
