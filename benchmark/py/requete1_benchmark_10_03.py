import time
import random
import psycopg2
import psycopg2.extras
import sys
import numpy as np

#Connexion a la base de donnees
try:
	conn = psycopg2.connect(database = sys.argv[1], user="testuser", password="testuser")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)

#Requete 1
print("########################## NOUVEAU TEST ##########################")
for t1 in range(2):
	A1 = time.time()
	A2 = time.clock()
	cursor.execute("SELECT * FROM Monomer;")
	for a1 in cursor:
		a = 0
	B1 = time.time()
	B2 = time.clock()
	print("Temps total de la requete 1 iteration %i : %f" %(t1, B1 - A1))
	print("Temps CPU de la requete 1 iteration %i : %f" %(t1, B2 - A2))
	cursor.execute("commit")

